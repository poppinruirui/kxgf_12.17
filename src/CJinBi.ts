class CJinBi extends CObj {

     // protected m_bmpMainPic:egret.Bitmap;
      protected m_BoundObj:CObj = null;

      protected m_nCurFrameIndex:number = 0;
      protected m_fAniTimeElapse:number = 0;

      protected m_eType:Global.eMoneyType = Global.eMoneyType.coin;
      protected m_eSubType:Global.eMoneySubType;
      protected m_nValue:number = 0;

     public constructor() {
        super();

        this.m_bmpMainPic = new egret.Bitmap( );
        this.addChild( this.m_bmpMainPic );

        this.touchEnabled = false;
   //     this.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapJinBi, this);
    } //  end constructor

    public SetValue( eType: Global.eMoneyType, nValue:number ):void
    {
        this.m_eType = eType;
        this.m_nValue = nValue;
    }

    public SetSubType( eSubType:Global.eMoneySubType ):void
    {
        this.m_eSubType = eSubType;
    }

    public OnClick():void
    {
        var tiaozi:CTiaoZi = CTiaoZiManager.NewTiaoZi();
        tiaozi.BeginTiaoZi();
        tiaozi.x = this.GetBoundObj().x + 100;
        tiaozi.y = this.GetBoundObj().y;// - 150;
        tiaozi.SetTypeAndValue( this.m_eType, this.m_nValue );
        tiaozi.scaleX = 1;
        tiaozi.scaleY = 1;
        
        if  ( this.m_eType == Global.eMoneyType.coin )
        {
            Main.s_CurTown.SetCoins( Main.s_CurTown.GetCoins() + this.m_nValue );
        }
        else if (this.m_eType == Global.eMoneyType.diamond)
        {
            CPlayer.SetDiamond( CPlayer.GetDiamond() + this.m_nValue );
        }


        (this.m_BoundObj as CAutomobile).ClearBoundJinBi();
        CJinBiManager.DeleteJinBi( this );

        if ( this.m_eSubType == Global.eMoneySubType.small_coin )
        {
            CSoundManager.PlaySE( Global.eSE.small_coin );
        }
        else if (this.m_eSubType == Global.eMoneySubType.big_coin)
        {
             CSoundManager.PlaySE( Global.eSE.big_coin );
        }
        else if (this.m_eSubType == Global.eMoneySubType.big_diamond ||
        this.m_eSubType == Global.eMoneySubType.small_diamond
        )
        {
            CSoundManager.PlaySE( Global.eSE.green_bux );
        }

       // 飞翔的绿票
         Main.s_CurTown.TownPos2StagePos( this.GetBoundObj().x, this.GetBoundObj().y );
    //console.log( "car stage pos=" +  CTown.s_objectTemp["StagePosX"] + "," + CTown.s_objectTemp["StagePosY"]  );
        CGreenMoneymanager.GenerateOneGreenMoneyToFly( 1,  CTown.s_objectTemp["StagePosX"],  CTown.s_objectTemp["StagePosY"] );
    }

    public onTapJinBi(e: egret.TouchEvent):void{
       this.OnClick();
    }

    public BeginTiaoZi():void
    {

    }

    public SetBoundObj( obj:CObj ):void
    {
        this.m_BoundObj = obj;
    }

    public GetBoundObj( ):CObj
    {
        return this.m_BoundObj;
    }

   

    public UpdateJinBiAnimation():void
    {
        this.m_fAniTimeElapse += CDef.s_fFixedDeltaTime;
        if ( this.m_fAniTimeElapse < CDef.s_fJinBiAniInterval )
        {
            return;
        }
        this.m_fAniTimeElapse = 0;

        var nRealResIndex:number = this.m_nCurFrameIndex;
        if ( this.m_nCurFrameIndex == 2 )
        {
            nRealResIndex = 3;
        }
        if ( this.m_nCurFrameIndex == 3  )
        {
            nRealResIndex = 2;
        }
         if ( this.m_nCurFrameIndex == 4  )
        {
            nRealResIndex = 3;
        }
        this.m_bmpMainPic.texture = CJinBiManager.GetFrameTexture(nRealResIndex, this.m_eType);
        
        this.m_nCurFrameIndex++
        if ( this.m_nCurFrameIndex >= CDef.s_fJinBiAniFrameNum )
        {
            this.m_nCurFrameIndex = 0;
        }

                       this.anchorOffsetX = this.width / 2;
                 this.anchorOffsetY = this.height / 2;
    }

    public  Update():void
    {
        this.UpdateJinBiAnimation();
        //this.UpdatePos();
        
    }

} // end class