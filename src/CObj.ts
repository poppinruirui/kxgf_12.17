


class CObj extends egret.DisplayObjectContainer {

    public static objectTemp:Object = new Object();

    protected m_BuildingData:CConfigBuilding = null;

    protected m_eBuildinSpecial:Global.eOperate = Global.eOperate.do_nothing;

    protected m_objBuilding:CObj = null;

    public  m_bmpMainPic:egret.Bitmap = new egret.Bitmap(); // 一个Obj的核心图片

    protected m_nGuid:number = 0;
    protected m_Town:CTown = null;
    protected m_eSizeType:Global.eObjSize = Global.eObjSize.size_1x1;
    protected m_eFuncType:Global.eObjFunc = Global.eObjFunc.none;
    protected m_eOperateType:Global.eOperate = Global.eOperate.edit_obj;
    protected m_eBuildingLandStatus:Global.eBuildingLandStatus = Global.eBuildingLandStatus.empty;

    protected m_ConstructingAni:CConstructingAnimation = null;

    protected m_fSortPosY:number = 0;

    protected m_txtDebugInfo:egret.TextField = new egret.TextField();

    protected m_bHasBuilding:boolean = false;

    protected m_nLotLevel:number = 0; // 建筑物其实是没有等级的概念的，等级和产出其实是那个地块的
    protected m_nCurBuildingResIndex:number = 0;
    protected m_bHistorical:boolean = false;

    protected m_eLotProperty:Global.eLotPsroperty = Global.eLotPsroperty.residential;

    protected m_progressBar:CUIBaseProgressBar = null;

    protected m_eProcessType:Global.eProgressBarType = Global.eProgressBarType.none;
    protected m_nProcessingTimeElapse:number = 0;
    protected m_nProcessingTotalTime:number = 0;

    protected m_CurBuildingConfig:CConfigBuilding = null;

    protected m_fSpeed:number = 0;
    protected m_fTime:number = 0;
    protected m_objectParams:Object = new Object();

    //// Move to Dest
    protected m_vecDestPos:CVector = new CVector();
    protected m_vecSpeed:CVector = new CVector(); 
    //// end Move to Dest


 public constructor() {
        super();

        this.addChild( this.m_bmpMainPic );
       
       
        
       


 }

   public SetSizeType( eSizeType:Global.eObjSize ):void
   {

        this.m_eSizeType = eSizeType;
   }

   public GetSizeType():Global.eObjSize
   {
       return this.m_eSizeType;
   }

   public static GetPixelSizeBySizeType( eType:Global.eObjSize ):Object
   {
       switch( eType )
       {
           case Global.eObjSize.size_1x1:
           {
               CObj.objectTemp["x"] = CDef.s_nTileWidth;
               CObj.objectTemp["y"] = CDef.s_nTileHeight;
           }
           break;

           case Global.eObjSize.size_2x2:
           {
               CObj.objectTemp["x"] = -160;//CDef.s_nTileWidth;
               CObj.objectTemp["y"] = -880;//CDef.s_nTileHeight;
           }
           break;

           case Global.eObjSize.size_4x4:
           {
               CObj.objectTemp["x"] = CDef.s_nTileWidth * 2;
               CObj.objectTemp["y"] = CDef.s_nTileHeight * 2;
           }
           break;
       }

       return CObj.objectTemp;
   }

   public SetFuncType( eFuncType:Global.eObjFunc ):void
   {
       this.m_eFuncType = eFuncType;

        if ( this.GetFuncType() == Global.eObjFunc.building )
        {
            /*
            this.scaleX = 1.5;
            this.scaleY = 1.5;
        
            var fLocalOffsetY:number = 0;
          switch( this.GetSizeType() )
          {
            case Global.eObjSize.size_2x2:
            {

                fLocalOffsetY = 2 * CDef.s_nTileHeight * 0.25;
            }
            break;
            case Global.eObjSize.size_4x4:
            {

                fLocalOffsetY = 4 * CDef.s_nTileHeight* 0.25 ;
            }
            break;
          }

          
           this.y += fLocalOffsetY;
*/
        }
        else
        {
            
        }

   }

    public GetFuncType(  ):Global.eObjFunc
   {
       return this.m_eFuncType;
   }

   public Reset():void
   {
       this.visible = true;
         this.SetMainPicVisible( true );
       this.m_objBuildingOnTile = null;

       this.m_BelongToDistrict = null;

       this.alpha = 1;

       this.rotation = 0;

       this.m_bmpMainPic.filters = null;

        this.m_eProcessType = Global.eProgressBarType.none;

       this.m_Town = null;

       // 跟本Obj绑定的格子
       this.m_aryBoundGridIndex.length = 0;
       this.m_gridLocation = null;

       // 跟本Obj绑定的建筑
       this.m_objBuilding  = null;
       this.m_bHasBuilding = false;

       this.m_eBuildinSpecial = Global.eOperate.do_nothing;

       this.scaleX = 1;
        this.scaleY = 1;

        this.m_nLotLevel = 0;

        this.m_nCurBuildingResIndex = 0;

        this.m_bmpMainPic.texture = null;

        this.m_BuildingData = null; 
        
   }



    public SetPos( x:number, y:number ):void
    {
        this.x = x;
        this.y = y;
    }

    private m_objectPos:Object = new Object();
    public GetPos( ):Object
    {
        this.m_objectPos["x"] = this.x;
        this.m_objectPos["y"] = this.y;
        return this.m_objectPos;
    }

    
    public SetGuid( nGuid:number ):void
    {
        this.m_nGuid = nGuid;
    }

    public GetGuid():number
    {
        return this.m_nGuid;
    }

    public SetBitmap( bitmap:egret.Bitmap ):void
    {
        this.m_bmpMainPic = bitmap;

       
    }

    public SetTexture( tex:egret.Texture ):void
    {
        this.m_bmpMainPic.texture = tex;
    }

   

    public GetTexture( ):egret.Texture
    {
        return this.m_bmpMainPic.texture;
    }

    public SetTown( town:CTown ):void
    {
        this.m_Town = town;
    }

    public ClearBoundGrid():void
    {
       
        for ( var i:number = 0; i < this.m_aryBoundGridIndex.length - 1; i+= 2 ) // 注意，步进是2
        {
           // console.log( "clear bound grid:" + this.m_aryBoundGridIndex[i] + "_" + this.m_aryBoundGridIndex[i+1] );
            var grid:CGrid = Main.s_CurTown.GetGridByIndex( this.m_aryBoundGridIndex[i].toString(), this.m_aryBoundGridIndex[i+1].toString() );
            if ( grid == null || grid == undefined )
            {
                console.log( "有Bug:" + this.m_aryBoundGridIndex[i] + "," + this.m_aryBoundGridIndex[i+1] );
                continue;
            }
                    if ( grid.GetZhiNengJianZhu() )
        {
          console.log( "不得哦aaaaaaaaaa"  );
        }
            grid.SetBoundObj(null);
        }
    }

    protected m_gridLocation:CGrid = null;
    public SetLocationGrid( grid:CGrid ):void
    {
        this.m_gridLocation = grid;
    }
    public GetLocationGrid():CGrid
    {
        return this.m_gridLocation;
    }


    protected m_aryBoundGridIndex:Array<number> = new Array<number>();
    public SetBoundGrid( grid:CGrid ):void
    {
        this.m_aryBoundGridIndex.push( grid.GetId()["nIndexX"] );
        this.m_aryBoundGridIndex.push( grid.GetId()["nIndexY"] );
      //  console.log( "set bound grid:" + grid.GetId()["nIndexX"] + "," + grid.GetId()["nIndexY"] );
    }

    public SetOperateType( eOperate:Global.eOperate ):void
    {
        this.m_eOperateType = eOperate;
    }
    
    public GetOperateType(  ):Global.eOperate
    {
        return this.m_eOperateType;
    }
    
    public SetBuildingLandStatus( eStatus:Global.eBuildingLandStatus ):void
    {
        this.m_eBuildingLandStatus = eStatus;
    }

    public GetBuildingLandStatus():Global.eBuildingLandStatus
    {
        return this.m_eBuildingLandStatus;
    }

    public ProcessAddSpecialBuilding( op:Global.eOperate ):Global.eBuildinSpeical
    {
         var eBuildingSpecial:Global.eBuildinSpeical = Global.eBuildinSpeical.none;
         // this.m_objBuilding = new CObj();
          var szResName:string = "";
          switch( op )
          {
              case Global.eOperate.add_cityhall:
              {
                szResName ="shizhengting_png" ;
                 this.SetBuildinSpecial( op );
              
              }
              break;

              case Global.eOperate.add_bank:
              {
                szResName = "yinhang_png" ;
                this.SetBuildinSpecial( op );
               
              }
              break;

              case Global.eOperate.add_garage:
              {
                szResName = "chezhan_png" ;
                this.SetBuildinSpecial(op );
               
              }
              break;
          }
         
         // this.SetTexture( RES.getRes(szResName ) );
         // this.anchorOffsetX = this.width / 2;
         // this.anchorOffsetY = this.height;
         this.SetSpecialBuildingByType( op );


          var fLocalOffsetX:number = 0;
          var fLocalOffsetY:number = 0;

          this.scaleX = 1;
          this.scaleY = 1;
        
    
          
         this.SetHasBuilding( true );

         return eBuildingSpecial;
    }

    protected onTapgarage( evt:egret.TouchEvent ):void
    {
        CUIManager.SetUiVisible( Global.eUiId.garage, true );
    }

    // 获取这个地块的CPS
    // CTown.ts中的GetBuildingCPS()函数，是获取当前所有地块CPS的总和
    protected m_nRealCPSOfThisLot:number = 0;
    public GetLotCPS( ):number
    {
                // poppin to do 
                // 可以预见的，接下里会影响地块产出的因素还有：特殊建筑、市政厅中的项目加成
             var nLotCps =    CConfigManager.GetBuildingCoinBySizeAndLevel( this.GetMyTile().GetSizeType(), this.GetMyTile().GetLotLevel() );
             var nCPS_temp:number = nLotCps;
             var nCityHallItemAffect:number = Main.s_CurTown.GetCityHallItemAffect( 0 );
             if ( nCityHallItemAffect != undefined )
             {
             
                 nLotCps *= ( 1 + nCityHallItemAffect );
             }

             if ( this.m_BuildingData != null )
             {
                  if ( this.m_BuildingData.bSpecial )
                  {
                      
                       nLotCps *= ( 1 + this.m_BuildingData.fGainRate );

                  }
             }

               

               this.m_nRealCPSOfThisLot = Math.ceil( nLotCps );
               console.log( "this.m_nRealCPSOfThisLot=" + this.m_nRealCPSOfThisLot );
                return this.m_nRealCPSOfThisLot;
    }

    public ProcessUpgrade():void
    {
      
      

            CUIManager.s_uiLotUpgrade.UpdateInstanceUpgradeButtonStatus();
            CUIManager.s_uiLotUpgrade.SetCurLotLevel( this.GetLotLevel() );
            CUIManager.s_uiLotUpgrade.SetCurLotCPS( this.m_nRealCPSOfThisLot );
            CUIManager.s_uiLotUpgrade.SetBuildingAvatar( this.m_objBuildingOnTile.GetTexture() );
            CUIManager.s_uiLotUpgrade.SetBuildingName( this.m_BuildingData/*m_CurBuildingConfig*/.szName );
            CUIManager.s_uiLotUpgrade.UpdatePanelStatusWhenOpen();
    }

   

   
    public ProcessEdit():void
    {
        if ( Main.s_CurTown.GetOperate() != Global.eOperate.edit_obj )
        {
            return;
        }

        /*
         if ( this.IsBuildinSpecial() ) // 是系统内建的特殊建筑物，不能编辑
         {
             if ( this.GetBuildinSpecial() == Global.eBuildinSpeical.garage )
             {
                 CUIManager.SetUiVisible( Global.eUiId.garage, true );
                 Main.s_CurTown.UpdateGarageStatus();
             }
             else if ( this.GetBuildinSpecial() == Global.eBuildinSpeical.cityhall )
             {
                 CUIManager.SetUiVisible( Global.eUiId.city_hall, true );
                 CUIManager.s_uiCityHall.UpdateStatus();
             }
             else if ( this.GetBuildinSpecial() == Global.eBuildinSpeical.bank )
             {
                 CUIManager.SetUiVisible( Global.eUiId.bank, true );
                 CUIManager.s_uiBank.UpdateStatus();
             }

             return;
         }
         */


         // 目前只有房屋类的Obj可以操作。
          if ( this.GetFuncType() != Global.eObjFunc.tile  )
          {
              return;
          }      

          if ( this.m_eProcessType == Global.eProgressBarType.constructing )
          {
              return;
          }

          if ( this.GetBuildinSpecial() != Global.eOperate.do_nothing )
          {
              return;
          }

          Main.s_CurTown.SetCurEditProcessingLot( this );

         if ( this.GetBuildingLandStatus() == Global.eBuildingLandStatus.building_exist )
         {
             CUIManager.SetUiVisible( Global.eUiId.lot_upgrade, true );
             this.ProcessUpgrade();
             Main.s_CurTown.SetCurEditProcessingLot( this );
            return;
         }
    

         CUIManager.SetUiVisible( Global.eUiId.buy_lot_panel, true );
        // Main.s_CurTown.UpdateCurLotStatus();
      
         var nCurNumOfThisSize:number = Main.s_CurTown.GetCurBuildingLotNum( this.GetSizeType() );
         var nBulkNum:number = 1;
         var nTotalCost:number = 0;
         var nBuyOnePrice:number = (CConfigManager.s_dicLotPrice[this.GetSizeType()])[nCurNumOfThisSize]; 
         var nBulkCost:number = 0;
         while( nTotalCost <= Main.s_CurTown.GetCoins() )
         {
             nBulkNum++;
             nTotalCost = 0;
             for ( var i:number = 0; i < nBulkNum; i++ )
             {
                var nNum:number = nCurNumOfThisSize + i;
                nTotalCost +=  (CConfigManager.s_dicLotPrice[this.GetSizeType()])[nNum];     
             }
         } // end while
        nBulkNum--;

        // 至此，nBulkNum表示当前拥有的金币能批量建设的最大个数
        // 但是，还要检查一下剩余多少空地。就算有钱，没有空地也没法批量建设
        console.log( "当前剩余这型空地:" + Main.s_CurTown.GetEmptyLotNum(this.GetSizeType()) );

        if ( nBulkNum >= 2 )
        {
            for ( var i:number = 0; i < nBulkNum; i++ )
             {
                var nNum:number = nCurNumOfThisSize + i;
                nBulkCost +=  (CConfigManager.s_dicLotPrice[this.GetSizeType()])[nNum];     
             }
            console.log( "bulk buy:" + nBulkNum + " , " + nBulkCost );
        }
        else
        {
            console.log( "can not bulk buy"  );
        }
        
         CUIManager.s_uiBuyLotPanel.SetParams( nBuyOnePrice, 0, 0, 
            Main.s_CurTown.GetLotProperty( Global.eLotPsroperty.residential ),
            Main.s_CurTown.GetLotProperty( Global.eLotPsroperty.business ),
            Main.s_CurTown.GetLotProperty( Global.eLotPsroperty.service )
          );

         this.SetLotLevel( 1 );
    }

    // 开始升级
    public BeginUpgrade():void
    {
       // CSoundManager.PlaySE( Global.eSE.begin_upgrade );

        // 生成一个进度条
        if ( this.m_progressBar == null  )
        {
            this.m_progressBar = CResourceManager.NewProgressBar();
            Main.s_CurTown.AddProgressBar( this.GetDistrict(),  this.m_progressBar );
        }
     
 CSoundManager.PlaySE( Global.eSE.begin_upgrade );
     
         this.m_ConstructingAni = CResourceManager.NewConstructingAni();
        this.m_ConstructingAni.SetSize( this.GetSizeType() );
     
        
        // poppin test 暂不显示建造动画
        // this.addChild( this.m_ConstructingAni );
        CObj.objectTemp = CObj.GetPixelSizeBySizeType( this.GetSizeType() );
        this.m_ConstructingAni.x = CObj.objectTemp["x"];
        this.m_ConstructingAni.y = CObj.objectTemp["y"];
        this.m_ConstructingAni.SetParentObjPixelSize(this.GetSizeType(),  CObj.objectTemp["x"], CObj.objectTemp["y"] );
        this.m_ConstructingAni.SetSegNum( 1 );  // 暂时统一用一层的建筑工地  
        

        this.m_progressBar.scaleX = 2.5;
        this.m_progressBar.scaleY = 2.5;
     if ( this.GetSizeType() == Global.eObjSize.size_2x2 )
        {   
             this.m_progressBar.x = this.GetLocationGrid().x - CDef.s_nConstructionProgressBarPosOffsetX;
             this.m_progressBar.y = this.GetLocationGrid().y - CDef.s_nConstructionProgressBarPosOffsetY;
        }
        else if ( this.GetSizeType() == Global.eObjSize.size_4x4 )
        {
            this.m_progressBar.x = this.GetLocationGrid().x - CDef.s_nConstructionProgressBarPosOffsetX;
            this.m_progressBar.y = this.GetLocationGrid().y - CDef.s_nConstructionProgressBarPosOffsetY * 1.5;
        }
        
        
        this.m_progressBar.Begin( this.m_nProcessingTotalTime );
        this.m_progressBar.SetTitle( "升级" );

        // poppin test 8.24
        this.m_eProcessType = Global.eProgressBarType.upgrading;
        this.m_nProcessingTotalTime = CConfigManager.GetUpgradeTimeByLotLevel ( this.GetLotLevel() );
        this.m_nProcessingTimeElapse = 0;
    }

    // 开始建造
    public BeginConstrucingStatus():void
    {
        if ( this.GetBuildingLandStatus() != Global.eBuildingLandStatus.empty )
        {
            console.log("有bug");
            return;
        }

        CSoundManager.PlaySE( Global.eSE.begin_upgrade );

        // 注意：只要一开始建设，就已经算是“本地块有建筑”了，同时就开始产钱了。
        this.SetHasBuilding( true );

        this.SetBuildingLandStatus( Global.eBuildingLandStatus.contructing );

        this.m_ConstructingAni = CResourceManager.NewConstructingAni();
        this.m_ConstructingAni.SetSize( this.GetSizeType() );

         // 暂不显示建造动画    
        //this.addChild( this.m_ConstructingAni );
        CObj.objectTemp = CObj.GetPixelSizeBySizeType( this.GetSizeType() );
        this.m_ConstructingAni.x = CObj.objectTemp["x"]; 
        this.m_ConstructingAni.y = CObj.objectTemp["y"];
        this.m_ConstructingAni.SetParentObjPixelSize(this.GetSizeType(),  CObj.objectTemp["x"], CObj.objectTemp["y"] );
        this.m_ConstructingAni.SetSegNum( 1 );  // 暂时统一用一层的建筑工地  

        // 生成一个进度条
        if ( this.m_progressBar == null )
        {
            this.m_progressBar = CResourceManager.NewProgressBar();
            Main.s_CurTown.AddProgressBar( this.GetDistrict(),  this.m_progressBar );
        }

        this.m_progressBar.scaleX = 2.5;
        this.m_progressBar.scaleY = 2.5;

        var posX:number = this.x;
        var posY:number = this.y;

        if ( this.GetSizeType() == Global.eObjSize.size_2x2 )
        {   
             this.m_progressBar.x = posX - CDef.s_nConstructionProgressBarPosOffsetX;
             this.m_progressBar.y = posY - CDef.s_nConstructionProgressBarPosOffsetY;
        }
        else if ( this.GetSizeType() == Global.eObjSize.size_4x4 )
        {
            this.m_progressBar.x = posX - CDef.s_nConstructionProgressBarPosOffsetX;
            this.m_progressBar.y = posY - CDef.s_nConstructionProgressBarPosOffsetY * 1.5;
        }
        
        this.m_progressBar.Begin( CConfigManager.GetConstructingTime() );
        this.m_progressBar.SetTitle( "新建" );

        this.m_eProcessType = Global.eProgressBarType.constructing;
        this.m_nProcessingTotalTime = 1;
        this.m_nProcessingTimeElapse = 0;
     }

     public ProgressBarLoop():void
     {
         if ( this.m_progressBar == null )
         {
             return;
         }

         if ( this.m_eProcessType == Global.eProgressBarType.none )
         {
             return;
         }

         this.m_nProcessingTimeElapse += CDef.s_fFixedDeltaTime;
         var fPercent:number = this.m_nProcessingTimeElapse / this.m_nProcessingTotalTime;
        

         if ( this.m_progressBar != null )
         {
             this.m_progressBar.Loop(fPercent);
         }

         if ( fPercent >= 1 )
         {
             if ( this.m_eProcessType == Global.eProgressBarType.constructing )
             {
                this.EndConstrucingStatus();
             }
             else if ( this.m_eProcessType == Global.eProgressBarType.upgrading )
             {
                this.EndUUpgrade();
                Main.s_CurTown.SetupgradinLot( null );
             }
         }
     }

     public ProgressBarEnd():void
     {
         if ( this.m_progressBar == null )
         {
             return;
         }

         CResourceManager.DeleteProgressBar( this.m_progressBar );
         this.m_progressBar = null;
     }

     // 取消升级 
     public CancelUpgrade():void
     {
         Main.s_CurTown.SetupgradinLot( null );

         this.m_eProcessType = Global.eProgressBarType.none;
         this.ProgressBarEnd();
         CResourceManager.DeleteConstructingAni( this.m_ConstructingAni );

           Main.s_CurTown.UpdateCurLotStatus();
        Main.s_CurTown.OnBuildingCPSChanged();
     }

     // 结束升级
     public EndUUpgrade():void
     {

         this.m_eProcessType = Global.eProgressBarType.none;

        this.ProgressBarEnd();

        if ( this.m_ConstructingAni )
        {
            CResourceManager.DeleteConstructingAni( this.m_ConstructingAni );
        }
        this.SetLotLevel( this.GetLotLevel() + 1 );

        if ( !this.GetHistorical() )   
        {
            var cur_building:CConfigBuilding = this.GetBuilding();
            if ( cur_building != null && cur_building.bSpecial )
            {
                CBuildingManager.RecycleSpecialBuilding(cur_building);
            }
    
            var data:CConfigBuilding = CConfigManager.GetBuildingConfig( this.GetLotProperty(), this.GetSizeType() , this.m_nCurBuildingResIndex++ );
            this.SetBuilding( data );
        }

        Main.s_CurTown.UpdateCurLotStatus();
        Main.s_CurTown.OnBuildingCPSChanged();

    if ( this.GetDistrict() == Main.s_CurTown.m_CurDistrict )
    {
        var tiaozi:CTiaoZi = CTiaoZiManager.NewTiaoZi();
        tiaozi.BeginTiaoZi( Global.eTiaoZiType.level );
        tiaozi.x = this.x;
        tiaozi.y = this.y - 300;
        tiaozi.scaleX = 1/*2*/;
        tiaozi.scaleY = 1/*2*/;
       // tiaozi.SetTypeAndValue( this.m_eType, this.m_nValue );
        tiaozi.SetText( "lv." + this.GetLotLevel() );

        // 计算出该汽车的世界坐标。因为汽车是随容易拖动和缩放的，直接取得的它的坐标是局部坐标，在此需要转化成世界坐标（舞台坐标）
        Main.s_CurTown.EndUpgrade( this );



          // 播放一个“建造成功”的帧动画
         var effect:CFrameAni = CFrameAniManager.NewEffect();
         var szResUrl:string = CResourceManager.url + "Effect/WhiteSmoke/000";
         effect.SetParams( szResUrl, 18, 0, false,true );
         effect.Play();
         this.m_objBuildingOnTile.addChild( effect );
         effect.x = this.width / 2;
         effect.y = 100;
         if ( this.GetSizeType() == Global.eObjSize.size_2x2 )
         {
            effect.scaleX = 1.5;
            effect.scaleY = 1.5;
         }
         else
         {
              effect.scaleX =3;
            effect.scaleY = 3;
         }

    }

        if ( this == Main.s_CurTown.GetCurEditProcessingLot() )
        {
            this.ProcessUpgrade(  );
        }

                Main.s_CurTown.SetupgradinLot( null );



        


     }

    // 推倒成空地
   public Revert():void
   {
       CBuildingManager.RecycleSpecialBuilding( this.m_BuildingData );

       this.SetBuildingLandStatus( Global.eBuildingLandStatus.empty );
       Main.s_CurTown.RemoveUpgradableLot( this );
    
    /*
               this.anchorOffsetX = this.width / 2;
        this.anchorOffsetY = this.height;
       if ( this.GetSizeType() == Global.eObjSize.size_2x2 )
       {
      //  this.scaleX = 0.5;
       //  this.scaleY = 0.5;
            this.SetTexture( RES.getRes( "Tile_2x2_png" )  );
       }
       else
       {
        // this.scaleX = 1;
        // this.scaleY = 1;
            this.SetTexture( RES.getRes( "Tile_4x4_png" )  );
       }
*/

       this.SetHasBuilding( false );
       if ( this.m_eProcessType == Global.eProgressBarType.upgrading )
       {
           this.CancelUpgrade();
       }
      
      CResourceManager.DeleteObj( this.m_objBuildingOnTile );


      this.m_objBuildingOnTile = null;



        // 播放一个“摧毁建筑”的帧动画
         var effect:CFrameAni = CFrameAniManager.NewEffect();
         var szResUrl:string = CResourceManager.url + "Effect/RevertBuilding/";
         effect.SetParams( szResUrl, 12, 0, false,true );
         effect.Play();
         this.addChild( effect );
         effect.x = 180;
         effect.y = 200;
         if ( this.GetSizeType() == Global.eObjSize.size_2x2 )
         {
            effect.scaleX = 0.5;
            effect.scaleY = 0.5;
            effect.x = 120;
         effect.y = 200;
         }
         else
         {
             effect.x = 300;
         effect.y = 400;
              effect.scaleX =1;
            effect.scaleY = 1;
         }

   }

    // 结束建造
    public EndConstrucingStatus():void
    {
        this.m_eProcessType = Global.eProgressBarType.none;

         CSoundManager.PlaySE( Global.eSE.upgrad_complete );

        this.ProgressBarEnd();

        CResourceManager.DeleteConstructingAni( this.m_ConstructingAni );

        var data:CConfigBuilding = CConfigManager.GetBuildingConfig( this.GetLotProperty(), this.GetSizeType(), this.m_nCurBuildingResIndex++);
        //this.SetTexture( RES.getRes( this.m_CurBuildingConfig.szResName ) );
        this.SetBuilding( data );
        

        this.SetBuildingLandStatus( Global.eBuildingLandStatus.building_exist );

        Main.s_CurTown.AddUpgradableLot( this );

        Main.s_CurTown.CheckIfCanUpgrde();

         Main.s_CurTown.OnLotBuildingNumChanged();

       
    }

    


    public BuildingLandLoop():void
    {
        this.ConstructingLoop();
    }

    public ConstructingLoop():void
    {
        if ( this.m_eProcessType == Global.eProgressBarType.none )
        {
            return;
        }

        if ( this.m_ConstructingAni != null )
        {
            this.m_ConstructingAni.MainLoop();
        }
    }

    public GetSortPosY():number
    {
        if ( this.GetFuncType() == Global.eObjFunc.car )
        {
            this.SetSortPosY();
        }
        return this.m_fSortPosY;
    }

    public SetSortPosY():void
    {
        if ( this.GetFuncType() == Global.eObjFunc.building )
        {
            if ( this.GetSizeType() == Global.eObjSize.size_2x2 )
            {
               this.m_fSortPosY = this.GetLocationGrid().y - CDef.s_nTileHeight;
              
            }
            else if (this.GetSizeType() == Global.eObjSize.size_4x4)
            {
                this.m_fSortPosY = this.GetLocationGrid().y - CDef.s_nTileHeight * 2;
            }

        }
        else if( this.GetFuncType() == Global.eObjFunc.car )
        {
            this.m_fSortPosY = this.y;
            
        }


    }

    public SetDebugInfo( szDebugInfo:string ):void{
        this.m_txtDebugInfo.text = szDebugInfo;

    }

    public IsBuildinSpecial():boolean
    {
        return this.m_eBuildinSpecial != Global.eOperate.do_nothing;
    }

    public SetBuildinSpecial( eType:Global.eOperate ):void
    {
        this.m_eBuildinSpecial = eType; 
    }

    public GetBuildinSpecial(  ):Global.eOperate
    {
        return this.m_eBuildinSpecial; 
    }

    public HasBuilding():boolean
    {
        return this.m_bHasBuilding;

    }


    public SetHasBuilding( bHas:boolean ) :void
    {
        this.m_bHasBuilding = bHas;
    }

    public SetLotLevel( nLevel:number ):void
    {
        this.m_nLotLevel = nLevel;
    }

    public GetLotLevel( ):number
    {
        return this.m_nLotLevel;
    }

    public SetLotProperty( eLotProperty:Global.eLotPsroperty ):void
    {
        this.m_eLotProperty = eLotProperty;
    }

    public GetLotProperty():Global.eLotPsroperty
    {
        return this.m_eLotProperty;
    }

    public SetHistorical(bHistorical:boolean):void
    {
        this.m_bHistorical = bHistorical;
    }

    public GetHistorical():boolean
    {
        return this.m_bHistorical;
    }

    public GetBuildingOnTile():CObj
    {
        return this.m_objBuildingOnTile;
    }

    protected m_objBuildingOnTile:CObj = null;
    public SetBuilding( data:CConfigBuilding ):void
    {
        /*
        this.SetTexture( RES.getRes( data.szResName ) );
        this.m_BuildingData = data;

        this.anchorOffsetX  = this.width * 0.5;
        this.anchorOffsetY  = this.height;

        this.scaleX = 0.75;
        this.scaleY = 0.75;
        */
        this.m_BuildingData = data; 

        if ( this.m_objBuildingOnTile == null )
        {
             this.m_objBuildingOnTile = CResourceManager.NewObj();
        }
        this.m_objBuildingOnTile.m_BuildingData = data; 
         this.m_objBuildingOnTile.SetMyTile( this );
      
         //this.m_objBuildingOnTile.SetTexture( RES.getRes( data.szResName ) );  
         this.m_objBuildingOnTile.SetBuildingTypeAndIndex( this.GetLotProperty(), this.GetSizeType(),  data.nIndexOfThisType );


         // this.m_objBuildingOnTile.anchorOffsetX = this.m_objBuildingOnTile.width / 2;
         // this.m_objBuildingOnTile.anchorOffsetY = this.m_objBuildingOnTile.height;                        
         this.m_objBuildingOnTile.SetFuncType( Global.eObjFunc.building );
         this.m_objBuildingOnTile.SetSizeType( Global.eObjSize.size_2x2 );
         this.m_objBuildingOnTile.SetPos( this.GetPos()["x"],  this.GetPos()["y"] - CDef.GetBuildingOffsetY( this.GetSizeType() ));
         //var posX:number = this.GetPos()["x"];
         //var posY:number = this.m_objBuildingOnTile.GetPos()["y"] - CDef.GetBuildingOffsetY_Middle(this.GetSizeType() ) ;
         //this.m_objBuildingOnTile.SetRealAnchorY_Middle(posY );
        // Main.s_CurTown.InsertToVertObjList( this.m_objBuildingOnTile );
    

    }

    protected m_objMyTile:CObj = null;
    public SetMyTile( tile:CObj ):void
    {
        this.m_objMyTile = tile;
    }

    public GetMyTile(  ):CObj
    {
        return this.m_objMyTile;
    }

    public GetBuilding():CConfigBuilding
    {
        return this.m_BuildingData;
    }

    public GetTreeResId():number
    {
        return 0;
    }

    protected handleTapGarage( evt:egret.TouchEvent ):void
    {
                 CUIManager.SetUiVisible( Global.eUiId.garage, true );
                 Main.s_CurTown.UpdateGarageStatus();
    }

    public SetSpeed( fSpeed:number ):void
    {
        this.m_fSpeed = fSpeed;
    }

    public GetSpeed():number
    {
        return this.m_fSpeed;
    }

    public GetTime():number
    {
        return this.m_fTime;
    }

    public SetTime( fTime:number ):void
    {
        this.m_fTime = fTime;
    }

    public GetParams(  ):Object
    {
        return this.m_objectParams;
    }

    public SetColor( r:number, g:number, b:number ):void
    {
        this.m_bmpMainPic.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255( r, g, b )];
    }

    public SetParams( key:string, val:any ):void
    {
        this.m_objectParams[key] = val;
    }

    public GetParamsByKey( key:string ):any
    {
        return this.m_objectParams[key];
    }

    public SetMainPicVisible( bVisible:boolean ):void
    {
        this.m_bmpMainPic.visible = bVisible;
    }

    protected m_BelongToDistrict:CDistrict = null;
    public SetDistrict( district:CDistrict ):void
    {
        this.m_BelongToDistrict = district;
    }

    public GetDistrict(  ):CDistrict
    {
        return this.m_BelongToDistrict;
    }



    public SetMoveParams( vecDestPos:CVector, vecCurPos:CVector, nTime:number ):void
    {
        this.m_vecDestPos.x = vecDestPos.x;
        this.m_vecDestPos.y = vecDestPos.y;

        this.m_vecSpeed.x = ( this.m_vecDestPos.x  - vecCurPos.x ) / nTime * CDef.s_fFixedDeltaTime;
        this.m_vecSpeed.y = ( this.m_vecDestPos.y  - vecCurPos.y ) / nTime * CDef.s_fFixedDeltaTime;


    }

    public GetDestPos():CVector
    {
        return this.m_vecDestPos;
    }

    public MoveToDest():boolean
    {
        this.x += this.m_vecSpeed.x;
        this.y += this.m_vecSpeed.y;

        var bEndX:boolean = false;
        var bEndY:boolean = false;

        if ( this.m_vecSpeed.x > 0)
        {
            if ( this.x >= this.m_vecDestPos.x )
            {
                bEndX = true;
            }
        }
        else if ( this.m_vecSpeed.x < 0)
        {
            if ( this.x <= this.m_vecDestPos.x )
            {
                bEndX = true;
            }
        }
        else
        {
            bEndX = true;
        }

        if ( this.m_vecSpeed.y > 0)
        {
            if ( this.y >= this.m_vecDestPos.y )
            {
                bEndY = true;
            }
        }
        else if ( this.m_vecSpeed.y < 0)
        {
            if ( this.y <= this.m_vecDestPos.y )
            {
                bEndY = true;
            }
        }
        else
        {
            bEndY = true;
        }

        if ( bEndX )
        {
            this.x = this.m_vecDestPos.x;
        }

        if ( bEndY )
        {
            this.y = this.m_vecDestPos.y;
        }

        if ( bEndX && bEndY )
        {
            return true;
        }

        return false;
    }

    protected m_fRealAnchorY_Bottom:number = 0;
    public GetRealAnchorY_Bottom( ):number
    {
        
        return this.m_fRealAnchorY_Bottom;
      
    }

    protected m_fRealAnchorY_Middle:number = 0;
    public GetRealAnchorY_Middle(  ):number
    {
        return this.m_fRealAnchorY_Middle;
    }

    public SetRealAnchorY_Bottom( fRealAnchorY_Bottom:number ):void
    {
        
     this.m_fRealAnchorY_Bottom = fRealAnchorY_Bottom;
      
    }

    protected m_dicRealAnchorMiddle:Object = new Object();
    public SetRealAnchorMiddle( posX, posY ):void
    {
        this.m_dicRealAnchorMiddle["x"] = posX;
        this.m_dicRealAnchorMiddle["y"] = posY;
    }

    public GetRealAnchorMiddle():Object
    {
        return this.m_dicRealAnchorMiddle;
    }

    public SetRealAnchorY_Middle(  fRealAnchorY_Middle:number ):void
    {
      this.m_fRealAnchorY_Middle = fRealAnchorY_Middle;
    }

    protected m_objDebugPoint:CObj = null;
    public SetDebugPoint( debug_point:CObj ):void
    {
        this.m_objDebugPoint = debug_point;
    }

    public GetDebugPoint(  ):CObj
    {
        return this.m_objDebugPoint;
    }

    public CalculateMyCorners():void
    {

    }

    public SetSpecialBuildingByType( eType:Global.eOperate ):void
    {
        var url:string = CResourceManager.url;
        url += "Building/";
        switch( eType )
        {
            case Global.eOperate.add_garage:
            {
                url += "chezhan.png";
            }
            break;
           
            case Global.eOperate.add_cityhall:
            {
                url += "shizhengting.png";
            }
            break;
                       
            case Global.eOperate.add_bank:
            {
                url += "yinhang.png";
            }
            break;
        } // end switch

        this.SetResByUrl( url );


    }

    // type: 0 - 住宅  1 - 商业  2 - 公共服务
    public SetBuildingTypeAndIndex( nPropertyType:Global.eLotPsroperty, nSizeType:Global.eObjSize, nIndex:number ):void
    {
        var url:string = CResourceManager.url;
        url += "Building/";
        switch( nPropertyType )
        {
            case Global.eLotPsroperty.residential:
            {
                url += "residential/";
            }
            break;
          
            case Global.eLotPsroperty.business:
            {
                url += "business/";
            }
            break;
                       
            case Global.eLotPsroperty.service:
            {
                url += "service/";
            }
            break;
        } // end switch

         switch( nSizeType )
        {
            case Global.eObjSize.size_2x2:
            {
                url += "2x2/";
            }
            break;
          
            case Global.eObjSize.size_4x4:
            {
                url += "4x4/";
            }
            break;
         
        } // end switch

        url += nIndex + ".png";

        this.SetResByUrl( url );


       //  var posX:number = this.GetPos()["x"];  
       // var posY:number = this.GetPos()["y"] - CDef.GetBuildingOffsetY_Middle(this.GetMyTile().GetSizeType() ) ;
       // this.SetRealAnchorY_Middle(posY );
       this.visible = false;
         Main.s_CurTown.InsertToVertObjList( this, true );
         

    } // end SetBuildingTypeAndIndex

    protected m_dicUrlRes:Object = new Object();
    protected m_szCurUrl:string = "";
    public SetResByUrl( url:string ):void
    {

         RES.getResByUrl( url ,this.onLoadResComplete, this,RES.ResourceItem.TYPE_IMAGE);
    }

    protected onLoadResComplete(event:any):void {
      
        var tex: egret.Texture = <egret.Texture>event;
        this.SetTexture( tex );

        this.visible = true;

        this.anchorOffsetX = this.m_bmpMainPic.width / 2;
        this.anchorOffsetY = this.m_bmpMainPic.height;      
          
        var posX:number = this.GetPos()["x"];  
        var posY:number = this.GetPos()["y"] - CDef.GetBuildingOffsetY_Middle(this.GetMyTile().GetSizeType() ) ;
        this.SetRealAnchorY_Middle(posY );
         Main.s_CurTown.InsertToVertObjList( this, true );
         

 // 播放一个“建造成功”的帧动画
         var effect:CFrameAni = CFrameAniManager.NewEffect();
         var szResUrl:string = CResourceManager.url + "Effect/WhiteSmoke/000";
         effect.SetParams( szResUrl, 18, 0, false,true );
         effect.Play();
         this.addChild( effect );
         effect.x = this.width / 2;
         effect.y = 100;
         if ( this.GetSizeType() == Global.eObjSize.size_2x2 )
         {
            effect.scaleX = 1.5;
            effect.scaleY = 1.5;
         }
         else
         {
              effect.scaleX =3;
            effect.scaleY = 3;
         }

/*
        var red_point:CObj = new CObj();
        Main.s_CurTown.m_containerDebug.addChild( red_point );
        red_point.SetTexture( RES.getRes( "red_point_png") );
        red_point.anchorOffsetX = red_point.width / 2;
        red_point.anchorOffsetY = red_point.height ;
        red_point.SetPos( posX, posY );  

*/


    }


} // end class CObj