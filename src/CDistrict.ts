class CDistrict extends egret.DisplayObjectContainer {

    static s_aryTempObjs:Array<CObj> = new Array<CObj>();

    public m_containerGrids:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public m_containerObjs:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public m_containerRoads:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public m_containerTrees:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public m_containerArrows:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public m_containerTurbine:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public m_containerProgressbar:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public m_containerDiBiao:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public m_containerTiaoZi:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
         public m_containerTile:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
          public m_containerDebug:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();



    public  m_dicGrids:Object = new Object();
    public m_aryAllGrids:Array<CGrid> = new Array<CGrid>();

    protected m_nId:number = 1;

    public constructor() {
        super();

if ( CDef.EDITOR_MODE )
{
   this.addChild( this.m_containerGrids);
}
 
        this.addChild( this.m_containerDiBiao );
        this.addChild( this.m_containerRoads );
        this.addChild( this.m_containerTile );

        this.addChild( this.m_containerTrees );
        this.addChild( this.m_containerObjs );
        this.addChild( this.m_containerTurbine );
        this.addChild( this.m_containerArrows );
        this.addChild( this.m_containerProgressbar );
        this.addChild( this.m_containerTiaoZi );

        this.CreateGrids();

        
        this.addChild( this.m_containerDebug );

    }

    public SetId( nId:number ):void
    {
        this.m_nId = nId;
    }

    public GetId():number
    {
        return this.m_nId;
    }

    // 12.1_Load
    public LoadTownData( szData:string ):void
    {
            

            var aryData:string[] = szData.split( '|' );
   
    for ( var i:number = 0; i < aryData.length; i++ )
    {
       
        if ( aryData[i] == "" )
        {
                continue;
        }
        var op:number = i;
        var szGridIds:string = aryData[i];

        var aryGridIs:string[] = szGridIds.split( ',' );
        for ( var j:number = 0; j < aryGridIs.length; j++ )
       {
                switch( op)
                {
                     

                        case Global.eOperate.add_tree:
                        {
                               var aryParams:string[] = aryGridIs[j].split( '_' );
                               var nTreeResId:number = (Number)(aryParams[0]);
                               var fPosInTownX:number = (Number)(aryParams[1]);
                               var fPosInTownY:number = (Number)(aryParams[2]);
                               Main.s_CurTown.DoAddTree( nTreeResId, fPosInTownX, fPosInTownY );
                        }
                        break;

                        case Global.eOperate.add_windturbine:
                        {
                                var aryParams:string[] = aryGridIs[j].split( '_' );
                               var fPosInTownX:number = (Number)(aryParams[0]);
                               var fPosInTownY:number = (Number)(aryParams[1]);
                               Main.s_CurTown.DoAddWindTurbine(  fPosInTownX, fPosInTownY ); 
                        }
                        break;

                        default:
                        {
                                var aryGridIndex:string[] = aryGridIs[j].split( '_' );
                                var szIndexX:string = aryGridIndex[0]; 
                                var szIndexY:string = aryGridIndex[1];
                                var szParam0:string = "";
                                var szParam1:string = "";
                                if ( aryGridIndex.length >= 3 )
                                {
                                         szParam0 = aryGridIndex[2];
                                         szParam1 = aryGridIndex[2];
                                }
                      
                                var grid:CGrid = this.GetGridByIndex( szIndexX, szIndexY );
                         
                                Main.s_CurTown.DoAddObj( grid, op,szParam0, szParam1 );
                                
                        }
                        break;
                }





        } // end j
    } // end i

    } // end LoadTownData


public ClearAll():void
{
  
  // 清除Obj
              for ( var i:number = this.m_containerObjs.numChildren - 1; i >= 0; i-- )
              {
                      var obj:CObj = this.m_containerObjs.getChildAt(i) as CObj;
                      if ( obj.GetFuncType() == Global.eObjFunc.car )
                      {
                        var jinbi:CJinBi = obj.getChildByName( "jinbi" ) as CJinBi;
                        if ( jinbi != null && jinbi != undefined )
                        {
                                CJinBiManager.DeleteJinBi( jinbi );
                        }
                      }
                      obj.ClearBoundGrid();
                      if ( obj.GetFuncType() == Global.eObjFunc.car )
                      {
                        CAutomobileManager.DeleteCar( obj as CAutomobile );
                      }
                      else
                      {
                        CResourceManager.DeleteObj( obj );
                      }
              }

              // 清除地块
              for ( var i:number = this.m_containerTile.numChildren - 1; i >= 0; i-- )
              {
                        var obj:CObj = this.m_containerTile.getChildAt(i) as CObj;
                        CResourceManager.DeleteObj( obj );
              }

              // 清除路面
              for ( var i:number = this.m_containerRoads.numChildren - 1; i >= 0; i-- )
              {
                      var obj:CObj = this.m_containerRoads.getChildAt(i) as CObj;
                       obj.ClearBoundGrid();
                      CResourceManager.DeleteObj( obj );
              } 

              // 清除树木
              for ( var i:number = this.m_containerTrees.numChildren - 1; i >= 0; i-- )
              {
                      var obj:CObj = this.m_containerTrees.getChildAt(i) as CObj;
                      CResourceManager.DeleteObj( obj );
              } 

              // 清除地表
              for ( var i:number = this.m_containerDiBiao.numChildren - 1; i >= 0; i-- )
              {
                      var obj:CObj = this.m_containerDiBiao.getChildAt(i) as CObj;
                      CResourceManager.DeleteObj( obj );
              } 

              // 清除风车 
              for ( var i:number = this.m_containerTurbine.numChildren - 1; i >= 0; i-- )
              {
                      var wind:CWindTurbine = this.m_containerTurbine.getChildAt(i) as CWindTurbine;
                      CResourceManager.DeleteWindTurbine( wind );
              } 

               // 清空所有格子上的信息
              for ( var i:number = this.m_containerGrids.numChildren - 1; i >= 0; i-- )
              {
                      var grid:CGrid = this.m_containerGrids.getChildAt(i) as CGrid ;
                      grid.ClearBoundObj();
                      
                      //  poppin to do
                      // 格子应该回收到对象池中复用
              }

  // 清除进度条
              if ( this.m_containerProgressbar )
              {
                for ( var i:number = this.m_containerProgressbar.numChildren - 1; i >= 0; i-- )
                {
                        var bar:CUIBaseProgressBar = this.m_containerProgressbar.getChildAt(i) as CUIBaseProgressBar ;
                        CResourceManager.DeleteProgressBar( bar );
                } 
              }
            
}

public  ResetAll():void
{

}

public GetGridByIndex( nIndexX:string, nIndexY:string ):CGrid
{
        var szKey:string = nIndexX + "," + nIndexY;
        var grid:CGrid = this.m_dicGrids[szKey];   
        if ( grid == undefined )
        {
               grid = null; 
        }   
        return grid;
}

// right here 12.1
public CreateGrids():void
{

       

        var nWidth:number = CDef.s_nTileWidth;
        var nHeight:number = CDef.s_nTileHeight;
        var nHalfWidth:number = nWidth/ 2;
        var nHalfHeight:number = nHeight / 2;


// 规定：偶数行的列号都是偶数；奇数行的列号都是奇数
for ( var i:number = -CDef.s_nGridNumY; i <= CDef.s_nGridNumY; i++ ) // y轴方向（行）
{
        var nShit = Math.abs(i);
        for ( var j:number = -CDef.s_nGridNumX + nShit; j <= CDef.s_nGridNumX - nShit; j++ )// x轴方向（列）
        {
                var posX:number = 0;
                var posY:number = 0;
                
                 if ( i % 2 == 0 ) // 偶数行
                  {
                       if ( j % 2 != 0  ) // 不是偶数列就跳过。因为地图的格子机制规定偶数行只有偶数编号的列
                       {
                               continue;
                       }

                       posX = ( j / 2 ) * nWidth;
                       posY = i * nHalfHeight; 
                  }
                  else// 奇数行（在奇数行列处生成逻辑矩形）
                  {
                        if ( j % 2 == 0  ) // 是偶数列就跳过。因为地图的格子机制规定奇数行只能有奇数编号的列
                       {
                               continue;
                       }
                        posX = ( j / 2 ) * nWidth;
                        posY = i * nHalfHeight; 
                  }

                  var grid:CGrid = new CGrid(); // to youhua 这里应该做对象池，不应该一概直接new
                  
          
                  if ( true )
                  {
                 
                        this.m_containerGrids.addChild( grid );
                  }
             
                  grid.anchorOffsetX = nHalfWidth;
                  grid.anchorOffsetY = nHeight;//nHalfHeight; //;CDef.s_nTileHeight;
                  //grid.DrawRhombus( CDef.s_nTileWidth, CDef.s_nTileHeight, nHalfWidth, nHalfHeight);
                  //grid.x = posX;
                  //grid.y = posY;


                  grid.SetPos( posX, posY );



                  // j是x轴坐标，i是y轴坐标
                  var szKey:string = j + "," + i;
                  grid.SetText( szKey);
                  grid.SetId( j,i );
                  this.m_dicGrids[szKey] = grid;
                  this.m_aryAllGrids.push( grid );

               
        } // end for j
} // end for i    
   
   



} // end CreateGrids


        public BuildingAndUpgradingLoop( bBuildingLandLoop:boolean ):void
        {
                 for ( var i:number = 0; i < this.m_containerObjs.numChildren; i++ )
                {
                    var obj:CObj = this.m_containerObjs.getChildAt(i) as CObj;
                    if ( obj.GetFuncType() != Global.eObjFunc.building )
                    {
                            continue;
                    }
                    obj.SetDistrict( this );
                    if ( obj.GetFuncType() != Global.eObjFunc.building && obj.GetFuncType() != Global.eObjFunc.tile  ) // 稍后路面可以放在另一个显示列表中，反正不参与遮挡排序，其职能与建筑物也不同
                    {
                            continue;
                    }
                   
                    obj.ProgressBarLoop();

                    if ( bBuildingLandLoop )
                    {
                        obj.BuildingLandLoop();
                    }
                } //end for vert_obj


                for ( var i:number = 0; i < this.m_containerTile.numChildren; i++ )
                {
                    var obj:CObj = this.m_containerTile.getChildAt(i) as CObj;
                    if ( obj.GetFuncType() != Global.eObjFunc.tile )
                    {
                            continue;
                    }
                    obj.SetDistrict( this );
                    if ( obj.GetFuncType() != Global.eObjFunc.building && obj.GetFuncType() != Global.eObjFunc.tile  ) // 稍后路面可以放在另一个显示列表中，反正不参与遮挡排序，其职能与建筑物也不同
                    {
                            continue;
                    }
                   
                    obj.ProgressBarLoop();

                    if ( bBuildingLandLoop )
                    {
                        obj.BuildingLandLoop();
                    }
                }
        }

        public GetCars():void
        {
                CDistrict.s_aryTempObjs.length = 1;
                for ( var i:number = 0; i < this.m_containerObjs.numChildren; i++ )
                {

                }
        }

} // end class