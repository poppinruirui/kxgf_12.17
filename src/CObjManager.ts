
class CObjManager extends egret.DisplayObjectContainer {

    public static s_aryRoadResName:string[] = 
    [
        "blueRoad0_png",
        "blueRoad1_png"

    ];
   

    public static NewObj():CObj
    {
        var obj:CObj = null;



        if ( CObjManager.m_lstRecycledObjs.numChildren > 0 )
        {
            obj = CObjManager.m_lstRecycledObjs.getChildAt(0) as CObj;
            obj.visible = true;
            obj.Reset();
            CObjManager.m_lstRecycledObjs.removeChildAt(0);
        }
        else
        {
            obj = new CObj();
        }

        return obj;
    }

    

    protected static s_nObjGuid:number = 0;
    public static NewObjByResName( szResName:string ):CObj
    {
        var obj:CObj = new CObj();// CObjManager.NewObj();
        var tex:egret.Texture =  RES.getRes( szResName );
        obj.SetTexture(tex );

        obj.SetGuid( this.s_nObjGuid );
        this.s_nObjGuid++;
        
        return obj;
    }

    public static m_lstRecycledObjs:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public static DeleteObj( obj:CObj ):void
    {
        obj.visible = false;
        CObjManager.m_lstRecycledObjs.addChild( obj );
    }
    


} // end CObjManager