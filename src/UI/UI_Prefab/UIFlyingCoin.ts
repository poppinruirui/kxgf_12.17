
class UIFlyingCoin extends eui.Component {

    protected m_uiContainer:eui.Component = new eui.Component();
    protected m_imgMain:eui.Image = null;

     protected m_fSession0_AX:number = 0;
     protected m_fSession0_AY:number = 0;

     protected m_fSession1_AX:number = 0;
     protected m_fSession1_AY:number = 0;

     protected m_fMiddlePosX:number = 0;
     protected m_fMiddlePosY:number = 0;

     protected m_fEndPosX:number = 0;
     protected m_fEndPosY:number = 0;

     protected m_aryAX:Array<number> = new Array<number>();
     protected m_aryAY:Array<number> = new Array<number>();
     protected m_aryStartPosX:Array<number> = new Array<number>();
     protected m_aryStartPosY:Array<number> = new Array<number>();
     protected m_aryEndPosX:Array<number> = new Array<number>();
     protected m_aryEndPosY:Array<number> = new Array<number>();

     protected m_bFlying:boolean = false;
     protected m_nStatus:number = 0;

     protected m_VX:number = 0;
     protected m_VY:number = 0;

   public constructor() {
        super();

        this.m_uiContainer.skinName = "resource/assets/MyExml/FlyingCoin.exml";
        this.addChild( this.m_uiContainer );

        this.m_imgMain = this.m_uiContainer.getChildByName( "imgMain" ) as eui.Image;

   } // end constructor

   public SetMiddlePos( fMiddlePosX:number, fMiddlePosY:number ):void
   {
        this.m_fMiddlePosX = fMiddlePosX;
        this.m_fMiddlePosY = fMiddlePosY;
   }

   public SetEndPos( fEndPosX:number, fEndPosY:number ):void
   {
       this.m_fEndPosX = fEndPosX;
       this.m_fEndPosY = fEndPosY;
   }

   public AddSessionParams( fAX:number,  fAY:number, fStartPosX:number, fStartPosY:number, fEndPosX:number, fEndPosY:number )  :void
   {
       this.m_aryAX.push(fAX);
       this.m_aryAY.push(fAY);
       this.m_aryStartPosX.push( fStartPosX );
       this.m_aryStartPosY.push( fStartPosY );
       this.m_aryEndPosX.push( fEndPosX );
       this.m_aryEndPosY.push( fEndPosY ); 
   }

   public BeginFlying():void
   {
       this.m_bFlying = true;
       this.m_nStatus = 0;

       this.x = this.m_aryStartPosX[0];
       this.y = this.m_aryStartPosY[0];

       this.m_VX = 0;
       this.m_VY = 0;

       this.visible = true;
   }


   public Moving():void
   {
       if ( !this.m_bFlying )
       {
           return;
       }

        this.m_VX += this.m_aryAX[this.m_nStatus] * CDef.s_fFixedDeltaTime;
        this.m_VY += this.m_aryAY[this.m_nStatus] * CDef.s_fFixedDeltaTime;

        this.x += this.m_VX * CDef.s_fFixedDeltaTime;
        this.y += this.m_VY * CDef.s_fFixedDeltaTime;

        var bEndX:boolean = false;
        var bEndY:boolean = false;
        if ( this.m_VX > 0 )
        {
            if ( this.x >= this.m_aryEndPosX[this.m_nStatus] )
            {
                bEndX = true;
            }
        }
        else if ( this.m_VX < 0 )
        {
            if ( this.x <= this.m_aryEndPosX[this.m_nStatus] )
            {
                bEndX = true;
            }
        }
        else
        {
             bEndX = true;
        }

        if ( this.m_VY > 0 )
        {
            if ( this.y >= this.m_aryEndPosY[this.m_nStatus] )
            {
                bEndY = true;
            }
        }
        else if ( this.m_VY < 0 )
        {
            if ( this.y <= this.m_aryEndPosY[this.m_nStatus] )
            {
                bEndY = true;
            }
        }
        else
        {
             bEndY = true;
        }

        if ( bEndX && bEndY )
        {
            this.m_nStatus++;
            this.m_VX = 0;
            this.m_VY = 0;

            if ( this.m_nStatus >= 2 )
            {
                this.EndFly();
            }
        }

   }


   

   protected EndFly():void
   {
       this.m_bFlying = false;
       this.visible = false;

       CSoundManager.PlaySE( Global.eSE.small_coin );
   }


} // end class

