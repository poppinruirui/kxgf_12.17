class CCosmosImage extends eui.Component {

     protected m_progressBar:CUIBaseProgressBar = null;

     public constructor() {
        super();

        this.addEventListener( egret.TouchEvent.TOUCH_BEGIN, this.onTapMeDown, this );
        this.addEventListener( egret.TouchEvent.TOUCH_END, this.onTapMeUp, this );
      // this.addEventListener( egret.TouchEvent.TOUCH_MOVE, this.onTapMeCancel, this );
     }

     public SetAlpha( fAlpha:number ):void
     {
           this.alpha = fAlpha;
     }

     public SetEnabled( bEnabled:boolean ):void
     {
          this.enabled =  bEnabled; // eui的控件设置touchEnabl属性e没什么卵用，要设置enabled属性
          if ( bEnabled )
          {
                this.filters = null;
                this.alpha = 1;
          }
          else
          {
                this.filters = [ CColorFucker.GetDisabledGray() ];
                this.alpha = 0.8;
          }

     }

     public SetExml( source:string ):void
     {
         this.skinName = source;
     } 

     protected onTapMeDown( evt:egret.TouchEvent ):void
     {
            if ( this.enabled )
           {
           this.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255( 190, 190, 190 ) ];
           }
     }

    protected onTapMeCancel( evt:egret.TouchEvent ):void
     {
            if ( this.enabled )
           {
            this.filters = null;//[ CColorFucker.GetColorMatrixFilterByRGBA(1,1,1,1) ];
           }
     }

     protected onTapMeUp( evt:egret.TouchEvent ):void
     {
           if ( this.enabled )
           {
            this.filters = null;//[ CColorFucker.GetColorMatrixFilterByRGBA(1,1,1,1) ];
            } 
     }

     public SetTextVisible( nIndex:number, bVisible:boolean ):void
     {
             var txt:eui.Label = this.getChildByName( "txt" + nIndex ) as eui.Label;
             txt.visible = bVisible;
     }

     public SetTextColor( nIndex:number, color:number ):void
     {
            var txt:eui.Label = this.getChildByName( "txt" + nIndex ) as eui.Label;
            txt.textColor = color;
     }

     public SetTextSize(nIndex:number, nSize:number):void
     {
            var txt:eui.Label = this.getChildByName( "txt" + nIndex ) as eui.Label;
            txt.size = nSize;
     }

     public SetText( nIndex:number, szContent:string ):void
     {
            var txt:eui.Label = this.getChildByName( "txt" + nIndex ) as eui.Label;
            txt.text = szContent;
     }

     public SetImageTexture( nIndex:number, tex:egret.Texture ):void
     {
            var img:eui.Image = this.getChildByName( "img" + nIndex ) as eui.Image;
            img.texture  = tex;
     }

     public SetImageAnchor(nIndex:number, nType:number):void
     {
      var img:eui.Image = this.getChildByName( "img" + nIndex ) as eui.Image;
      img.anchorOffsetX = img.width * 0.5;
      img.anchorOffsetX = img.height * 0.5;
     }

     public SetImageSize( nIndex:number, nWidth:number, nHeight:number ):void
     {
             var img:eui.Image = this.getChildByName( "img" + nIndex ) as eui.Image;
             img.width = nWidth;
             img.height = nHeight;
     }

     public SetImageVisible(nIndex:number, bVisible:boolean):void
     {
            var img:eui.Image = this.getChildByName( "img" + nIndex ) as eui.Image;
            img.visible = bVisible;
     }

     public SetImageAlpha(nIndex:number, fAlpha:number):void
     {
            var img:eui.Image = this.getChildByName( "img" + nIndex ) as eui.Image;
            img.alpha = fAlpha;
     }

     public SetImageColor_New( nIndex:number, r:number, g:number, b:number ):void
     {
           var img:eui.Image = this.getChildByName( "img" + nIndex ) as eui.Image;
           CColorFucker.SetImageColor(img, r, g, b );
     }

     public SetImageColor_255( tex:egret.Texture, nIndex:number, r:number, g:number, b:number, a:number = 255, bFilter:boolean = true ):void
     {
            r /= 255;
            g /= 255;
            b /= 255;
            a /= 255;
            this.SetImageColor(tex, nIndex, r, g, b, a, bFilter);
     }

     public SetImageColor( tex:egret.Texture, nIndex:number, r:number, g:number, b:number, a:number = 1, bFilter:boolean = true ):void
     {
           var img:eui.Image = this.getChildByName( "img" + nIndex ) as eui.Image;
           if ( bFilter )
           {
            img.filters = [ CColorFucker.GetColorMatrixFilterByRGBA( r,g,b,a ) ];
           }
           else
           {
                 img.filters = null;
           }
           img.alpha = a;

           if ( tex != null )
           {
   
                 img.source = tex;
                 
           }
     }

     public SetLabelVisible( nIndex:number, bVisible:boolean ):void
     {
             var txt:eui.Label = this.getChildByName( "txt" + nIndex ) as eui.Label;
             txt.visible = bVisible;
     }
     

     public SetLabelContent( nIndex:number, szContent:string ):void
     {
           var txt:eui.Label = this.getChildByName( "txt" + nIndex ) as eui.Label;
           txt.text = szContent;
     }

     public UseColorSolution( nSolutionId:number = 0 ):void
     {
           switch(nSolutionId)
           {
                 case 0:
                 {
                  var tex:egret.Texture = RES.getRes( CDef.RES_1x1 );
                        this.SetImageColor( tex, 0, 1,1,1 );
                        this.SetImageColor( tex, 1, 0.663,0.663,0.663 );
                        this.SetImageColor( tex, 2, 0.923,0.278,0.227 );
                        this.SetImageColor( tex, 3, 0.753,0.2,0.145 );
                 }
                 break;
                 
                 case 1:
                 {
                        var tex:egret.Texture = RES.getRes( CDef.RES_1x1 );
                        this.SetImageColor( tex, 0, 1,1,1 );
                        this.SetImageColor( tex, 1, 0.663,0.663,0.663 );
                        this.SetImageColor( tex, 2, 0.420,0.761,0.224 );
                        this.SetImageColor( tex, 3, 0.306,0.541,0.149 );
                 }
                 break;

                 case 2:
                 {
                        this.SetImageColor( tex, 0, 1,1,1 );
                        this.SetImageColor( tex, 1, 0.369,0.561,0.204 );
                        this.SetImageColor( tex, 2, 0.65,0.847,0.463 );
                        this.SetImageColor( tex, 3, 0.831,0.831,0.831 );
                        this.SetTextColor( 0, 0x5E8F34 );
                        this.SetTextColor( 1, 0x5E8F34 );
                        this.SetTextColor( 2, 0x666666 );
                 }
                 break;

                  case 3:
                 {
                        this.SetImageColor( tex, 0, 1,1,1 );
                        this.SetImageColor( tex, 1, 0.271,0.514,0.737 );
                        this.SetImageColor( tex, 2, 0.561,0.788,0.984 );
                        this.SetImageColor( tex, 3, 0.831,0.831,0.831 );
                        this.SetTextColor( 0, 0x4583BC );
                        this.SetTextColor( 1, 0x4583BC );
                        this.SetTextColor( 2, 0x666666 );
                 }
                 break;

                     case 4:
                 {
                        this.SetImageColor( tex, 0, 1,1,1 );
                        this.SetImageColor( tex, 1, 0.513,0.478,0.145 );
                        this.SetImageColor( tex, 2, 0.835,0.784,0.275 );
                        this.SetImageColor( tex, 3, 0.831,0.831,0.831 );
                        this.SetTextColor( 0, 0x837A25 );
                        this.SetTextColor( 1, 0x837A25 );
                        this.SetTextColor( 2, 0x666666 );
                 }
                 break;
            }

     }

     public CreateProgressBar( nImgIndex:number, nProgressBarExmlIndex:number, aryFilledbarColor:number[], aryFilledbarBgColor:number[] ):void
     {
           var imgTemp:eui.Image = this.getChildByName( "img2" ) as eui.Image;
           this.m_progressBar = new CUIBaseProgressBar( "resource/assets/MyExml/CProgressBar" + nProgressBarExmlIndex + ".exml" );
           this.addChild( this.m_progressBar );
           this.m_progressBar.x = imgTemp.x;
           this.m_progressBar.y = imgTemp.y;
           this.m_progressBar.SetFilledBarTotalWidth( imgTemp.width );
           this.m_progressBar.SetFilledBarTotalHeight( imgTemp.height );
           this.m_progressBar.SetFilledBarColor( aryFilledbarColor[0], aryFilledbarColor[1], aryFilledbarColor[2] );
           this.m_progressBar.SetFilledBarBgColor( aryFilledbarBgColor[0], aryFilledbarBgColor[1], aryFilledbarBgColor[2] );
           this.removeChild( imgTemp );

     }

     public SetProgressBarPercent( fPercent:number ):void
     {
           if ( this.m_progressBar == null  )
           {
                 return;
           }
           this.m_progressBar.SetPercentByPercent(fPercent);

     }

} // end class