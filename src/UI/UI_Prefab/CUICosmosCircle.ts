
class CUICosmosCircle extends eui.Component {

    protected m_shapeCircleInner:egret.Shape;
    protected m_shapeCircleOut:egret.Shape;
    protected m_imgIcon:eui.Image;


      public constructor() {
        super();

        this.m_shapeCircleInner = new egret.Shape();
        this.m_shapeCircleOut = new egret.Shape();
        this.m_imgIcon = new eui.Image();

        this.addChild( this.m_shapeCircleOut );
        this.addChild( this.m_shapeCircleInner );
        this.addChild( this.m_imgIcon );

      } // end constructor

      public SetParams( out_radius:number, out_thickness:number, out_outline_color:number, out_color:number,
                        inner_radius:number, inner_thickness:number, inner_outline_color:number, inner_color:number ):void
      {
        this.m_shapeCircleOut.graphics.clear();
        this.m_shapeCircleOut.graphics.lineStyle( out_thickness, out_outline_color );
        this.m_shapeCircleOut.graphics.beginFill( out_color );
        this.m_shapeCircleOut.graphics.drawCircle( 0, 0, out_radius );
        this.m_shapeCircleOut.graphics.endFill();

        this.m_shapeCircleInner.graphics.clear();
        this.m_shapeCircleInner.graphics.lineStyle( inner_thickness, inner_outline_color );
        this.m_shapeCircleInner.graphics.beginFill( inner_color );
        this.m_shapeCircleInner.graphics.drawCircle( 0, 0, inner_radius );
        this.m_shapeCircleInner.graphics.endFill();
      }

} // end class


