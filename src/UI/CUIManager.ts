class CUIManager extends egret.DisplayObjectContainer {

     public static s_dicUIs:Object = new Object();

     public static s_uiDeveloperEditor:CUIDeveloperEditor; // “车库”界面
     public static s_uiGarage:CUIGarage; // “车库”界面
     public static s_uiMainTitle:CUIMainTitle; // 主标题界面
     public static s_uiCPosButton:CUICPosBUtton; // C位按钮
     public static s_uiBuyLotPanel:CUIBuyLotPanel; // "买地"界面
     public static s_uiWindTurbine:CUIWindTurbine; // "风车"界面
     public static s_uiLotUpgrade:CUIUpgeadeLot;   // "地块升级"界面
     public static s_uiRevertLotConfirm:CUIRevertLotConfirm;   // "推平确认"界面
     public static s_uiBoost:CUIBoost;   // "加速"界面
     public static s_uiCityHall:CUICityHall;   // "市政厅"界面
     public static s_uiBank:CUIBank;   // 银行
     public static s_uiBankBack:CUIBankBack;   // 返回游戏时银行收钱界面
     public static s_uiSpecialPanel:CUISpecailPanel;   // "特殊建筑"面板
     public static s_uiToNextCityPanel:CUIToNextCity;   // "升级到下一级城市"面板
     public static s_uiWelcome:CUIWelcome; // “欢迎界面”
     public static s_uiDistrict:CUIDistrict; // “分区界面”

     public static s_containerUIs:egret.DisplayObjectContainer;

     

     public constructor() {
        super();

    
     } // end constructor

     public static Init():void
     {   
       


        CUIManager.s_uiMainTitle = new CUIMainTitle();
        CUIManager.s_containerUIs.addChild( CUIManager.s_uiMainTitle );
        CUIManager.s_dicUIs[Global.eUiId.main_title] = CUIManager.s_uiMainTitle;

 CUIManager.s_uiGarage = new CUIGarage();
        CUIManager.s_containerUIs.addChild( CUIManager.s_uiGarage );
        CUIManager.s_uiGarage.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.garage] = CUIManager.s_uiGarage;


        CUIManager.s_uiCPosButton = new CUICPosBUtton();
        CUIManager.s_containerUIs.addChild( CUIManager.s_uiCPosButton );
        CUIManager.s_dicUIs[Global.eUiId.bottom_buttons] = CUIManager.s_uiCPosButton;

        CUIManager.s_uiDeveloperEditor = new CUIDeveloperEditor();
        CUIManager.s_containerUIs.addChild( CUIManager.s_uiDeveloperEditor );
        CUIManager.s_uiDeveloperEditor.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.developer_editor] = CUIManager.s_uiDeveloperEditor;

        CUIManager.s_uiBuyLotPanel = new CUIBuyLotPanel();
        CUIManager.s_containerUIs.addChild( CUIManager.s_uiBuyLotPanel );
        CUIManager.s_uiBuyLotPanel.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.buy_lot_panel] = CUIManager.s_uiBuyLotPanel;

        CUIManager.s_uiWindTurbine = new CUIWindTurbine();
        CUIManager.s_containerUIs.addChild( CUIManager.s_uiWindTurbine );
        CUIManager.s_uiWindTurbine.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.wind_turbine] = CUIManager.s_uiWindTurbine;

        CUIManager.s_uiLotUpgrade = new CUIUpgeadeLot();
        CUIManager.s_containerUIs.addChild( CUIManager.s_uiLotUpgrade );
        CUIManager.s_uiLotUpgrade.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.lot_upgrade] = CUIManager.s_uiLotUpgrade;


        CUIManager.s_uiRevertLotConfirm = new CUIRevertLotConfirm();
        CUIManager.s_containerUIs.addChild( CUIManager.s_uiRevertLotConfirm );
        CUIManager.s_uiRevertLotConfirm.visible = false;
        CUIManager.s_dicUIs[Global.eUiId.revert_lot_confirm] = CUIManager.s_uiRevertLotConfirm;

        CUIManager.s_uiBoost = new CUIBoost();
        CUIManager.s_containerUIs.addChild( CUIManager.s_uiBoost );
        CUIManager.s_uiBoost.visible = false; 
        CUIManager.s_dicUIs[Global.eUiId.boost_panel] = CUIManager.s_uiBoost;

        
        CUIManager.s_uiCityHall = new CUICityHall();
        CUIManager.s_containerUIs.addChild( CUIManager.s_uiCityHall );
        CUIManager.s_uiCityHall.visible = false; 
        CUIManager.s_dicUIs[Global.eUiId.city_hall] = CUIManager.s_uiCityHall;
        
        CUIManager.s_uiBank = new CUIBank();
        CUIManager.s_containerUIs.addChild( CUIManager.s_uiBank );
        CUIManager.s_uiBank.visible = false; 
        CUIManager.s_dicUIs[Global.eUiId.bank] = CUIManager.s_uiBank;

        CUIManager.s_uiBankBack = new CUIBankBack();
        CUIManager.s_containerUIs.addChild( CUIManager.s_uiBankBack );
        CUIManager.s_uiBankBack.visible = false; 
        CUIManager.s_dicUIs[Global.eUiId.bank_back] = CUIManager.s_uiBankBack;


        CUIManager.s_uiSpecialPanel = new CUISpecailPanel();
        CUIManager.s_containerUIs.addChild( CUIManager.s_uiSpecialPanel );
        CUIManager.s_uiSpecialPanel.visible = false; 
        CUIManager.s_dicUIs[Global.eUiId.special_panel] = CUIManager.s_uiSpecialPanel;

        CUIManager.s_uiToNextCityPanel = new CUIToNextCity();
        CUIManager.s_containerUIs.addChild( CUIManager.s_uiToNextCityPanel );
         CUIManager.s_uiToNextCityPanel.visible = false; 
        CUIManager.s_dicUIs[Global.eUiId.to_next_city] = CUIManager.s_uiToNextCityPanel;

        CUIManager.s_uiWelcome = new CUIWelcome();
        CUIManager.s_containerUIs.addChild( CUIManager.s_uiWelcome );
         CUIManager.s_uiWelcome.visible = false; 
        CUIManager.s_dicUIs[Global.eUiId.welcome] = CUIManager.s_uiWelcome;



        CUIManager.s_uiDistrict = new CUIDistrict();
        CUIManager.s_containerUIs.addChild( CUIManager.s_uiDistrict );
        CUIManager.s_uiDistrict.visible = false; 
        CUIManager.s_dicUIs[Global.eUiId.district] = CUIManager.s_uiDistrict;


        CUIManager.s_containerUIs.addChild( CGreenMoneymanager.s_lstContainerFlyingGreen );


     }

      static s_bUIShowing:boolean = false;
      public static SetUiVisible( eId:Global.eUiId, bVisible:boolean ):void
      {
          CUIManager.s_dicUIs[eId].visible = bVisible;

          if ( eId != Global.eUiId.bottom_buttons  )
          {
            CUIManager.s_dicUIs[Global.eUiId.bottom_buttons].visible = !bVisible;
          }

         CUIManager.s_bUIShowing = bVisible;

         if ( eId ==  Global.eUiId.garage)
         {
             (CUIManager.s_dicUIs[eId] as CUIGarage ).ResetScrollPosition();
             
         }

      }

      public static IsUiShowing():boolean
      {
          return CUIManager.s_bUIShowing || CUIManager.s_uiDeveloperEditor.visible;
      }

     public static MainLoop():void
     {
         if ( CUIManager.s_uiMainTitle )
         {
           CUIManager.s_uiMainTitle.MainLoop();
         }

            CGreenMoneymanager.FixedUpdate();

            if ( CUIManager.s_uiBoost )
            {
                CUIManager.s_uiBoost.FixedUpdate();
            }

     } 

     public static MainLoop_1():void
     {
        if ( CUIManager.s_uiBoost && CUIManager.s_uiBoost.isShowing() )
        {
            CUIManager.s_uiBoost.UpdateFastForwardInfo();

            
        }
     } 


} // end class
