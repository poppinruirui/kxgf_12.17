class CUIDistrictSketch extends egret.DisplayObjectContainer {

      protected m_uiContainer:eui.Component = new eui.Component();
      protected m_imgDistrict:eui.Image;
      protected m_txtDistrictId:eui.Label;

      protected m_nId:number = 0;

      public constructor() {
        super();

        this.m_uiContainer.skinName = "resource/assets/MyExml/DistrictSketch.exml";
        this.addChild( this.m_uiContainer );

        this.m_txtDistrictId = this.m_uiContainer.getChildByName( "txtDistrictId" ) as eui.Label;

      } // end constructor

      public SetDistrictId( nId:number ):void
      {
        this.m_nId = nId;
        this.m_txtDistrictId.text = nId + "区";
      }

      public GetDistrictId():number
      {
        return this.m_nId;
      }


} // end class