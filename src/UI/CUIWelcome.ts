class CUIWelcome extends egret.DisplayObjectContainer {

     protected m_uiContainer:eui.Component = new eui.Component();

      protected m_timerMainLoop1:egret.Timer = new egret.Timer( 2000, 1 );
        protected m_timerMainLoop2:egret.Timer = new egret.Timer( 60, 0 );

      public constructor() {
        super();

        this.m_uiContainer.skinName = "resource/assets/MyExml/UIWelcome.exml";
        this.addChild( this.m_uiContainer );

        var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "img0" ) as eui.Image;
        imgTemp.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(255 ,110 ,180 )];

        this.m_timerMainLoop1 .addEventListener(egret.TimerEvent.TIMER, this.Status1, this);
        this.m_timerMainLoop1.start();

        this.m_timerMainLoop2 .addEventListener(egret.TimerEvent.TIMER, this.Status2, this);
   } // constructor

   public Status1():void
   {
       
       this.m_timerMainLoop2.start();
  Main.s_CurTown.visible = true;
            Main.m_CloudManager.visible = true;
   }
 
   public Status2():void
   {
     
       this.alpha -= 0.06;
       if ( this.alpha <= 0 )
       {
           this.m_timerMainLoop2.stop();
           CUIManager.SetUiVisible( Global.eUiId.welcome, false );

         
       }
   }


} // end class