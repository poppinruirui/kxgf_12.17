class CUIMainTitle extends egret.DisplayObjectContainer {

     protected m_uiContainer:eui.Component = new eui.Component();

     protected m_txtDebugInfo:eui.Label;

     protected m_txtCityName:eui.Label;
     protected m_txtCoins:eui.Label;
     protected m_txtPopulation:eui.Label;
     protected m_txtCPS:eui.Label;
     protected m_txtDiamond:eui.Label;
     protected m_txtKeysBoost:eui.Label;

     protected m_pbCoinBg:CUICosmosProgressBar;
     protected m_pbDiamondBg:CUICosmosProgressBar;
     protected m_pbPopulationBg:CUICosmosProgressBar;
     protected m_pbCpsBg:CUICosmosProgressBar;

     protected m_imgBg1:eui.Image;
     protected m_imgBg2:eui.Image;
     protected m_imgBg3:eui.Image;
     protected m_imgBg4:eui.Image;

     protected m_btnNextCity:CCosmosImage;
     protected m_pbPopulation:CUICosmosProgressBar;
     
     protected m_btnTask:CCosmosImage;
     protected m_btnTaskExclamation:eui.Image;
     
     protected m_btnDistrict:CCosmosImage;

     protected m_btnCityHall_New:CUICosmosCircle;

     protected m_nRealtimeCoin:number = 0;
     protected m_nShowCoin:number = 0;

     protected c_step:number = 5;

     protected m_imgCoinIcon:eui.Image = null;

      protected m_containerFlyingCoins:eui.Component = new eui.Component();


     public constructor() {
        super();

        this.m_uiContainer.skinName = "resource/assets/MyExml/test_ui.exml";
        this.addChild( this.m_uiContainer );

         this.m_uiContainer.addChild( this.m_containerFlyingCoins );



        this.m_txtDebugInfo = this.m_uiContainer.getChildByName( "txtDebugInfo" ) as eui.Label;

        this.m_txtCityName = this.m_uiContainer.getChildByName( "labelCityName" ) as eui.Label;
        this.m_txtCityName.text = "浮云小镇";

        var imgBtnPopulationAndNextCity:eui.Image = this.m_uiContainer.getChildByName( "btnPopulationAndNextCity" ) as eui.Image; 
       imgBtnPopulationAndNextCity.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_NextCity, this);
         
      

         this.m_imgCoinIcon = this.m_uiContainer.getChildByName( "imgCoinIcon" ) as eui.Image; 
/*
        this.m_imgBg1 = this.m_uiContainer.getChildByName( "imgBg1" ) as eui.Image;
        this.m_imgBg1.filters = [CColorFucker.GetColorMatrixFilterByRGBA( 1,1,1,1 )];
        
        this.m_imgBg2 = this.m_uiContainer.getChildByName( "imgBg2" ) as eui.Image;
        this.m_imgBg2.filters = [CColorFucker.GetColorMatrixFilterByRGBA( 0.8313,0.8313,0.8313,1 )];
      
        this.m_imgBg3 = this.m_uiContainer.getChildByName( "imgBg3" ) as eui.Image;
        this.m_imgBg3.filters = [CColorFucker.GetColorMatrixFilterByRGBA( 1,1,1,1 )];
        
        this.m_imgBg4 = this.m_uiContainer.getChildByName( "imgBg4" ) as eui.Image;
        this.m_imgBg4.filters = [CColorFucker.GetColorMatrixFilterByRGBA( 0.8313,0.8313,0.8313,1 )];
*/

var imgTemp:eui.Image = null;

/*
        var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "imgNextCity" ) as eui.Image;
        this.m_btnNextCity = new CCosmosImage();
        this.m_uiContainer.addChild( this.m_btnNextCity );
        this.m_btnNextCity.SetExml("resource/assets/MyExml/CCosmosButton8.exml");
        this.m_btnNextCity.x = imgTemp.x;
        this.m_btnNextCity.y = imgTemp.y;
        this.m_btnNextCity.width = imgTemp.width;
        this.m_btnNextCity.height = imgTemp.height;
        this.m_btnNextCity.scaleX = imgTemp.scaleX;  
        this.m_btnNextCity.scaleY = imgTemp.scaleY;   
        this.m_btnNextCity.SetImageTexture( 0, RES.getRes( "(12)_png" ) );
        this.m_uiContainer.removeChild( imgTemp );
           this.m_btnNextCity.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_NextCity, this);
*/

/*
        imgTemp = this.m_uiContainer.getChildByName( "imgPopulationPercent" ) as eui.Image;
        this.m_pbPopulation = new CUICosmosProgressBar();
        this.m_uiContainer.addChild( this.m_pbPopulation );
        this.m_pbPopulation .SetParams( imgTemp.width, imgTemp.height, 2, 0x00EE00, 0x000000, 0x006400 );
        this.m_pbPopulation.x = imgTemp.x;
        this.m_pbPopulation.y = imgTemp.y;
        this.m_uiContainer.removeChild( imgTemp );
     this.m_pbPopulation.SetPercent( 0 );
  */
   //
     

/*
        imgTemp = this.m_uiContainer.getChildByName( "imgTask" ) as eui.Image;
        this.m_btnTask = new CCosmosImage();
        this.m_btnTask.SetExml("resource/assets/MyExml/CTaskButton.exml");
        this.m_uiContainer.addChild( this.m_btnTask );
        this.m_btnTask.x = imgTemp.x;
        this.m_btnTask.y = imgTemp.y;
        this.m_btnTask.scaleX = imgTemp.scaleX;
        this.m_btnTask.scaleY = imgTemp.scaleY;
        this.m_uiContainer.removeChild( imgTemp );
        */
/*
        imgTemp = this.m_uiContainer.getChildByName( "btnDistrict" ) as eui.Image;
        this.m_btnDistrict = new CCosmosImage();
        this.m_btnDistrict.SetExml("resource/assets/MyExml/CDistrictBtn.exml");
        this.m_uiContainer.addChild( this.m_btnDistrict );
        this.m_btnDistrict.x = imgTemp.x;
        this.m_btnDistrict.y = imgTemp.y;
        this.m_btnDistrict.scaleX = imgTemp.scaleX;
        this.m_btnDistrict.scaleY = imgTemp.scaleY;
        this.m_uiContainer.removeChild( imgTemp );
         this.m_btnDistrict.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_District, this);
*/

       // this.m_btnTaskExclamation = this.m_uiContainer.getChildByName( "imgExclamation" ) as eui.Image;
        //this.m_uiContainer.addChild( this.m_btnTaskExclamation );
/*
        imgTemp = this.m_uiContainer.getChildByName( "pbCoins" ) as eui.Image;
        this.m_pbCoinBg = new CUICosmosProgressBar();
        this.m_uiContainer.addChild( this.m_pbCoinBg );
        this.m_pbCoinBg.x = imgTemp.x;
        this.m_pbCoinBg.y = imgTemp.y;
        this.m_pbCoinBg.SetParams( imgTemp.width, imgTemp.height, 2, 0xFFFFFF, 0x000000, 0xFFFFFF ) ;
        this.m_uiContainer.removeChild( imgTemp );

                imgTemp = this.m_uiContainer.getChildByName( "pbDiamond" ) as eui.Image;
        this.m_pbDiamondBg = new CUICosmosProgressBar();
        this.m_uiContainer.addChild( this.m_pbDiamondBg );
        this.m_pbDiamondBg.x = imgTemp.x;
        this.m_pbDiamondBg.y = imgTemp.y;
        this.m_pbDiamondBg.SetParams( imgTemp.width, imgTemp.height, 2, 0xFFFFFF, 0x000000, 0xFFFFFF ) ;
        this.m_uiContainer.removeChild( imgTemp );

                imgTemp = this.m_uiContainer.getChildByName( "pbPopulation" ) as eui.Image;
        this.m_pbPopulationBg = new CUICosmosProgressBar();
        this.m_uiContainer.addChild( this.m_pbPopulationBg );
        this.m_pbPopulationBg.x = imgTemp.x;
        this.m_pbPopulationBg.y = imgTemp.y;
        this.m_pbPopulationBg.SetParams( imgTemp.width, imgTemp.height, 2, 0xFFFFFF, 0x000000, 0xFFFFFF ) ;
        this.m_uiContainer.removeChild( imgTemp );
*/

/*
             imgTemp = this.m_uiContainer.getChildByName( "pbCpsBg" ) as eui.Image;
        this.m_pbCpsBg = new CUICosmosProgressBar();
        this.m_uiContainer.addChild( this.m_pbCpsBg );
        this.m_pbCpsBg.x = imgTemp.x;
        this.m_pbCpsBg.y = imgTemp.y;
        this.m_pbCpsBg.SetParams( imgTemp.width, imgTemp.height, 0, 0x000000, 0x000000, 0xFFFFFF, 0.3 ) ;
        this.m_uiContainer.removeChild( imgTemp );
*/




           this.m_txtPopulation = this.m_uiContainer.getChildByName( "labelPopulation" ) as eui.Label;
          this.m_uiContainer.addChild( this.m_txtPopulation );

        this.m_txtCPS = this.m_uiContainer.getChildByName( "labelCPS" ) as eui.Label;
        this.m_uiContainer.addChild( this.m_txtCPS );

        this.m_txtDiamond = this.m_uiContainer.getChildByName( "labelDiamond" ) as eui.Label;
        this.m_uiContainer.addChild( this.m_txtDiamond );

        this.m_txtCoins = this.m_uiContainer.getChildByName( "labelTotoalcoin" ) as eui.Label;
        this.m_uiContainer.addChild( this.m_txtCoins );

        imgTemp = this.m_uiContainer.getChildByName( "imgDiamond" ) as eui.Image;
        this.m_uiContainer.addChild(imgTemp);

        imgTemp = this.m_uiContainer.getChildByName( "imgCoinIcon" ) as eui.Image;
        this.m_uiContainer.addChild(imgTemp);

        imgTemp = this.m_uiContainer.getChildByName( "imgPopulation" ) as eui.Image;
        this.m_uiContainer.addChild(imgTemp);

         this.m_uiContainer.addChild( this.m_txtCityName );


         this.m_txtKeysBoost = this.m_uiContainer.getChildByName( "txtKeysBoost" ) as eui.Label;
         this.m_uiContainer.addChild( this.m_txtKeysBoost );

         var labelPerSec = this.m_uiContainer.getChildByName( "labelPerSec" ) as eui.Label;
         this.m_uiContainer.addChild( labelPerSec );
         
/*       
        this.m_btnNextCity = new CUICosmosCircle();
        this.m_uiContainer.addChild(this.m_btnNextCity);
        this.m_btnNextCity.x = 60;
        this.m_btnNextCity.y = 200;
        this.m_btnNextCity.SetParams( 45, 1, 0x363636, 0xFFFFFF, 38, 0, 0xFFFFFF, 0x00F5FF );
*/
       
        /*
        this.m_btnNextCity_New = new CUICosmosProgressBar();
        this.m_uiContainer.addChild(this.m_btnNextCity_New);
        this.m_btnNextCity_New.x = imgTemp.x;
        this.m_btnNextCity_New.y = imgTemp.y;
        this.m_btnNextCity_New.SetParams( 200, 30, 2, 0xFFFFFF, 0x000000, 0x00FF00 );
        this.m_btnNextCity_New.SetPercent( 0.5 );
        */


        /*
        this.m_btnCityHall_New = new CUICosmosCircle();
        this.m_uiContainer.addChild(this.m_btnCityHall_New);
        this.m_btnCityHall_New.x = imgTemp.x;
        this.m_btnCityHall_New.y = imgTemp.y;
        this.m_btnCityHall_New.SetParams(   60, 2, 0x000000, 0xFF0000, 30, 6, 0x00FF00, 0x0000FF );
        */

        // end poppin test
/*
        this.m_btnNextCity.SetExml( "resource/assets/MyExml/CCosmosButton7.exml" );
        this.m_btnNextCity.x = imgTemp.x;
        this.m_btnNextCity.y = imgTemp.y;
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnNextCity.SetImageColor_New( 1, 30, 144, 255 );
        this.m_btnNextCity.CreateProgressBar( 2, 2, [30, 144, 255], [105, 105, 105] );
        this.m_btnNextCity.SetImageColor_New( 3, 211, 211, 211 );
        
         this.m_btnNextCity.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_NextCity, this);
*/

        this.CalculateFlyingCoinParams();

     } // end contructor

     public SetPopulation( nPopulation:number ):void
     {
        this.m_txtPopulation.text = nPopulation.toString();

        var config:CCofigCityLevel = CConfigManager.GetCityLevelConfig( Main.s_CurTown.GetLevel() );
        var fPercent:number = nPopulation / config.nPopulationToLevelUp;

        
      // this.m_pbPopulation.SetPercent(fPercent);
     }

     public SetCoins( nCoins:number, bDirect:boolean = false ):void
     {
        this.m_nRealtimeCoin = nCoins;
        if ( bDirect )
        {
            this.m_nShowCoin = nCoins;
        }
     }

     public  UpdateShowCoin():void
     {
         if ( Main.s_CurTown == null )
         {
             return;
         }

        var fShowCoinChangeAmount:number = Main.s_CurTown.GetShowCoinChangeSpeed();
        fShowCoinChangeAmount *= CDef.s_fFixedDeltaTime;
       


         if ( this.m_nShowCoin < this.m_nRealtimeCoin )
         {
            this.m_nShowCoin += fShowCoinChangeAmount;
            if ( this.m_nShowCoin > this.m_nRealtimeCoin  )
            {
                this.m_nShowCoin = this.m_nRealtimeCoin 
            }
         }
         else if ( this.m_nShowCoin > this.m_nRealtimeCoin )
         {

            this.m_nShowCoin -= fShowCoinChangeAmount;
             if ( this.m_nShowCoin < this.m_nRealtimeCoin  )
            {
                this.m_nShowCoin = this.m_nRealtimeCoin 
            }
         }

         this.m_txtCoins.text = (Math.floor( this.m_nShowCoin ) ).toString();

     }

     public SetCPS( nCPS:number ):void
     {
         this.m_txtCPS.text = nCPS.toString();
     }

     protected m_fShowCoinTimeElapse = 0;
     public MainLoop():void
     {
         this.UpdateShowCoin();

         this.FlyingCoinLoop();
     }

     public SetDiamond( nDiamond:number ):void
     {
         this.m_txtDiamond.text = nDiamond.toString();
     }

     public SetCityName( szCityName:string ):void
     {
         this.m_txtCityName.text = szCityName;
     }

     public SetCityId( nTownId:number ):void
     {
        // this.m_btnNextCity.SetImageTexture(1, CResourceManager.GetNumberTexture(nTownId) );
     }

     protected onButtonClick_NextCity( evt:egret.TouchEvent ):void
     {
         CUIManager.SetUiVisible( Global.eUiId.to_next_city, true );
         CUIManager.s_uiToNextCityPanel.UpdateInfo();
     }

     public ShowDebugInfo( szInfo:string ):void
     {
         this.m_txtDebugInfo.text = szInfo;
     }

     public onButtonClick_District( evt:egret.TouchEvent ):void
     {
             CUIManager.SetUiVisible( Global.eUiId.district, true );

             switch( Main.s_CurTown.GetId() )
             {
                 case 5:
                 {
                    CUIManager.s_uiDistrict.UpdateDistrictInfo( 2, 1 );
                 }
                 break;

                 case 6:
                 {
                    CUIManager.s_uiDistrict.UpdateDistrictInfo( 4, 1 );
                 }
                 break;

                 case 7:
                 {
                    CUIManager.s_uiDistrict.UpdateDistrictInfo( 6, 1 );
                 }
                break;
                 case 8:
                 {
                    CUIManager.s_uiDistrict.UpdateDistrictInfo( 8, 1 );
                 }
                 break;
             } // end switch
            
     }

     protected m_fStartPosX:number = 150;
     protected m_fStartPosY:number = 1193;

     protected m_fMiddle_Left_PosX:number = 68;
     protected m_fMiddle_Left_PosY:number = 944;

     protected m_fMiddle_Right_PosX:number = 260    ;
     protected m_fMiddle_Right_PosY:number = 1020;

     protected m_fEnd_PosX:number = 212;
     protected m_fEnd_PosY:number = 54;

   

     protected m_fSession0Time:number = 0.5;
     protected m_fSession1Time:number = 0.5;

   

       protected CalculateFlyingCoinParams():void
       {
          for ( var i:number = 0; i < 20; i++ )
         {
            var coin:UIFlyingCoin = new UIFlyingCoin();
            this.m_containerFlyingCoins.addChild( coin );

            var k:number = Math.random();
            var fMiddlePosX:number = this.m_fMiddle_Left_PosX * k + this.m_fMiddle_Right_PosX * ( 1 - k );
            var fMiddlePosY:number = this.m_fMiddle_Left_PosY * k + this.m_fMiddle_Right_PosY * ( 1 - k );
            coin.SetMiddlePos( fMiddlePosX,  fMiddlePosY);
            coin.SetEndPos( this.m_fEnd_PosX, this.m_fEnd_PosY );
        
            // session 0
            var sX:number = fMiddlePosX - this.m_fStartPosX;
            var sY:number = fMiddlePosY - this.m_fStartPosY;
            var t:number = this.m_fSession0Time;
            t = t * k + (2 * t ) * ( 1 - k );
            var aX:number = 2 * sX / (t * t);
            var aY:number = 2 * sY / (t * t);
            coin.AddSessionParams( aX, aY, this.m_fStartPosX, this.m_fStartPosY, fMiddlePosX, fMiddlePosY );

            // session 1
            var sX:number = this.m_fEnd_PosX - fMiddlePosX;
            var sY:number = this.m_fEnd_PosY - fMiddlePosY;
            var t:number = this.m_fSession0Time;
            t = t * k + (2 * t ) * ( 1 - k );
            var aX:number = 2 * sX / (t * t);
            var aY:number = 2 * sY / (t * t);
            coin.AddSessionParams( aX, aY, fMiddlePosX, fMiddlePosY ,this.m_fEnd_PosX, this.m_fEnd_PosY  );

            coin.visible = false;
         } // end i
       }


       protected FlyingCoinLoop():void
       {
           for ( var i:number = this.m_containerFlyingCoins.numChildren - 1; i >= 0; i-- )
           {
               var coin:UIFlyingCoin = this.m_containerFlyingCoins.getChildAt(i) as UIFlyingCoin;
               coin.Moving();
           } // end for
       }

       public BeginCoinsFly():void
       {
           return;

            for ( var i:number = this.m_containerFlyingCoins.numChildren - 1; i >= 0; i-- )
           {
               var coin:UIFlyingCoin = this.m_containerFlyingCoins.getChildAt(i) as UIFlyingCoin;
               coin.BeginFlying();
           } // end for


          var music_url:string = CResourceManager.url_root + "Audio/bank.mp3";
                  RES.getResByUrl( music_url ,this.onLoadAudioComplete,this,RES.ResourceItem.TYPE_SOUND); 
       }


 protected m_Sound:egret.Sound;
      protected   onLoadAudioComplete(event:any):void {
        
          this.m_Sound = <egret.Sound>event;
        this.m_Sound .play( 0, 1 );
   

         

      }

} // end class