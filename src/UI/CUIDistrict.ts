class CUIDistrict extends egret.DisplayObjectContainer {

     protected m_uiContainer:eui.Component = new eui.Component();
     protected m_btnClose:CCosmosImage;

     protected m_nDistrictNum:number = 0;

     protected m_containerSketch:eui.Component = new eui.Component();

     public constructor() {
        super();

        this.m_uiContainer.skinName = "resource/assets/MyExml/Distrixt.exml";
        this.addChild( this.m_uiContainer );

        this.addChild( this.m_containerSketch );

        var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "imgBg" ) as eui.Image;
        imgTemp.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255( 181, 181, 181 )];

          var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "btnClose" ) as eui.Image;
        this.m_btnClose = new CCosmosImage();
        this.m_btnClose.SetExml( "resource/assets/MyExml/CCosmosButton1.exml" );
        this.m_uiContainer.addChild( this.m_btnClose );
        this.m_btnClose.x = imgTemp.x;
        this.m_btnClose.y = imgTemp.y;
        this.m_uiContainer.removeChild( imgTemp );
        this.m_btnClose.UseColorSolution(0);
        this.m_btnClose.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Close, this);

      



     } // end constructor


     private onButtonClick_Close( evt:egret.TouchEvent ):void
    {
          CUIManager.SetUiVisible( Global.eUiId.district, false );
    }

     public UpdateDistrictInfo( nDistrictNum:number, nCurDistrictId:number ):void
     {
         var bReGenerate:boolean = false;
         if ( this.m_nDistrictNum != nDistrictNum )
         {
             bReGenerate = true;
          
         }
         this.m_nDistrictNum = nDistrictNum;

         if (bReGenerate)
         {
                this.ReGenerateDistrict();
         }
     }

     public ReGenerateDistrict():void
     {
         this.Clear();
         var nCenterPosX:number = 250;
         var nCenterPosY:number = 300;
        
         for ( var i:number = 1; i <= this.m_nDistrictNum; i++ )
         {

             var sketch:CUIDistrictSketch = CResourceManager.NewDistrictSketch();
              sketch.SetDistrictId( i  );
             this.m_containerSketch.addChild( sketch );
             
             // 添加事件之前一定要检查该物件上是不是已经有该事件了。因为整套体系中各种物件都是复用的。
             if ( !sketch.hasEventListener(egret.TouchEvent.TOUCH_TAP,) )
             {
                sketch.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onClickSketch, this);
             }

             switch( i )
             {  
                 case 1:
                 {
                   
                     sketch.x = 250;
                     sketch.y = 300;
                 }
                 break;
                 case 2:
                 {
                   
                     sketch.x = nCenterPosX;
                     sketch.y = nCenterPosY - CDef.s_nTileHeight * 2;
                 }
                 break;
                 case 3:
                 {
                   
                     sketch.x = nCenterPosX + CDef.s_nTileWidth ;
                     sketch.y = nCenterPosY - CDef.s_nTileHeight;
                 }
                 break;
                 case 4:
                 {
                   
                     sketch.x = nCenterPosX + CDef.s_nTileWidth;
                     sketch.y = nCenterPosY;
                 }
                 break; 
                 case 5:
                 {
                   
                     sketch.x = nCenterPosX + CDef.s_nTileWidth;
                     sketch.y = nCenterPosY + CDef.s_nTileHeight ;
                 }
                 break;
                 case 6:
                 {
                   
                     sketch.x = nCenterPosX;
                     sketch.y = nCenterPosY + CDef.s_nTileHeight * 2;
                 }
                 break;
                 case 7:
                 {
                   
                     sketch.x = nCenterPosX - CDef.s_nTileWidth ;
                     sketch.y = nCenterPosY + CDef.s_nTileHeight ;
                 }
                 break;
                 case 8:
                 {
                   
                     sketch.x = nCenterPosX - CDef.s_nTileWidth ;
                     sketch.y = nCenterPosY ;
                 }
                 break;
                 case 9:
                 {
                   
                     sketch.x = nCenterPosX - CDef.s_nTileWidth * 0.5;
                     sketch.y = nCenterPosY - CDef.s_nTileHeight * 0.5;
                 }
                 break;


             } // end switch
         } // end for
    
     }

     public Clear():void
     {
         for ( var i:number = this.m_containerSketch.numChildren - 1; i >= 0; i-- )
         {
             var sketch:CUIDistrictSketch = this.m_containerSketch.getChildAt(i) as CUIDistrictSketch;
             CResourceManager.DeleteDistrictSketch( sketch );
         }

         
     }

     protected onClickSketch( evt:egret.TouchEvent ):void
     {
         var sketch:CUIDistrictSketch = evt.currentTarget as CUIDistrictSketch;
         Main.s_CurTown.ChangeDistrict( sketch.GetDistrictId() );
     }

} // end class