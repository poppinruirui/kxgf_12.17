/*
显示类配置在这个文件里面

逻辑类数值在CConfigmanager里面
*/
module Global{

   

    export enum eOperate{
        toggle_grid = 0 ,// 显示/隐藏网格
   
        edit_obj,    // 编辑物件
        del_obj,     // 删除物件

        add_diamond, // 增加钻石
        add_coin, // 增加金币

        add_cityhall,
        add_garage,
        add_bank,
        add_windturbine,
        add_airport,
        add_harbour,

        // (老的添加马路流程废弃)
        add_road_zuoshangyouxia,     // 添加马路-左上右下型
        add_road_youshangzuoxia,     // 添加马路-右上左下型
        add_road_corner_shang,       // 添加马路转角-上
        add_road_corner_xia,       // 添加马路转角-下
        add_road_corner_zuo,       // 添加马路转角-左
        add_road_corner_you,       // 添加马路转角-右
        
        // 新的添加马路流程
        add_road,             // 添加马路
        add_test_car,         // 添加测试车辆
        toggle_show_road,         // 切换：是否显示马路（有些马路是在地块上面，只执行马路功能，不显示马路图片）
        toggle_tile_touch_enabled,      // 切换：是否点选地块
        
        add_road_t_zuoxia,         // （废弃）

        add_tile_2x2,              // 添加2x2型宗地
        add_tile_4x4,              // 添加4x4型宗地
           
        add_dibiao,
        del_dibiao,

        add_tree, // 摆放树

        add_arrow_zuoshang,
        add_arrow_zuoxia,
        add_arrow_youshang,
        add_arrow_youxia,


        do_nothing,
        num,
    }

    // 尺寸规格
     export enum eObjSize{
       size_1x1,
       size_2x2,
       size_4x4,
    }

     // 地块状态
     export enum eBuildingLandStatus{
       empty,          // 空地
       contructing,    // 建设中
       building_exist, // 上有建筑 
    }


   // 物件的功能分类
     export enum eObjFunc{
         none,   
       road,     // 道路
       building, // 建筑物、建筑用宗地
       car,      // 汽车
       airplane, // 飞机
       ship,     // 轮船
       tile,     // 地块
    }

    // 特殊职能建筑，由系统生成，玩家不能去建设或修改
     export enum eBuildinSpeical{

       none,            // 
       
       // ----- 以下为特殊职能建筑
       cityhall,          // 市政厅
       bank,            // 银行
       garage,          // 车库
       airport,         // 机场

       
       num,
    }

    // 汽车的运行方向
     export enum eAutomobileDir{
         zuoshang, // 左上
         youshang, // 右上
         zuoxia,   // 左下 
         youxia,   // 右下
     }

     // 汽车转弯的方向
     export enum eNeighbourDir
     {
         qian, // 上
         hou,   // 下
         zuo,   // 左
         you,   // 右
     }

     // 汽车的运行状态
     export enum eAutomobileStatus{
         none,        // 没有行走
         normal,      // 常规行走
         u_turn,      // 掉头中
         turn_left,   // 左转中
         turn_right,  // 右转中
     }

       export enum eDoubleRectBgType{
         up_down,      // 上下结构
         in_out,      // 里外结构
    
     }

    // 各功能界面的ID号
    export enum eUiId{
        garage,
        main_title,
        developer_editor,
        bottom_buttons,
        buy_lot_panel,
        wind_turbine, // 风车
        lot_upgrade, // 地块升级界面
        revert_lot_confirm,  // 推平地块确认jiemian
        boost_panel, // “加速”面板
        city_hall,   // “市政厅”界面
        bank,       // 银行
        bank_back,  // 返回游戏时银行收钱界面
        special_panel, // “特殊建筑”面板
        to_next_city, // “升级到下一级城市”面板
        welcome, // 欢迎界面
        district, // 分区界面
     }

     // 空各自种类
      export enum eEmptyGridType{
          inside_city, // 城市内部
          city_edge,   // 城市边缘
          outside_city,// 城市外面
      }

      // 地块的属性
         export enum eLotPsroperty{
          residential = 0, //  住宅
          business,   // 商业
          service,    // 服务

          length,
      }

       // 车商城里面条目的状态
         export enum eGarageItemStatus{
          locked_and_can_unlock,         // 尚未解锁,
          unlocked,         // 已解锁,  
          can_not_unlock, // 不可解锁   
      }

        // 币的种类
         export enum eMoneySubType{
          small_coin,         // 小金币
          big_coin,         // 大金币 
          small_diamond, // 小钻石
           big_diamond, // 大钻石
      }


        // 进度条的种类
         export enum eProgressBarType{

          none, // 无

          constructing,         // 建造
          upgrading,         // 升级
         
      }

       // 跳字的种类
         export enum eTiaoZiType{

         
          money,         // 金钱
          level,         // 等级
         
      }

        export enum eGridCornerPosType{
            top,
            bottom,
            left, 
            right,
            one_fourth_0,
            one_fourth_1,
            one_fourth_2,
            one_fourth_3,
            one_fourth_4,
            one_fourth_5,
            one_fourth_6,
            one_fourth_7,

        }

        export enum eValueShowStyle
        {
            directly,   // 直接原封不动显示
            percent,    // 以百分比显示
            per_second, // 以“数值/秒”的格式显示
        }

        export enum eCityHallPageType
        {
            city,
            game,
        }

        export enum eMoneyType
        {
            coin,
            diamond,
        }

} // end Global

class CDef
{
    public static EDITOR_MODE:boolean = false;    

    public static s_nConstructionProgressBarPosOffsetX:number = 120;
    public static s_nConstructionProgressBarPosOffsetY:number = 450;

    // 最大规格的格子尺寸
    public static s_nTileWidth:number  = 82;//316;
    public static s_nTileHeight:number = 47; // 181;
    public static s_nGridNumX:number = 50;
    public static s_nGridNumY:number = 50;
    
    public static s_nMaxBuildingLevel = 24;

    public static s_nSaveTypeStartIndex:number = Global.eOperate.add_road_zuoshangyouxia;
    public static s_nSaveTypeEndIndex:number = Global.eOperate.num - 1;

    public static s_fAutomobileMoveSpeed:number =2;
    public static s_fAutomobileMoveDirX:number = 0;
    public static s_fAutomobileMoveDirY:number = 0;

    public static s_fFixedDeltaTime:number = 0.04;

    public static s_fTiaoZiTotalTime:number = 0.5;
    public static s_fTiaoZiTotalDistance:number = 10;

    public static s_fCraneFrameInterval:number = 0.5; //  “塔吊”帧动画的时间间隔


    public static s_fBuildingLandLoopInterval = 0.5; //

    public static s_fCarReInsertSortInterval = 0.3;

    //// car garage
    public static s_fGarageItemWitdh:number = 550;
    public static s_fGarageItemHeight:number = 300;

    //// end car garage


    //// JinBI
    public static s_fJinBiAniInterval:number = 0.3;
    public static s_fJinBiAniFrameNum:number = 4;
    //// end JinBI

    public static RES_1x1:string = "1X1_png";

    public static COIN_UPDATE_STEP:number = 15;

    public static s_nDoubleTimes:number = 600;
    public static s_nFastForward_10:number = 10;
    public static s_nFastForward_60:number = 60;

    public static JINBI_RES:string = "jinbi_0_png";
    public static ZUANSHI_RES:string = "zuanshi_png";

    public static GetAnchorOffsetY( obj_size:Global.eObjSize ):number
    {
        switch( obj_size )
        {
            case Global.eObjSize.size_1x1:
            {
                return 11;
            }
            break;
            case Global.eObjSize.size_2x2:
            {
                return 175;
            }
            break;
            case Global.eObjSize.size_4x4:
            {
                return 175;
            }
            break;
        } // end switch

        return 0;
    }

    public static GetAnchorOffsetY_Middle( obj_size:Global.eObjSize ):number
    {
        switch( obj_size )
        {
            case Global.eObjSize.size_1x1:
            {
                return 22;
            }
            break;
            case Global.eObjSize.size_2x2:
            {
                return 90;
            }
            break;
            case Global.eObjSize.size_4x4:
            {
                return 180;
            }
            break;
        } // end switch

        return 0;
    }

    public static GetBuildingOffsetY( obj_size:Global.eObjSize ):number
    {
         switch( obj_size )
        {
            case Global.eObjSize.size_2x2:
            {
                return 180;
            }
            break;
            case Global.eObjSize.size_4x4:
            {
                return 185;
            }
            break;
        }
            return 0;
    }

 public static GetBuildingOffsetY_Middle( obj_size:Global.eObjSize ):number
    {
         switch( obj_size )
        {
            case Global.eObjSize.size_2x2:
            {
                return 65;
            }
            break;
            case Global.eObjSize.size_4x4:
            {
                return 160;
            }
            break;
        }
            return 0;
    }


} // end CDef