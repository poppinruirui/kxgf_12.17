class CBankData extends egret.DisplayObjectContainer {

    public nCurLevel:number = 1;
    public nCurAvailableTime:number = 0;
    public nCurProfitRate:number = 0;

    public GetCurLevel():number
    {
        return this.nCurLevel;
    }

    public SetCurLevel( nLevel:number ):void
    {
        this.nCurLevel = nLevel;

        // poppin to do 还有CityHall_Game中的加成，稍后做
        this.nCurAvailableTime = CConfigManager.GetBankTimeChangeAmount() * this.nCurLevel;
    }

} // end class