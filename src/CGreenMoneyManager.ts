class CGreenMoneymanager extends egret.DisplayObjectContainer {

     static vecTempPos:CVector = new CVector();
     static vecTempPos1:CVector = new CVector();

     public static s_lstContainerFlyingGreen:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();

     public static s_nPigGreenNum:number = 0;

     public constructor() {
        super();



     }

     public static SetPigGreenNum( num:number ):void
     {
         CGreenMoneymanager.s_nPigGreenNum = num;

         CUIManager.s_uiCPosButton.SetPigGreenNum( num );
     }

     public static GetPigGreenNum( ):number
     {
         return CGreenMoneymanager.s_nPigGreenNum;
     }

     public static FixedUpdate():void
     {
        CGreenMoneymanager.FlyLoop();
     }

     protected static FlyLoop():void
     {
         for ( var i:number = CGreenMoneymanager.s_lstContainerFlyingGreen.numChildren - 1; i >= 0; i-- )
         {
             var green:CObj = CGreenMoneymanager.s_lstContainerFlyingGreen.getChildAt(i) as CObj;
             if ( green.MoveToDest() )
             {
                  CResourceManager.DeleteFlyJinBi( green );

                   var effect:CFrameAni = CFrameAniManager.NewEffect();
                    effect.SetParams( "1a_", 12, 0, false );
                    effect.Play();
                    effect.x = green.GetDestPos().x;
                    effect.y = green.GetDestPos().y;
                    effect.scaleX = 0.8;
                    effect.scaleY = 0.8;

                    var szType:string = green.GetParamsByKey( "MoveType" );
                    if ( szType == "FlyToMainTitle"  )
                    {

                    }
                    else if ( szType == "FlyToPig"  )
                    {
                          CGreenMoneymanager.SetPigGreenNum( CGreenMoneymanager.GetPigGreenNum() + 1 );     
                    }
             }
         }
     }

     public static GenerateOneGreenMoneyToFly( nType:number, nCurPosX:number, nCurPosY:number ):void
     {
         CGreenMoneymanager.vecTempPos1.x = nCurPosX;
         CGreenMoneymanager.vecTempPos1.y = nCurPosY;

         var green:CObj = CResourceManager.NewFlyJinBi();
         green.SetTexture( RES.getRes( "lvpiao_png" ) );

        if ( nType == 0 ) // fly to GreenMoneyFlyToMainTitle
        {
            CGreenMoneymanager.vecTempPos.x = 20;
            CGreenMoneymanager.vecTempPos.y = 20;
            green.SetMoveParams( CGreenMoneymanager.vecTempPos, CGreenMoneymanager.vecTempPos1, 0.5 );
            green.SetParams( "MoveType", "FlyToMainTitle" );
        }
        else if ( nType == 1 ) // fly to pig
        {
            CGreenMoneymanager.vecTempPos.x = CUIManager.s_uiCPosButton.GetPigButton().x + 49;
            CGreenMoneymanager.vecTempPos.y = CUIManager.s_uiCPosButton.GetPigButton().y + 49;
            green.SetMoveParams( CGreenMoneymanager.vecTempPos, CGreenMoneymanager.vecTempPos1,  0.3  );
            green.SetParams( "MoveType", "FlyToPig" );
        }

        CGreenMoneymanager.s_lstContainerFlyingGreen.addChild( green );
        green.scaleX = 0.8;
        green.scaleY = 0.8;
        green.anchorOffsetX = green.width * 0.5;
        green.anchorOffsetY = green.height * 0.5;
        green.x = nCurPosX;
        green.y = nCurPosY;
         
     }


} // end class