class CGrid extends egret.DisplayObjectContainer {

    private m_txtId:egret.TextField = null;// = new egret.TextField();
    private m_bmpEmpty:egret.Bitmap = null;// = new egret.Bitmap(); //  该网格没有物件时显示的代图

    private m_Graphics:egret.Shape = null;// = new egret.Shape();

    private m_BoundObj:CObj = null;

    protected m_eEmptyGridType:Global.eEmptyGridType = Global.eEmptyGridType.outside_city;

    protected m_dicCornerPosX:Object = new Object();
    protected m_dicCornerPosY:Object = new Object();

    protected m_nGuid:number = 0;

      public constructor() {
        super();

       
        if ( CDef.EDITOR_MODE )
        { 
        
   
          this.m_bmpEmpty = new egret.Bitmap(); 
          this.m_bmpEmpty.texture = RES.getRes("Tile_1x1_png");
          this.addChild(this.m_bmpEmpty);
          this.m_bmpEmpty.filters = [CColorFucker.GetColorMatrixFilterByRGBA( 0.663, 0.663, 0.663 )];
          //this.m_Graphics = new egret.Shape();
          //this.addChild( this.m_Graphics );

          this.m_txtId = new egret.TextField();
          this.addChild( this.m_txtId );
          this.m_txtId.text = "123";
          this.m_txtId.size  =20;
          this.m_txtId.x = this.width / 4;
          this.m_txtId.y = this.height / 4;
          this.m_txtId.textAlign = egret.HorizontalAlign.JUSTIFY;
          this.m_txtId.touchEnabled = true;
          this.m_txtId.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapMe, this );
        }

      }

      protected onTapMe( evt:egret.TouchEvent ):void
      {
        console.log( "tap grid:" + this.GetIdByString() );
        Main.s_CurTown.Processtap( this, CTown.s_PosXInTown, CTown.s_PosYInTown ); 

      }

      //  绘制菱形（矢量图）
      public DrawRhombus( nWidth:number, nHeight:number, nHalfWidth:number, nHalfHeight:number ):void
      {
        if ( this.m_Graphics == null )
        {
          return;
        }

         this.m_Graphics.graphics.lineStyle(0.1, 0x000000 );
         this.m_Graphics.graphics.moveTo(0,nHalfHeight);
         this.m_Graphics.graphics.lineTo(nHalfWidth,nHeight);
          this.m_Graphics.graphics.lineTo(nWidth,nHalfHeight);
          this.m_Graphics.graphics.lineTo(nHalfWidth,0);
           this.m_Graphics.graphics.lineTo(0,nHalfHeight);
         this.m_Graphics.graphics.endFill();  
         
      }

      private m_szId:string = "";
     private m_dicId:Object = new Object();

      public SetId( nIndexX:number, nIndexY:number ):void
      {
        this.m_dicId["nIndexX"] = nIndexX;
         this.m_dicId["nIndexY"] = nIndexY;
      }

      public GetId(  ):Object
      {
          return this.m_dicId; 
      }

      public GetIdByString():string
      {
        return ( this.m_dicId["nIndexX"] + "_" + this.m_dicId["nIndexY"] );
      }


   

      public SetText( txt:string ):void
      {
        if (  this.m_txtId == null )
        {
          return;
        }

         this.m_txtId.text = txt;
      }

      public SetGridVisible( bVisible:boolean ):void
      {
        if ( this.m_Graphics )
        {
         this.m_Graphics.visible = bVisible;
        }
         this.m_txtId.visible = bVisible;
         this.m_bmpEmpty.alpha = 0.01;
      }


      public  ClearBoundObj():void
      {
        this.m_BoundObj = null;   
        return;

        if ( this.m_BoundObj == null || this.m_BoundObj == undefined )
        {
          return;
        }

       // console.log( "销毁当前已有物体：" + this.m_BoundObj.GetGuid() );
        var obj_to_delete:CObj = this.m_BoundObj;
        this.m_BoundObj.ClearBoundGrid(); // 互相解绑
    
       // Main.s_CurTown.DelObj( obj_to_delete );
       CResourceManager.DeleteObj( obj_to_delete );
       
        this.m_BoundObj = null;
      }

      protected m_bZhiNeng:boolean = false;
      public SetZhiNengJianZhu( bVal:boolean ):void
      {
        this.m_bZhiNeng = bVal;
      }

      public GetZhiNengJianZhu():boolean
      {
       return  this.m_bZhiNeng;
      }

      public SetBoundObj( obj:CObj ):void
      {


        this.m_BoundObj = obj;
/*
        if ( this.m_BoundObj == null )
        {
          this.m_bmpEmpty.alpha = 1;
        }
        else
        {
          this.m_bmpEmpty.alpha = 0.01;
        }
        */
      }

      public GetBoundObj( ):CObj
      {
        return this.m_BoundObj;
      }

 public SetPos( x:number, y:number ):void
    {
        this.x = x;
        this.y = y;

        var CornerPosX:number = 0;
        var CornerPosY:number = 0;  
        CornerPosX = x;
        CornerPosY = y + CDef.s_nTileHeight * 0.5;
        this.m_dicCornerPosX[Global.eGridCornerPosType.bottom] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.bottom] = CornerPosY;

        CornerPosX = x + CDef.s_nTileWidth * 0.5;
        CornerPosY = y ;
        this.m_dicCornerPosX[Global.eGridCornerPosType.right] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.right] = CornerPosY;
      
        CornerPosX = x;
        CornerPosY = y - CDef.s_nTileHeight * 0.5;
        this.m_dicCornerPosX[Global.eGridCornerPosType.top] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.top] = CornerPosY;

        CornerPosX = x - CDef.s_nTileWidth * 0.5;
        CornerPosY = y ;
        this.m_dicCornerPosX[Global.eGridCornerPosType.left] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.left] = CornerPosY;
        ///// ------------ 

        // poppin trick
        y += CDef.s_nTileHeight * 0.5;

        CornerPosX = x + CDef.s_nTileWidth * 0.125;
        CornerPosY = y - CDef.s_nTileHeight * 0.125;
        this.m_dicCornerPosX[Global.eGridCornerPosType.one_fourth_0] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.one_fourth_0] = CornerPosY;

        CornerPosX = x + CDef.s_nTileWidth * 0.375;
        CornerPosY = y - CDef.s_nTileHeight * 0.375;
        this.m_dicCornerPosX[Global.eGridCornerPosType.one_fourth_1] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.one_fourth_1] = CornerPosY;

        CornerPosX = x + CDef.s_nTileWidth * 0.375;
        CornerPosY = y - CDef.s_nTileHeight * 0.625;
        this.m_dicCornerPosX[Global.eGridCornerPosType.one_fourth_2] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.one_fourth_2] = CornerPosY;

        CornerPosX = x + CDef.s_nTileWidth * 0.125;
        CornerPosY = y - CDef.s_nTileHeight * 0.875;
        this.m_dicCornerPosX[Global.eGridCornerPosType.one_fourth_3] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.one_fourth_3] = CornerPosY;

        CornerPosX = x - CDef.s_nTileWidth * 0.125;
        CornerPosY = y - CDef.s_nTileHeight * 0.875;
        this.m_dicCornerPosX[Global.eGridCornerPosType.one_fourth_4] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.one_fourth_4] = CornerPosY;

        var
               CornerPosX = x - CDef.s_nTileWidth * 0.375;
        CornerPosY = y - CDef.s_nTileHeight * 0.625;
        this.m_dicCornerPosX[Global.eGridCornerPosType.one_fourth_5] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.one_fourth_5] = CornerPosY;

               CornerPosX = x - CDef.s_nTileWidth * 0.375;
        CornerPosY = y - CDef.s_nTileHeight * 0.375;
        this.m_dicCornerPosX[Global.eGridCornerPosType.one_fourth_6] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.one_fourth_6] = CornerPosY;

        CornerPosX = x - CDef.s_nTileWidth * 0.125;
        CornerPosY = y - CDef.s_nTileHeight * 0.125;
        this.m_dicCornerPosX[Global.eGridCornerPosType.one_fourth_7] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.one_fourth_7] = CornerPosY;

    }

    

    public GetCornerPosX( eCornerType:Global.eGridCornerPosType ):number
    {
      return this.m_dicCornerPosX[eCornerType];
    }

    public GetCornerPosY( eCornerType:Global.eGridCornerPosType ):number
    {
      return this.m_dicCornerPosY[eCornerType];
    }

    private m_objectPos:Object = new Object();
    public GetPos( ):Object
    {
        this.m_objectPos["x"] = this.x;
        this.m_objectPos["y"] = this.y;
        return this.m_objectPos;
    }

    public Processtap():void
    {

  
        switch( Main.s_CurTown.GetOperate() )
        {
          case Global.eOperate.add_windturbine:
          {
            Main.s_CurTown.AddWindTurbine( this );
          }
          break;

/*
          case Global.eOperate.set_inside_empty_grid:
          {
            this.SetEmptyGridType( Global.eEmptyGridType.inside_city );
          }
          break;

           case Global.eOperate.set_edge_empty_grid:
          {
            this.SetEmptyGridType( Global.eEmptyGridType.city_edge );
          }
          break;
          */

          case Global.eOperate.del_obj:
          {
             var obj:CObj = this.GetBoundObj();
             if ( obj != null  )
             {                 // 物件和格子互相绑定
                                         if ( this.GetZhiNengJianZhu() )
                                         {
                                                 console.log( "不得哦3333333" );
                                         }
                this.ClearBoundObj();
             }
          }
          break;

/*
          case Global.eOperate.edit_obj:
          {
            var obj:CObj = this.GetBoundObj();
            if ( obj != null  )
            {
                obj.ProcessEdit();
              
            }
          }
          break;
*/
/*
          case Global.eOperate.add_cityhall: // 放置“市政厅”
          case Global.eOperate.add_bank: // 放置“银行”
          case Global.eOperate.add_garage: // 放置“车站”
          {
            var obj:CObj = this.GetBoundObj();
            if ( obj != null  )
            {
              obj.ProcessAddSpecialBuilding(Main.s_CurTown.GetOperate());
            }
            else
            {
               Main.s_panelMsgBox.ShowMsg( "此处尚不是建筑用地" );
            }
          }
          break;
          */

        } // end switch

          var obj:CObj = this.GetBoundObj();
            if ( obj != null  )
            {
                obj.ProcessEdit();
              
            }



    }

    public IsTaken():boolean
    {
      return this.m_BoundObj != null;
    }

    public SetColor( r:number, g:number, b:number ):void
    {
      this.m_bmpEmpty.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255( r,g,b )];
    }

    public SetEmptyGridType( eType:Global.eEmptyGridType ):void
    {
      if ( this.IsTaken() )
      {
        return;
      }

  
      this.m_eEmptyGridType = eType;

      if (  this.m_eEmptyGridType == Global.eEmptyGridType.inside_city )
      {
        this.SetColor(105,105,105);
      }
      else if (  this.m_eEmptyGridType == Global.eEmptyGridType.city_edge )
      {
        this.SetColor(82, 173, 70); 
      }
    }

    public GetEmptyGridType():Global.eEmptyGridType
    {
      return this.m_eEmptyGridType;
    }

    public SetGuid( nGuid:number ):void
    {
      this.m_nGuid = nGuid;
    }

    public GetGuid(  ):number
    {
      return this.m_nGuid;
    }


} // end class CGrid