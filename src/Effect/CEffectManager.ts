class CEffectManager 
{
    public static s_lstUiEffect:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();

    public static Init():void
    {
        CEffectManager.s_lstUiEffect.addChild( CFrameAniManager.s_containerEffects );
    }

    public static FixedUpdate():void
    {
        CFrameAniManager.FixedUpdate();
    }

} // end class