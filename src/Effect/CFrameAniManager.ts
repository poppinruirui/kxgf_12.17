class CFrameAniManager extends egret.DisplayObjectContainer {

      public static s_containerEffects:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();



      public constructor() {
        super();

     

      } // constructor

      public static GetEffectsContainer():egret.DisplayObjectContainer  
        {
            return CFrameAniManager.s_containerEffects;
        }


      public static s_lstRecycledEffects:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
      public static NewEffect():CFrameAni
      {
          var effect:CFrameAni = null;
          if ( CFrameAniManager.s_lstRecycledEffects.numChildren > 0 )
          {
              effect = CFrameAniManager.s_lstRecycledEffects.getChildAt(0) as CFrameAni;
              CFrameAniManager.s_lstRecycledEffects.removeChildAt(0);
          }
          else
          {
              effect = new CFrameAni();
          }
 
         // CFrameAniManager.s_containerEffects.addChild( effect );
         CFrameAniManager.s_aryPlayingEffects.push( effect );

          return effect;
      }

      public static DeleteEffect( effect:CFrameAni ):void
      { 
          CFrameAniManager.s_lstRecycledEffects.addChild( effect );

          for ( var i:number = 0; i <  CFrameAniManager.s_aryPlayingEffects.length; i++ )
          {
              var effect_node:CFrameAni = CFrameAniManager.s_aryPlayingEffects[i] as CFrameAni;
              if ( effect_node == effect )
              {
                  CFrameAniManager.s_aryPlayingEffects.splice( i, 1 );
                  break;
              }
          }
      }

      public static FixedUpdate():void
      {
        
         for ( var i:number = CFrameAniManager.s_aryPlayingEffects.length - 1; i >= 0; i-- )
        
          {
             
              var effect:CFrameAni = CFrameAniManager.s_aryPlayingEffects[i] as CFrameAni;
             
              effect.FixedUpdate();
            
          } // end for`
      }

      protected static s_aryPlayingEffects:Array<CFrameAni> = new Array<CFrameAni> ();

} // end class