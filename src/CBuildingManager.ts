class CBuildingManager extends egret.DisplayObjectContainer {

     protected static m_aryCommonBuildings:Array<CConfigBuilding> = new Array<CConfigBuilding>();
     protected static m_aryLockedSpecialBuildings:Array<CConfigBuilding> = new Array<CConfigBuilding>();
     protected static m_aryUnLockedSpecialBuildings:Array<CConfigBuilding> = new Array<CConfigBuilding>();

     protected static m_bBuildingConfigLoaded:boolean = false;

   

     protected static m_nNumOfUnlockSpecialBuildings:number = 0;

     public constructor() {
        super();

     } // end constructor

      public static InsertBuildingConfig2List( nCommonOrSpecial:number, config:CConfigBuilding ):void
      {
          var ary:Array<CConfigBuilding> = null;
          if ( nCommonOrSpecial == 0 )
          {
            ary = CBuildingManager.m_aryCommonBuildings;
          }
          else 
          {
              ary = CBuildingManager.m_aryLockedSpecialBuildings;
          }

          ary.push( config );
      }

      public static LoadConfig():void
      {
          if ( CBuildingManager.m_bBuildingConfigLoaded )
          {
              return;
          }

          var dicBuildings:Object = CConfigManager.GetBuildingConfigDic();
          var dicSpecialBuildings:Object = CConfigManager.GetSpecialBuildingConfigDic();

          //// 普通建筑
          for ( var i:Global.eLotPsroperty = Global.eLotPsroperty.residential; i < Global.eLotPsroperty.length; i++ )
          {
              // 2x2
              var ary_2x2:Array<CConfigBuilding> = dicBuildings[i + "_" + Global.eObjSize.size_2x2];
              for ( var j:number = 0; j < ary_2x2.length; j++ )
              {
                  CBuildingManager.InsertBuildingConfig2List( 0, ary_2x2[j] );
              }

              // 4x4
              var ary_4x4:Array<CConfigBuilding> = dicBuildings[i + "_" + Global.eObjSize.size_4x4];
              if ( ary_4x4 != undefined )
              {
                for ( var j:number = 0; j < ary_4x4.length; j++ )
                {

                    CBuildingManager.InsertBuildingConfig2List( 0, ary_4x4[j] );
                }
              }
          }

          //// 特殊建筑
          for ( var i:Global.eLotPsroperty = Global.eLotPsroperty.residential; i < Global.eLotPsroperty.length; i++ )
          {
              // 2x2
              var ary_2x2:Array<CConfigBuilding> = dicSpecialBuildings[i+ "_" + Global.eObjSize.size_2x2];
              for ( var j:number = 0; j < ary_2x2.length; j++ )
              {
                  CBuildingManager.InsertBuildingConfig2List( 1, ary_2x2[j] );
              }

              // 4x4
          }

          CBuildingManager.m_bBuildingConfigLoaded = true;


          
      }

      public static GetCommonBuildings():Array<CConfigBuilding>
      {
          return CBuildingManager.m_aryCommonBuildings;
      }

      public static GetLockedSpecialBuildings():Array<CConfigBuilding>
      {
          return CBuildingManager.m_aryLockedSpecialBuildings;
      }

      public static GetUnLockSpecialBuildings():Array<CConfigBuilding>
      {
          return CBuildingManager.m_aryUnLockedSpecialBuildings;
      }

      public static PropertyType2String( eType:Global.eLotPsroperty ):string
      {
          switch(eType)
          { 
            case Global.eLotPsroperty.residential:
            {
                return "住宅";
            }
            break;

            case Global.eLotPsroperty.business:
            {
                return "商业";
            }
            break;

             case Global.eLotPsroperty.service:
            {
                return "服务";
            }
            break; 

          }

          return "有Bug!";
      }



      public static UnlockBuilding( data:CConfigBuilding ):void
      {
          data.bUnLocked = true;
          CBuildingManager.RemoveFromLockedList( data );
          CBuildingManager.AddToUnLockedList( data );

          CPlayer.SetDiamond( CPlayer.GetDiamond() - data.nPrice );

          CUIManager.s_uiSpecialPanel.SelectLastBulding(  );

          CUIManager.s_uiSpecialPanel.UpdateInfo(); 
          CUIManager.s_uiSpecialPanel.SwitchPage(1);

      }

      protected static RemoveFromLockedList( data:CConfigBuilding ):void
      {
          for ( var i:number = 0; i < CBuildingManager.m_aryLockedSpecialBuildings.length; i++ )
          {
              var node:CConfigBuilding = CBuildingManager.m_aryLockedSpecialBuildings[i];
              if ( node.nID == data.nID )
              { 
                  CBuildingManager.m_aryLockedSpecialBuildings.splice( i, 1 );
 
                  return;
              }
          }
      }

      protected static AddToUnLockedList( data:CConfigBuilding ):void
      {
          CBuildingManager.m_aryUnLockedSpecialBuildings.push( data );
      }

     public static UseBuilding( data:CConfigBuilding ):void
     {
             var obj:CObj = Main.s_CurTown.GetCurEditProcessingLot();
             obj.SetBuilding( data );
             obj.SetHistorical( true );

            if ( data.bSpecial )
            {
                Main.s_CurTown.UpdateCurLotStatus();
            }
     }

      public static RecycleSpecialBuilding( building:CConfigBuilding ):void
      {
          building.bUsed = false;

          CUIManager.s_uiSpecialPanel.UpdateInfo();

                  Main.s_CurTown.UpdateCurLotStatus();
      }

} // end class