
class CAutomobile extends CObj {

   protected m_shapeFortap:egret.Shape = new egret.Shape();

    protected m_bmpMoney:egret.Bitmap = new egret.Bitmap(); // （头顶）钱币

    protected m_Dir:Global.eAutomobileDir = Global.eAutomobileDir.youshang;

    protected m_CurRoad:CObj = null;
    protected m_NextRoad:CObj = null;

    protected m_shapeLocationPoint:egret.Shape = new egret.Shape();

    protected m_eStatus:Global.eAutomobileStatus = Global.eAutomobileStatus.none;
    protected m_nSubStatus:number = 0;
    protected m_nTempMoveAmount:number = 0;
    protected m_fTempGridPosX:number = 0;
    protected m_fTempGridPosY:number = 0;

    protected m_JinBi:CJinBi = null;

    protected m_bHasCoinNow:boolean = false;

    

    protected m_nCarIndex:number = 0;

    protected m_fBoundary_P1_X:number = 0;
    protected m_fBoundary_P1_Y:number = 0;
    protected m_fBoundary_P2_X:number = 0;
    protected m_fBoundary_P2_Y:number = 0;

    protected m_objDestRoad:CObj = null;

    protected m_nMoveDirX:number = 0;
    protected m_nMoveDirY:number = 0;
    protected m_fDestPosX:number = 0;
    protected m_fDestPosY:number = 0;

    public constructor() {
        super();

        this.addChild( this.m_bmpMoney );
        this.addChild( this.m_bmpMainPic );

        this.scaleX = 0.5;
        this.scaleY = 0.5;

        //this.m_bmpMoney.texture = RES.getRes ( "" );

/*
         this.m_shapeLocationPoint.graphics.beginFill(0xFF0000, 0.5);
         this.m_shapeLocationPoint.graphics.drawRect(0, 0, 10, 10);
         this.m_shapeLocationPoint.graphics.endFill();
         this.addChild( this.m_shapeLocationPoint );
         */

        this.touchEnabled = false;
      //  this.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapCar, this);
       // this.m_shapeFortap.touchEnabled = true;
        this.m_shapeFortap.graphics.beginFill( 0xFFFFFF, 0.01 );
        this.m_shapeFortap.graphics.drawRect( -300, 350, 700, -900 );
        this.m_shapeFortap.graphics.endFill();
        this.addChild( this.m_shapeFortap );
this.m_shapeFortap.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapCar, this);
    } // end constructor()

    public onTapCar(e: egret.TouchEvent):void{

        Main.s_nTapOtherPlaceBeforeTapStage = true;

        if ( this.m_JinBi != null )
        {
            this.m_JinBi.OnClick();
            this.m_shapeFortap.touchEnabled = false;
        }
    }

    public ClearBoundJinBi():void
    {
        this.m_JinBi = null;
        this.SetHasCoin( false );
    }

    public Reset():void
    {
        this.m_CurRoad = null;
        this.m_NextRoad = null;
    }

    public SetJinBi( jinbi:CJinBi ):void
    {
        this.m_JinBi = jinbi;

        this.m_shapeFortap.touchEnabled = true;
    }

    public GetDir():Global.eAutomobileDir
    {
        return this.m_Dir;
    }

    public UpdateDir(dir:Global.eAutomobileDir):void
    {
        this.m_Dir = dir;

       // this.m_bmpMainPic.texture = CAutomobile.re( dir, this.GetCarIndex() );
       this.GetAutoMobileResByDir( dir, this.GetCarIndex() );

        
    }

    public SetDestRoad( road:CObj ):void
    {
        this.m_objDestRoad = road;
        
        this.m_fDestPosX = this.m_objDestRoad.GetRealAnchorMiddle()["x"];
        this.m_fDestPosY = this.m_objDestRoad.GetRealAnchorMiddle()["y"];
        
        if ( this.m_fDestPosX > this.x )
        {
            this.m_nMoveDirX = 1;
        }
        else if ( this.m_fDestPosX < this.x )
        {
            this.m_nMoveDirX = -1;
        }
        else
        {
            this.m_nMoveDirX = 0;
        }

        if ( this.m_fDestPosY > this.y )
        {
            this.m_nMoveDirY = 1;
        }
        else if ( this.m_fDestPosY < this.y )
        {
            this.m_nMoveDirY = -1;
        }
        else
        {
            this.m_nMoveDirY = 0;
        }

    }

    public CheckIfArriveDest():boolean
    {
        var bEndX:boolean = false;
        var bEndY:boolean = false;

        if ( this.m_nMoveDirX > 0 )
        {
            if ( this.x >= this.m_fDestPosX )
            {
                bEndX = true;
            }
        }else if ( this.m_nMoveDirX < 0 )
        {
            if ( this.x <= this.m_fDestPosX )
            {
                bEndX = true;
            }
        }
        else
        {
            bEndX = true;
        }

        if ( this.m_nMoveDirY > 0 )
        {
            if ( this.y >= this.m_fDestPosY )
            {
                bEndY = true;
            }
        }else if ( this.m_nMoveDirY < 0 )
        {
            if ( this.y <= this.m_fDestPosY )
            {
                bEndY = true;
            }
        }
        else
        {
            bEndY = true;
        }

        if ( bEndX && bEndY )
        {
            return true;
        }

        return false;
    }

    public GetDestRoad( ):CObj
    {
        return this.m_objDestRoad;
    }

    public GetAutoMobileResByDir( dir:Global.eAutomobileDir, nCarIndex:number ):egret.Texture
    {
        // 改成网络版加载
         var url:string = CResourceManager.url;
        url += ( "Car/" + nCarIndex + "/" ) ;


       

          switch( dir )
        {
            case Global.eAutomobileDir.youshang:
            {
               // return RES.getRes( "car_" + nCarIndex + "_2" + "_png" );
               url += ( "car_" + nCarIndex + "_2" + ".png" )
            }
            break;

            case Global.eAutomobileDir.zuoshang:
            {
               // return RES.getRes( "car_" + nCarIndex + "_1" + "_png" );
               url += ( "car_" + nCarIndex + "_1" + ".png" );
            }
            break;

            case Global.eAutomobileDir.zuoxia:
            {  
               // return RES.getRes( "car_" + nCarIndex + "_0" + "_png" );
               url += ( "car_" + nCarIndex + "_0" + ".png" );
            }
            break;

            case Global.eAutomobileDir.youxia:
            {
               // return RES.getRes( "car_" + nCarIndex + "_3" + "_png" );
               url +=( "car_" + nCarIndex + "_3" + ".png" );
            }
            break;

        } // end switch

        RES.getResByUrl( url, this.onLoadCarResComplete, this, RES.ResourceItem.TYPE_IMAGE);

        return null;
    }

    public static GetCarResUrlByIdAndDir( nCarIndex:number, nDir:number ):string
    {
         // 改成网络版加载
         var url:string = CResourceManager.url;
        url += ( "Car/" + nCarIndex + "/" ) ;
        
          switch( nDir )
        {
            case Global.eAutomobileDir.youshang:
            {
               // return RES.getRes( "car_" + nCarIndex + "_2" + "_png" );
               url += ( "car_" + nCarIndex + "_2" + ".png" )
            }
            break;

            case Global.eAutomobileDir.zuoshang:
            {
               // return RES.getRes( "car_" + nCarIndex + "_1" + "_png" );
               url += ( "car_" + nCarIndex + "_1" + ".png" );
            }
            break;

            case Global.eAutomobileDir.zuoxia:
            {  
               // return RES.getRes( "car_" + nCarIndex + "_0" + "_png" );
               url += ( "car_" + nCarIndex + "_0" + ".png" );
            }
            break;

            case Global.eAutomobileDir.youxia:
            {
               // return RES.getRes( "car_" + nCarIndex + "_3" + "_png" );
               url +=( "car_" + nCarIndex + "_3" + ".png" );
            }
            break;

        } // end switch

        return url;
    }

     protected onLoadCarResComplete(event:any):void {
      
        var tex: egret.Texture = <egret.Texture>event;
        this.m_bmpMainPic.texture = tex;

          this.anchorOffsetX = this.m_bmpMainPic.width / 2;
                this.anchorOffsetY = this.m_bmpMainPic.height / 2 + 30;
     }


    public SetPos( posX:number, posY:number ):void
    {
        this.x = posX;
        this.y = posY;
    }

    public GetPosX():number
    {
        return this.x;
    }

    public GetPosY():number
    {
        return this.y;
    }

    public SetRaod( road:CObj ):void
    {
        this.m_CurRoad = road;

        // 预判即将踩到的下一块路面
        var nIndexX:number = road.GetLocationGrid().GetId()["nIndexX"];
        var nIndexY:number = road.GetLocationGrid().GetId()["nIndexY"];
        var nIndexX_Next:number = 0;
        var nIndexY_Next:number = 0;
        switch( this.m_Dir )
        {
            case Global.eAutomobileDir.youshang:
            {
                nIndexX_Next = nIndexX + 1;
                nIndexY_Next = nIndexY - 1;
            }
            break;
            case Global.eAutomobileDir.youxia:
            {
                nIndexX_Next = nIndexX + 1;
                nIndexY_Next = nIndexY + 1;
            }
            break;
            case Global.eAutomobileDir.zuoshang:
            {
                nIndexX_Next = nIndexX - 1;
                nIndexY_Next = nIndexY - 1;
            }
            break;
            case Global.eAutomobileDir.zuoxia:
            {
                 nIndexX_Next = nIndexX - 1;
                nIndexY_Next = nIndexY + 1;
            }
            break;


        } // end switch

        var grid_next:CGrid = Main.s_CurTown.GetGridByIndex( nIndexX_Next.toString(), nIndexY_Next.toString() );
        var road_next:CObj = null;
        if ( grid_next == undefined || grid_next == null )
        {
              
        }
        else
        {
            
            road_next = grid_next.GetBoundObj();
            if ( road_next == undefined || road_next == null || road_next.GetFuncType() != Global.eObjFunc.road )
            {
                 if ( road_next != null && road_next != undefined ){

                 }
                road_next = null;
            }
        }

        this.m_NextRoad = road_next; // null表示断头路：下一个地块不是马路，车辆不能通行

    }

    public GetRaod():CObj
    {
        return this.m_CurRoad;
    }

    public GetNextRoad():CObj{
        return this.m_NextRoad;
    }

    public GetHeight():number
    {
        return this.height;
    }

    public Turn_Right():void
    {
        if ( this.m_eStatus != Global.eAutomobileStatus.turn_right )
        {
            return;
        }

        

        CAutomobile.GetSpeedByDir( this.m_Dir );
        this.x += CAutomobile.s_objectSpeedByDir["x"];
        this.y += CAutomobile.s_objectSpeedByDir["y"];

        // check if Turn-left completed

        var nRet:number = 0;
        var grid:CGrid = this.m_NextRoad.GetLocationGrid();
        var bCanCompleted:boolean = false;
        switch( this.m_Dir )
        {
            case Global.eAutomobileDir.youshang:
            {
                nRet = CyberTreeMath.LeftOfLine( this.GetPos()["x"], this.GetPos()["y"], 
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_0),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_0),
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_5),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_5)
                );

                if ( nRet == -1 )
                {
                      this.UpdateDir( Global.eAutomobileDir.youxia );

                    bCanCompleted = true;
                }
            }
            break;
                
            case Global.eAutomobileDir.youxia:
            {
                nRet = CyberTreeMath.LeftOfLine( this.GetPos()["x"], this.GetPos()["y"], 
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_3),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_3),
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_6),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_6)
                );

                if ( nRet == -1 )
                {
                      this.UpdateDir( Global.eAutomobileDir.zuoxia );

                    bCanCompleted = true;
                }
            }
            break;

            case Global.eAutomobileDir.zuoshang:
            {
                nRet = CyberTreeMath.LeftOfLine( this.GetPos()["x"], this.GetPos()["y"], 
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_2),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_2),
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_7),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_7)
                );

                if ( nRet == 1 )
                {
                      this.UpdateDir( Global.eAutomobileDir.youshang );

                    bCanCompleted = true;
                }
            }
            break;

            case Global.eAutomobileDir.zuoxia:
            {
                nRet = CyberTreeMath.LeftOfLine( this.GetPos()["x"], this.GetPos()["y"], 
                grid.GetCornerPosX( Global.eGridCornerPosType.one_fourth_1),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_1),
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_4),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_4)
                );

                if ( nRet == 1 )
                {
                      this.UpdateDir( Global.eAutomobileDir.zuoshang );

                    bCanCompleted = true;
                }
            }
            break;

        } // end switch

        if ( bCanCompleted )
        {
            this.SetRaod( this.m_NextRoad );
            this.SetStartPosByDir(  this.m_Dir, this.m_CurRoad.x, this.m_CurRoad.y )
            this.m_eStatus = Global.eAutomobileStatus.normal;

        }
       
    } // end Turn_Left

    public Turn_Left():void
    {
        if ( this.m_eStatus != Global.eAutomobileStatus.turn_left )
        {
            return;
        }

       
        CAutomobile.GetSpeedByDir( this.m_Dir );
        this.x += CAutomobile.s_objectSpeedByDir["x"];
        this.y += CAutomobile.s_objectSpeedByDir["y"];

        // check if Turn-left completed

        var nRet:number = 0;
        var grid:CGrid = this.m_NextRoad.GetLocationGrid();
        var bCanCompleted:boolean = false;
        switch( this.m_Dir )
        {
            case Global.eAutomobileDir.youshang:
            {
                nRet = CyberTreeMath.LeftOfLine( this.GetPos()["x"], this.GetPos()["y"], 
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_4),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_4),
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_1),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_1)
                );

                if ( nRet == -1 )
                {
                      this.UpdateDir( Global.eAutomobileDir.zuoshang );

                    bCanCompleted = true;
                }
            }
            break;
                
            case Global.eAutomobileDir.youxia:
            {
                nRet = CyberTreeMath.LeftOfLine( this.GetPos()["x"], this.GetPos()["y"], 
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_2),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_2),
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_7),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_7)
                );

                if ( nRet == -1 )
                {
                      this.UpdateDir( Global.eAutomobileDir.youshang );

                    bCanCompleted = true;
                }
            }
            break;

            case Global.eAutomobileDir.zuoshang:
            {
                nRet = CyberTreeMath.LeftOfLine( this.GetPos()["x"], this.GetPos()["y"], 
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_3),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_3),
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_6),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_6)
                );

                if ( nRet == 1 )
                {
                      this.UpdateDir( Global.eAutomobileDir.zuoxia );

                    bCanCompleted = true;
                }
            }
            break;

            case Global.eAutomobileDir.zuoxia:
            {
                nRet = CyberTreeMath.LeftOfLine( this.GetPos()["x"], this.GetPos()["y"], 
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_0),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_0),
                grid.GetCornerPosX(Global.eGridCornerPosType.one_fourth_5),
                grid.GetCornerPosY(Global.eGridCornerPosType.one_fourth_5)
                );

                if ( nRet == 1 )
                {
                      this.UpdateDir( Global.eAutomobileDir.youxia );

                    bCanCompleted = true;
                }
            }
            break;

        } // end switch

        if ( bCanCompleted )
        {
            this.SetRaod( this.m_NextRoad );
            this.SetStartPosByDir(  this.m_Dir, this.m_CurRoad.x, this.m_CurRoad.y )
            this.SetMoveStatus(  Global.eAutomobileStatus.normal );

        }
       
    } // end Turn_Left

    public U_Turn():void
    {
        if ( this.m_eStatus != Global.eAutomobileStatus.u_turn )
        {
            return;
        }

        CAutomobile.GetSpeedByDir( this.m_Dir );
        this.x += CAutomobile.s_objectSpeedByDir["x"];
        this.y += CAutomobile.s_objectSpeedByDir["y"];
       
       
        if ( this.m_nSubStatus == 0 ) // 掉头的第一阶段
        {
            this.m_nTempMoveAmount += CAutomobile.s_objectSpeedByDir["x"];
            if ( Math.abs( this.m_nTempMoveAmount ) >= CDef.s_nTileWidth * 0.25 )
            {
                this.m_nSubStatus = 1;
               
                switch( this.m_Dir )
                {
                    case Global.eAutomobileDir.youxia:
                    {
         
                        this.UpdateDir( Global.eAutomobileDir.youshang );
                    }
                    break;

                    case Global.eAutomobileDir.zuoshang:
                    {
                        this.UpdateDir( Global.eAutomobileDir.zuoxia );
                    }
                    break;

                    case Global.eAutomobileDir.zuoxia:
                    {
                        this.UpdateDir( Global.eAutomobileDir.youxia );
                    }
                    break;

                    case Global.eAutomobileDir.youshang:
                    {
                        this.UpdateDir( Global.eAutomobileDir.zuoshang );
                    }
                    break;
                }

                 this.SetStartPosByDir(  this.m_Dir, this.m_fTempGridPosX, this.m_fTempGridPosY )
            }

        } // end 掉头的第一阶段
        else if ( this.m_nSubStatus == 1 ) // 掉头的第二阶段
        {
               var bLeaveTempGrid = CAutomobile.CheckIfLeaveGrid( this.m_Dir, this.m_fTempGridPosY, this.y, this.GetHeight() )
                if ( bLeaveTempGrid )
                {
                    this.m_eStatus = Global.eAutomobileStatus.normal;
                    this.SetRaod( this.m_CurRoad ); // 这里必须再重新setroad一次，不然next-road不会刷新 
                  //  this.SetStartPosByDir(  this.m_Dir, this.m_CurRoad.x, this.m_CurRoad.y )
                }
        }



    }

    public SetMoveStatus( eStatus:Global.eAutomobileStatus ):void
    {
      

        this.m_eStatus= eStatus;

    
    }

  protected m_aryTempAvailableDirs:Array<CObj> = new Array<CObj>();

        public GetNeighbourRoadByCurRunDir( eRunDir:Global.eAutomobileDir, objCurDestRoad:CObj  ):void
        {
             
                var road_zuoshang:CObj = null;
                var road_zuoxia:CObj = null;
                var road_youshang:CObj = null;
                var road_youxia:CObj = null;

                var cur_grid:CGrid = objCurDestRoad.GetLocationGrid();
                var nCurIndexX:number = cur_grid.GetId()["nIndexX"];
                var nCurIndexY:number = cur_grid.GetId()["nIndexY"];
                var nIndexX_ZuoShang = nCurIndexX - 1;
                var nIndexY_ZuoShang = nCurIndexY - 1;
                var nIndexX_YouShang = nCurIndexX + 1;
                var nIndexY_YouShang = nCurIndexY - 1;
                var nIndexX_ZuoXia = nCurIndexX - 1;
                var nIndexY_ZuoXia = nCurIndexY + 1;
                var nIndexX_YouXia = nCurIndexX + 1;
                var nIndexY_YouXia = nCurIndexY + 1;

                var grid_zuoshang:CGrid = Main.s_CurTown.GetGridByIndex( nIndexX_ZuoShang.toString(), nIndexY_ZuoShang.toString() );
                var grid_youshang:CGrid = Main.s_CurTown.GetGridByIndex( nIndexX_YouShang.toString(), nIndexY_YouShang.toString() );
                var grid_zuoxia:CGrid = Main.s_CurTown.GetGridByIndex( nIndexX_ZuoXia.toString(), nIndexY_ZuoXia.toString() );
                var grid_youxia:CGrid = Main.s_CurTown.GetGridByIndex( nIndexX_YouXia.toString(), nIndexY_YouXia.toString() );

                if ( grid_zuoshang )
                {
                        road_zuoshang = grid_zuoshang.GetBoundObj();
                        if (  road_zuoshang == null || road_zuoshang.GetFuncType() != Global.eObjFunc.road )
                        {
                                road_zuoshang = null;
                        }
                } 

                if ( grid_zuoxia )
                {
                        road_zuoxia = grid_zuoxia.GetBoundObj();
                        if (  road_zuoxia == null || road_zuoxia.GetFuncType() != Global.eObjFunc.road )
                        {
                                road_zuoxia = null;
                        }
                } 

                if ( grid_youshang )
                {
                        road_youshang = grid_youshang.GetBoundObj();
                        if (  road_youshang == null || road_youshang.GetFuncType() != Global.eObjFunc.road )
                        {
                                road_youshang = null;
                        }
                } 

                if ( grid_youxia )
                {
                        road_youxia = grid_youxia.GetBoundObj();
                        if (  road_youxia == null || road_youxia.GetFuncType() != Global.eObjFunc.road )
                        {
                                road_youxia = null;
                        }
                } 

                switch( eRunDir ) // 忽略掉后方，本游戏中汽车不会掉头走
                {
                        case Global.eAutomobileDir.youshang:
                        {
                                road_zuoxia = null;
                        }
                        break;

                        case Global.eAutomobileDir.zuoshang:
                        {
                                road_youxia = null;
                        }
                        break;
                                               
                         case Global.eAutomobileDir.youxia:
                        {
                                road_zuoshang = null;
                        }
                        break;
                                               
                         case Global.eAutomobileDir.zuoxia:
                        {
                                road_youshang = null;
                        }
                        break;
                } // end switch

                this.m_aryTempAvailableDirs.length = 0;
                if ( road_zuoshang )
                {
                        this.m_aryTempAvailableDirs.push(  road_zuoshang);
                        road_zuoshang.SetParams( "dir", Global.eAutomobileDir.zuoshang );
                }
                if ( road_zuoxia )
                {
                        this.m_aryTempAvailableDirs.push(  road_zuoxia);
                        road_zuoxia.SetParams( "dir", Global.eAutomobileDir.zuoxia );
                }
                if ( road_youshang )
                {
                        this.m_aryTempAvailableDirs.push(  road_youshang);
                        road_youshang.SetParams( "dir", Global.eAutomobileDir.youshang );
                }
                if ( road_youxia )
                {
                        this.m_aryTempAvailableDirs.push(  road_youxia);
                        road_youxia.SetParams( "dir", Global.eAutomobileDir.youxia );
                }

                 


        }


    public ReCalculateMoveDir():void
    {
                this.GetNeighbourRoadByCurRunDir( this.GetDir(), this.m_objDestRoad);

                var nRandomSelectedIndex:number =  Math.floor( Math.random() * this.m_aryTempAvailableDirs.length ) ;
                var selected_road:CObj = this.m_aryTempAvailableDirs[nRandomSelectedIndex];
                var eTurnDir:Global.eAutomobileDir =  selected_road.GetParamsByKey( "dir" );
                this.UpdateDir(eTurnDir);
                this.SetDestRoad( selected_road );
    }

    public Move():void
    {
       

         CAutomobile.GetSpeedByDir( this.m_Dir );
        this.x += CAutomobile.s_objectSpeedByDir["x"];
        this.y += CAutomobile.s_objectSpeedByDir["y"];

       

        if ( this.CheckIfArriveDest() )
        {
            this.SetPos( this.m_fDestPosX, this.m_fDestPosY );
            this.ReCalculateMoveDir();
        }

        /*
        if ( this.m_eStatus != Global.eAutomobileStatus.normal )
        {
            return;
        }

        CAutomobile.GetSpeedByDir( this.m_Dir );
        this.x += CAutomobile.s_objectSpeedByDir["x"];
        this.y += CAutomobile.s_objectSpeedByDir["y"];

        this.UpdateRoadInfo();*/
        
    }

    private static s_objectSpeedByDir:Object = new Object();
    public static GetSpeedByDir( dir:Global.eAutomobileDir ):void
    {
        var speedX = CDef.s_fAutomobileMoveDirX;
        var speedY = CDef.s_fAutomobileMoveDirY;

        switch( dir )
        {
            case Global.eAutomobileDir.youshang:
            {
                speedY= - speedY;
            }
            break;
             case Global.eAutomobileDir.youxia:
            {

            }
            break;
            case Global.eAutomobileDir.zuoshang:
            {
                speedX= - speedX;
                speedY= - speedY;
            }
            break;
            case Global.eAutomobileDir.zuoxia:
            {
                speedX= - speedX;
            }
            break;           
        } // end switch

        CAutomobile.s_objectSpeedByDir["x"] = speedX;
        CAutomobile.s_objectSpeedByDir["y"] = speedY;
    }

    public GetLocationPosX():number
    {
        return this.x + this.m_shapeLocationPoint.x;
    }

    public GetLocationPosY():number
    {
        return this.y + this.m_shapeLocationPoint.y;
    }

    public SetStartPosByDir( dir:Global.eAutomobileDir, nGridPosX:number,  nGridPosY:number):void
    {       // poppin trcik
        

               if ( dir == Global.eAutomobileDir.youshang )
                {
                        this.x = nGridPosX + CDef.s_nTileWidth * 0.125;
                        this.y = nGridPosY - CDef.s_nTileHeight * 0.375 ;
                }
                else if ( dir == Global.eAutomobileDir.youxia )
                {
                       this.x = nGridPosX - CDef.s_nTileWidth  * 0.125;
                       this.y = nGridPosY - CDef.s_nTileHeight * 0.375 ;
                }
                else if ( dir == Global.eAutomobileDir.zuoshang )
                {
                        this.x = nGridPosX + CDef.s_nTileWidth * 0.125;
                        this.y = nGridPosY - CDef.s_nTileHeight * 0.625;       
                }
                else if ( dir == Global.eAutomobileDir.zuoxia )
                {
                        this.x = nGridPosX - CDef.s_nTileWidth  * 0.125;
                        this.y = nGridPosY - CDef.s_nTileHeight * 0.625;; 
                }

              this.y += CDef.s_nTileHeight / 2;

                
    }

    public GenerateCurBoundary():void
    {

    }

    public CheckIfLeaveCurGrid():boolean
    {
        var grid:CGrid = this.GetRaod().GetLocationGrid();
        var nRet:number = 0;

        switch( this.m_Dir )
        {
             case Global.eAutomobileDir.youshang:
             {  
               nRet = CyberTreeMath.LeftOfLine( this.GetPosX(), this.GetPosY(), 
               grid.GetCornerPosX( Global.eGridCornerPosType.top ),
               grid.GetCornerPosY(Global.eGridCornerPosType.top), 
               grid.GetCornerPosX(Global.eGridCornerPosType.right),
               grid.GetCornerPosY(Global.eGridCornerPosType.right)  );

               if ( nRet == -1 )
               {
                   return true;
               }
             }
             break;

             case Global.eAutomobileDir.youxia:
             {
                nRet = CyberTreeMath.LeftOfLine( this.GetPosX(), this.GetPosY(), 
               grid.GetCornerPosX( Global.eGridCornerPosType.bottom ),
               grid.GetCornerPosY(Global.eGridCornerPosType.bottom), 
               grid.GetCornerPosX(Global.eGridCornerPosType.right),
               grid.GetCornerPosY(Global.eGridCornerPosType.right)  );

                if ( nRet == -1 )
               {
                   return true;
               }
             }
             break;

             case Global.eAutomobileDir.zuoshang:
             {
                 nRet = CyberTreeMath.LeftOfLine( this.GetPosX(), this.GetPosY(), 
               grid.GetCornerPosX( Global.eGridCornerPosType.top ),
               grid.GetCornerPosY(Global.eGridCornerPosType.top), 
               grid.GetCornerPosX(Global.eGridCornerPosType.left),
               grid.GetCornerPosY(Global.eGridCornerPosType.left)  );

                if ( nRet == 1 )
               {
                   return true;
               }
             }
             break;

             case Global.eAutomobileDir.zuoxia:
             {
                 nRet = CyberTreeMath.LeftOfLine( this.GetPosX(), this.GetPosY(), 
               grid.GetCornerPosX( Global.eGridCornerPosType.left ),
               grid.GetCornerPosY(Global.eGridCornerPosType.left), 
               grid.GetCornerPosX(Global.eGridCornerPosType.bottom),
               grid.GetCornerPosY(Global.eGridCornerPosType.bottom)  );

                              if ( nRet == 1 )
               {
                   return true;
               }
             }
             break;

        }


        return false;
    }

    public static CheckIfLeaveGrid( dir:Global.eAutomobileDir, nGridPosY:number, nCarPosY:number, nCarHeight:number ):boolean
    {   


        /*
        switch( dir )
        {
            case Global.eAutomobileDir.youshang:
            {
               
                if ( nCarPosY < ( nGridPosY - CDef.s_nTileHeight / 2 ) )
                {
                    //console.log( "离开" );
                     return true;
                }
            }
            break;

            case Global.eAutomobileDir.zuoshang:
            {
               
                if ( ( nCarPosY - 40 ) < ( nGridPosY - CDef.s_nTileHeight ) )
                {
                    //console.log( "离开" );
                    return  true;
                }
            }
            break;

            case Global.eAutomobileDir.youxia:
            {
               
                if ( ( nCarPosY - nCarHeight + 90 ) >  nGridPosY  )
                {
                    //console.log( "离开youxia" );
                    return  true;
                }
            }
            break;

             case Global.eAutomobileDir.zuoxia:
            {
               
                if ( ( nCarPosY - nCarHeight + 40 ) >  nGridPosY - CDef.s_nTileHeight / 2  )
                {
                   // console.log( "离开zuoxia" );
                   return true;
                }
            }
            break;
        } // end switch
        */


        return false;
    }

    protected UpdateRoadInfo():void
    {
        var bLeave:boolean = false;

        // 判断离开当前路块没有
       // bLeave = CAutomobile.CheckIfLeaveGrid( this.m_Dir, this.m_CurRoad.y, this.y, this.height);
       bLeave = this.CheckIfLeaveCurGrid();
      

        if ( !bLeave )
        {
            return;
        }


        if ( this.m_NextRoad == null ) // 没路了(原则上不会出现这种情况，地图编辑的时候不要出现断头路)
        {
            //  直接掉头
            this.JustUTurn();
            return;
        }
     

        ////  有路，判断是直行、左转、右转
        //  判断有没有直行的机会
        var nStraightIndexX:number = 0;
        var nStraightIndexY:number = 0;
        var nLeftIndexX:number = 0;
        var nLeftIndexY:number = 0;
        var nRightIndexX:number = 0;
        var nRightIndexY:number = 0;

        var nCurGridX:number = this.m_CurRoad.GetLocationGrid().GetId()["nIndexX"];
        var nCurGridY:number = this.m_CurRoad.GetLocationGrid().GetId()["nIndexY"];

        var grid_straight:CGrid = null;
        var road_straight:CObj = null;

        var grid_left:CGrid = null;
        var road_left:CObj = null;

        var grid_right:CGrid = null;
        var road_right:CObj = null;


        switch( this.m_Dir )
        {
            case Global.eAutomobileDir.youxia:
            {
                nStraightIndexX = nCurGridX + 2; 
                nStraightIndexY = nCurGridY + 2;
                nLeftIndexX = nCurGridX + 2;
                nLeftIndexY = nCurGridY;
                nRightIndexX = nCurGridX;
                nRightIndexY = nCurGridY + 2;
            }
            break;

            case Global.eAutomobileDir.zuoxia:
            {
                nStraightIndexX = nCurGridX - 2; 
                nStraightIndexY = nCurGridY + 2;
                nLeftIndexX = nCurGridX;
                nLeftIndexY = nCurGridY + 2;
                nRightIndexX = nCurGridX - 2;
                nRightIndexY = nCurGridY;
            }
            break;

            case Global.eAutomobileDir.youshang:
            {
                nStraightIndexX = nCurGridX + 2; 
                nStraightIndexY = nCurGridY - 2;
                nLeftIndexX = nCurGridX;
                nLeftIndexY = nCurGridY - 2;
                nRightIndexX = nCurGridX + 2;
                nRightIndexY = nCurGridY;
            }
            break;

            case Global.eAutomobileDir.zuoshang:
            {
                nStraightIndexX = nCurGridX - 2; 
                nStraightIndexY = nCurGridY - 2;
                nLeftIndexX = nCurGridX - 2;
                nLeftIndexY = nCurGridY;
                nRightIndexX = nCurGridX;
                nRightIndexY = nCurGridY - 2;
            }
            break;

        } // end switch

        grid_straight = Main.s_CurTown.GetGridByIndex( nStraightIndexX.toString(), nStraightIndexY.toString() );
        grid_left = Main.s_CurTown.GetGridByIndex( nLeftIndexX.toString(), nLeftIndexY.toString() );
        grid_right = Main.s_CurTown.GetGridByIndex( nRightIndexX.toString(), nRightIndexY.toString() );
 

        CAutomobile.s_aryTempChoice.length = 0;
        if ( grid_straight == null || grid_straight == undefined )
        {
            
        }
        else
        {
            road_straight = grid_straight.GetBoundObj();
            if ( road_straight != null && road_straight.GetFuncType() == Global.eObjFunc.road )
            {
                CAutomobile.s_aryTempChoice.push(0);
            }
            else
            {
                road_straight = null;
            }
        }
        
        if ( grid_left == null || grid_left == undefined )
        {
            
        }
        else
        {
            road_left = grid_left.GetBoundObj();
            if ( road_left != null &&road_left.GetFuncType() == Global.eObjFunc.road )
            {
                CAutomobile.s_aryTempChoice.push(1);
            }
            else
            {
                road_left = null;
            }
        }

        if ( grid_right == null || grid_right == undefined )
        {
            
        }
        else
        {
            road_right = grid_right.GetBoundObj();
            if ( road_right != null && road_right.GetFuncType() == Global.eObjFunc.road )
            {
                CAutomobile.s_aryTempChoice.push(2);
            }
            else
            {
                road_right = null;
            }
        }

       
      //  console.log( CAutomobile.s_aryTempChoice.length );
        if ( CAutomobile.s_aryTempChoice.length == 0 )
        {
          this.JustUTurn();
          return;
        }

        

        var nChoice:number = 0;
        if ( CAutomobile.s_aryTempChoice.length == 1 )
        {
            nChoice = CAutomobile.s_aryTempChoice[0];
        }
        else if ( CAutomobile.s_aryTempChoice.length == 2 )
        {
            var nRan:number = Math.random();
            if ( nRan < 0.5 )
            {
                nChoice = CAutomobile.s_aryTempChoice[0];
            }
            else
            {
                nChoice = CAutomobile.s_aryTempChoice[1];
            }
        } else if ( CAutomobile.s_aryTempChoice.length == 3 )
        {
            var nRan:number = Math.random();
            if ( nRan < 0.33 )
            {
                nChoice = CAutomobile.s_aryTempChoice[0];
            }
            else if ( nRan < 0.66 )
            {
                nChoice = CAutomobile.s_aryTempChoice[1];
            }
            else
            {
                nChoice = CAutomobile.s_aryTempChoice[2];
            }
        }

        if ( nChoice == 0 ) // 直行
        {
            this.SetRaod( this.m_NextRoad ); // 注意不是road_straight 

            // 继续维持直行状态。即便状态并没改变，也必须重新执行SetMoveStatus（）, 因为当前准备进入下一个地块了，牵涉的路面数据要更新
            this.SetMoveStatus( Global.eAutomobileStatus.normal ); 
        }
        else if ( nChoice == 1 ) // 左转
        {
            //this.JustTurnLeft();
            // this.SetRaod( this.m_NextRoad ); 
             this.SetMoveStatus( Global.eAutomobileStatus.turn_left ); 
        }
        else // 右转
        {
          // this.SetRaod( this.m_NextRoad ); 
             this.SetMoveStatus( Global.eAutomobileStatus.turn_right ); 
           
        } // end 右转

    } // end UpdateRoadInfo()
    
    public static s_aryTempChoice:Array<number> = new Array<number>();

    protected JustTurnLeft():void
    {
         this.m_eStatus = Global.eAutomobileStatus.turn_left;
         this.m_nSubStatus = 0;
            this.m_nTempMoveAmount = 0;
    }

    protected JustUTurn():void
    {

            switch( this.m_Dir )
            {
                case Global.eAutomobileDir.zuoxia:
                {
                    this.UpdateDir( Global.eAutomobileDir.youxia );
                    this.m_fTempGridPosX = this.m_CurRoad.x - CDef.s_nTileWidth * 0.5;
                    this.m_fTempGridPosY = this.m_CurRoad.y + CDef.s_nTileHeight * 0.5;
                }
                break;

                case Global.eAutomobileDir.youshang:
                {
                    this.UpdateDir( Global.eAutomobileDir.zuoshang );
                    this.m_fTempGridPosX = this.m_CurRoad.x + CDef.s_nTileWidth * 0.5;
                    this.m_fTempGridPosY = this.m_CurRoad.y - CDef.s_nTileHeight * 0.5;
                }
                break;

                case Global.eAutomobileDir.zuoshang:
                {
                    this.UpdateDir( Global.eAutomobileDir.zuoxia );
                    this.m_fTempGridPosX = this.m_CurRoad.x - CDef.s_nTileWidth * 0.5;
                    this.m_fTempGridPosY = this.m_CurRoad.y - CDef.s_nTileHeight * 0.5;
                }
                break;

               case Global.eAutomobileDir.youxia:
                {
                    this.UpdateDir( Global.eAutomobileDir.youshang );
                    this.m_fTempGridPosX = this.m_CurRoad.x + CDef.s_nTileWidth * 0.5;
                    this.m_fTempGridPosY = this.m_CurRoad.y + CDef.s_nTileHeight * 0.5;
                }
                break;
            } // end switch
            this.m_eStatus = Global.eAutomobileStatus.u_turn;
            this.m_nSubStatus = 0;
            this.m_nTempMoveAmount = 0;
    }

    protected m_fReInsertSortTimeElapse:number = 0;
    public ReInsertSort():void
    {
        this.m_fReInsertSortTimeElapse += CDef.s_fFixedDeltaTime;
        if ( this.m_fReInsertSortTimeElapse < CDef.s_fCarReInsertSortInterval )
        {
            return;
        }
        this.m_fReInsertSortTimeElapse = 0;
        this.SetSortPosY();
        Main.s_CurTown.ReInsertSortObj( this );

    }

    public SetHasCoin( bHas:boolean ):void
    {
        this.m_bHasCoinNow = bHas;
    }

    public GetHasCoin():boolean
    {
        return this.m_bHasCoinNow;
    }

    protected m_nGenCoinTimeElapse:number = 0;
    public GenerateCoinLoop():void
    {
        if ( this.GetHasCoin() )
        {
            return;
        }

        // Wiki中缺少这部分的数值配置，只有自己凭感觉弄
        var bGenCoin:boolean = false;
        this.m_nGenCoinTimeElapse += CDef.s_fFixedDeltaTime;
        if ( this.m_nGenCoinTimeElapse < 10 )
        {
             return;          
        }
        this.m_nGenCoinTimeElapse = 0;

        var nTemp:number = Math.random();
        if (nTemp < 0.85 ) // 生成金币
        {
                var jinbi:CJinBi = CJinBiManager.NewJinBi();
                jinbi.name = "jinbi";
                this.addChild(jinbi);
                jinbi.x = 60;
                jinbi.y = -100;
                jinbi.anchorOffsetX = jinbi.width / 2;
                 jinbi.anchorOffsetY = jinbi.height / 2;
                jinbi.SetBoundObj( this );
                this.SetJinBi( jinbi );
                jinbi.Update();
                jinbi.visible = true;   
                this.SetHasCoin( true );

                var nSize:number = Math.random();
                var eSubSize:Global.eMoneySubType;
                if ( nSize < 0.8 )
                {
                    eSubSize = Global.eMoneySubType.small_coin;
                    jinbi.scaleX = 1.5;
                    jinbi.scaleY = 1.5;
                }
                else
                {
                     jinbi.scaleX = 3;
                     jinbi.scaleY = 3;
                     eSubSize = Global.eMoneySubType.big_coin;
                }

               
                var nValue:number = CConfigManager.GetCarJinBiValueRate( eSubSize ) * Main.s_CurTown.GetCPS();
                jinbi.SetSubType( eSubSize );
                jinbi.SetValue( Global.eMoneyType.coin,nValue );
        }  
        else // 生成绿票
        {
                eSubSize = Global.eMoneySubType.small_diamond;
                var jinbi:CJinBi = CJinBiManager.NewJinBi();
                jinbi.name = "lvpiao";
                this.addChild(jinbi);
                jinbi.x = 60;
                jinbi.y = -100;
                jinbi.anchorOffsetX = jinbi.width / 2;
                 jinbi.anchorOffsetY = jinbi.height / 2;
                jinbi.SetBoundObj( this );
                this.SetJinBi( jinbi );
                jinbi.Update();
                jinbi.visible = true;   
                this.SetHasCoin( true );
                jinbi.scaleX = 1.5;
                jinbi.scaleY = 1.5;
              

                var nSize:number = Math.random();
                var eSubSize:Global.eMoneySubType;
                if ( nSize < 0.8 )
                {
                    eSubSize = Global.eMoneySubType.small_diamond;
                    jinbi.scaleX = 1.5;
                    jinbi.scaleY = 1.5;
                }
                else
                {
                     jinbi.scaleX = 3;
                     jinbi.scaleY = 3;
                     eSubSize = Global.eMoneySubType.big_diamond;
                }
                var nValue:number = CConfigManager.GetCarJinBiValueRate( eSubSize );
                jinbi.SetValue( Global.eMoneyType.diamond,nValue );
    


                jinbi.SetSubType( eSubSize );
        }
             
    } // end       GenerateCoinLoop     

    public JinBiLoop():void
    {
        if ( this.m_JinBi == null )
        {
            return;
        }
        this.m_JinBi.Update();
    }

    public SetCarIndex( nIndex:number ):void
    {
        this.m_nCarIndex = nIndex;
    }

    public GetCarIndex():number   
    {
        return this.m_nCarIndex;
    }

} // end class

