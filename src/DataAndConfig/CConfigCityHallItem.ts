class CConfigCityHallItem extends egret.DisplayObjectContainer {

     public szDesc:string = "";
     public szName:string = "";
     public nGain:number = 0;
     public eShowStyle:Global.eValueShowStyle = Global.eValueShowStyle.directly;
     public aryValuesCost:Array<number> = new Array<number>();

     public constructor() {
         super();

     } // end constructor

     public GetMaxLevel():number
     {
         return this.aryValuesCost.length;
     }

} // end class