/*
    风车
*/
class CWindTurbine extends CObj {

       protected m_nFrameNum:number = 2;
       protected m_nFrameIndex:number = 0; 

       protected m_bmpFan:egret.Bitmap = new egret.Bitmap();

       public constructor() {
        super();

        this.addChild( this.m_bmpFan );

        this.m_bmpMainPic.texture = RES.getRes( "windturbine_stick_png" );
        this.m_bmpFan.x = 0;
        this.m_bmpFan.y = 0;


        this.scaleX = 0.5;
        this.scaleY = 0.5;

      } // end constructor

      public Reset():void
      {
          super.Reset();

          this.m_bmpFan.texture = null;
      }

      public Loop():void
      {
          if ( this.m_nFrameIndex >= this.m_nFrameNum )
          {
              this.m_nFrameIndex = 0;
          }
          var szResName:string = "windturbine_fan" + this.m_nFrameIndex + "_png";
          this.m_bmpFan.texture = RES.getRes( szResName );
          this.m_nFrameIndex++;
      }

} // end class