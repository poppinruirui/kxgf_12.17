class CJinBiManager extends CObj {

    public static s_containerJinBi:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public static s_aryJinBiAniFrames:Array<egret.Bitmap> = new Array<egret.Bitmap>();
    public static s_aryLvPiaoAniFrames:Array<egret.Bitmap> = new Array<egret.Bitmap>();
    

    public static Init():void
    {
        for ( var i:number = 0; i < CDef.s_fJinBiAniFrameNum; i++ )
        {
            var bmp:egret.Bitmap = new egret.Bitmap( RES.getRes ( "jinbi_" + i + "_png" ) );
            CJinBiManager.s_aryJinBiAniFrames.push( bmp );

            bmp = new egret.Bitmap( RES.getRes ( "lvpiao_" + i + "_png" ) );
            CJinBiManager.s_aryLvPiaoAniFrames.push( bmp );
        }
    }

    public static GetFrameTexture( nFrameIndex:number, eType:Global.eMoneyType = Global.eMoneyType.coin ):egret.Texture
    {
        if ( eType == Global.eMoneyType.coin )
        {
            return CJinBiManager.s_aryJinBiAniFrames[nFrameIndex].texture;
        }
        else
        {
            return CJinBiManager.s_aryLvPiaoAniFrames[nFrameIndex].texture;
        }
    }

    public static NewJinBi():CJinBi
    {
        var jinbi:CJinBi = null;
        if (  CJinBiManager.s_listRecycledJinBi.numChildren > 0 )
        {
            jinbi = CJinBiManager.s_listRecycledJinBi.getChildAt(0) as CJinBi;
            CJinBiManager.s_listRecycledJinBi.removeChildAt(0);
        }
        else
        {
            jinbi = new CJinBi();
            
        }
        
        //CJinBiManager.s_containerJinBi.addChild( jinbi );

        return jinbi;
    }

    protected static s_listRecycledJinBi:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
    public static DeleteJinBi( jinbi:CJinBi ):void
    {
        CJinBiManager.s_listRecycledJinBi.addChild( jinbi );
    }

    public static FixedUpdate():void
    {
        return;

        for ( var i:number = CJinBiManager.s_containerJinBi.numChildren - 1; i >= 0; i-- )
        {
            var jinbi:CJinBi = CJinBiManager.s_containerJinBi.getChildAt(i) as CJinBi;
            jinbi.Update();
        }
    }

} // end class