class CTiaoZi extends egret.DisplayObjectContainer {

    protected m_bmpIcon:egret.Bitmap;
    protected m_txtValue:egret.TextField;
    protected m_txtValue_outline:egret.TextField;

    protected m_fV0:number = 0;
    protected m_fA:number = 0;
    protected m_fAlphaChangeSpeed:number = 0;
    protected m_fTimeElapse:number = 0;

    protected m_eType:Global.eTiaoZiType = Global.eTiaoZiType.money;

    public Reset():void
    {
        this.alpha = 1;
    }

    public constructor() {
        super();

        this.m_bmpIcon = new egret.Bitmap(  CJinBiManager.GetFrameTexture(0) );
        this.addChild( this.m_bmpIcon );

        this.m_txtValue = new egret.TextField();
       
        this.m_txtValue.text = "21.99M";
        this.m_txtValue.x = 120;
        this.m_txtValue.y = 15;
        this.m_txtValue.size = 75;
        this.m_txtValue.bold = true;


        this.m_txtValue_outline = new egret.TextField();
        this.addChild( this.m_txtValue_outline );
        this.m_txtValue_outline.text = this.m_txtValue.text;
        this.m_txtValue_outline.x = this.m_txtValue.x + 1;
        this.m_txtValue_outline.y = this.m_txtValue.y - 1;
        this.m_txtValue_outline.size = this.m_txtValue.size;
        this.m_txtValue_outline.bold = true;
        this.m_txtValue_outline.textColor = 0x000000;
        this.addChild( this.m_txtValue );

        this.scaleX = 1.2;
        this.scaleY = 1.2;
    } // end constructor


    public FixedUpdate():void
    {
        this.m_fTimeElapse += CDef.s_fFixedDeltaTime;
        if ( this.m_fTimeElapse >= CDef.s_fTiaoZiTotalTime )
        {
            CTiaoZiManager.DeleteTiaoZi( this );
            return;
        }

        this.y += this.m_fV0;
        this.m_fV0 += this.m_fA;
        if ( this.m_fTimeElapse >= CDef.s_fTiaoZiTotalTime * 0.5 )
        {
            this.alpha += this.m_fAlphaChangeSpeed;
        }
    }

    public BeginTiaoZi( eType:Global.eTiaoZiType = Global.eTiaoZiType.money ):void
    {
        this.m_fV0 = CTiaoZiManager.s_fPosChangeV0 * CDef.s_fFixedDeltaTime;
        this.m_fA = CTiaoZiManager.s_fPosChangeA * CDef.s_fFixedDeltaTime;
        this.m_fAlphaChangeSpeed = CTiaoZiManager.s_fAlphaChangeSpeed * CDef.s_fFixedDeltaTime;
        this.m_fTimeElapse = 0;

        this.FixedUpdate();

        switch(eType)
        {
            case Global.eTiaoZiType.money:
            {
                this.m_bmpIcon.visible = true;
            }
            break;

            case Global.eTiaoZiType.level:
            {
                this.m_bmpIcon.visible = false;
            }
            break;
        }

    }

    public SetTypeAndValue( eType:Global.eMoneyType, nValue:number ):void{
        this.m_txtValue.text = (Math.floor(nValue)).toString();
        this.m_txtValue_outline.text = this.m_txtValue.text;
        switch( eType ) 
        {
            case Global.eMoneyType.coin:
            {
                this.m_bmpIcon.texture = CJinBiManager.GetFrameTexture(0);
            }
            break;

            case Global.eMoneyType.diamond:
            {
                this.m_bmpIcon.texture =CJinBiManager.GetFrameTexture(0, Global.eMoneyType.diamond);
            }
            break;

           
        } // end switch
       
       this.m_bmpIcon.width = 113;
       this.m_bmpIcon.height = 108;
    }

    public  SetText( szContent:string ):void
    {
        this.m_txtValue.text = szContent;
        this.m_txtValue_outline.text = this.m_txtValue.text;
    } 

} // end class