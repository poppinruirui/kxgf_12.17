 /*
  音频系统
 */
module Global{
    export enum eSE{ //  音效
        bank,
        big_coin,
        car,
        click_button,
        common_page_1,
        common_page_2,
        green_bux,
        small_coin,
        congratulations,
        begin_upgrade,
        upgrad_complete,
        revert,

        num,
    }

}// end module Global

 class CSoundManager extends egret.DisplayObjectContainer {
 
     public static s_soundBMG:egret.Sound;
     protected static s_arySE:Array<egret.Sound>;
     protected static s_dicSE:Object = new Object();

      public constructor() {
        super();
      }

      private static onLoadMusicComplete(event:any):void {
        
          CSoundManager.s_soundBMG = <egret.Sound>event;
         CSoundManager.s_soundBMG .play();
   

         

      }

      protected static onLoadAudioComplete_SmallCoin( event:any ):void
      {
        //    CUIManager.s_uiMainTitle.ShowDebugInfo(  "load audio completed:" +  event.type );
           CSoundManager.s_dicSE[Global.eSE.small_coin] = <egret.Sound>event;
      }

     public static Init():void
     {
        // var sound1:egret.Sound = RES.getResByUrl();
 CSoundManager.s_soundBMG = new egret.Sound();
   CSoundManager.s_soundBMG.addEventListener(egret.Event.COMPLETE, function loadOver(event:egret.Event) {
            console.log("loaded bmg completed!");
             
        }, this);
        CSoundManager.s_soundBMG.addEventListener(egret.IOErrorEvent.IO_ERROR, function loadError(event:egret.IOErrorEvent) {
            console.log("loaded bmg error!");
        }, this);
//resource/assets/Audios/small_coin


      var music_url:string = CResourceManager.url + "bmg1.mp3";
       RES.getResByUrl( music_url ,this.onLoadMusicComplete,this,RES.ResourceItem.TYPE_SOUND);
 /*
 var loader:egret.URLLoader = new egret.URLLoader();
  loader.addEventListener(egret.Event.COMPLETE, function loadOver(event:egret.Event) {
  console.log( "背景音乐加载成功" );
      var sound:egret.Sound = loader.data;
      sound.play();
  }, this);
  loader.dataFormat = egret.URLLoaderDataFormat.SOUND;
  loader.load(new egret.URLRequest(music_url));
*/


      //  var audio_url:string = "http://39.108.53.61:5566/data/small_coin.mp3";
      //  RES.getResByUrl( audio_url ,this.onLoadAudioComplete_SmallCoin,this,RES.ResourceItem.TYPE_SOUND);


         /*
         CSoundManager.s_soundBMG = new egret.Sound();
        CSoundManager.s_soundBMG.addEventListener(egret.Event.COMPLETE, function loadOver(event:egret.Event) {
           
             
        }, this);
        CSoundManager.s_soundBMG.addEventListener(egret.IOErrorEvent.IO_ERROR, function loadError(event:egret.IOErrorEvent) {
            console.log("loaded bmg error!");
        }, this);
        CSoundManager.s_soundBMG.load("resource/assets/Music/duokeli.mp3");
        */
        
        CSoundManager.s_arySE = new Array<egret.Sound>();

        var sound:egret.Sound = new egret.Sound();
        sound.load("resource/assets/Audios/bank.mp3");
        CSoundManager.s_arySE.push(  sound); 

        sound = new egret.Sound();
        sound.load("resource/assets/Audios/big_coin.mp3");
        CSoundManager.s_arySE.push(  sound); 

           sound = new egret.Sound();
        sound.load("resource/assets/Audios/car.mp3");
        CSoundManager.s_arySE.push(  sound); 

           sound = new egret.Sound();
        sound.load("resource/assets/Audios/click_button.mp3");
        CSoundManager.s_arySE.push(  sound); 

           sound = new egret.Sound();
        sound.load("resource/assets/Audios/common_page_1.mp3");
        CSoundManager.s_arySE.push(  sound); 

           sound = new egret.Sound();
        sound.load("resource/assets/Audios/common_page_2.mp3");
        CSoundManager.s_arySE.push(  sound); 

           sound = new egret.Sound();
        sound.load("resource/assets/Audios/green_bux.mp3");
        CSoundManager.s_arySE.push(  sound); 

        sound = new egret.Sound();
        sound.load("resource/assets/Audios/small_coin.mp3");
        CSoundManager.s_arySE.push(  sound); 

         sound = new egret.Sound();
        sound.load("resource/assets/Audios/congratulations.mp3");
        CSoundManager.s_arySE.push(  sound); 

        sound = new egret.Sound();
        sound.load("resource/assets/Audios/start_upgrade.mp3");
        CSoundManager.s_arySE.push(  sound);

        sound = new egret.Sound();
        sound.load("resource/assets/Audios/end_upgrade.mp3");
        CSoundManager.s_arySE.push(  sound);

        sound = new egret.Sound();
        sound.load("resource/assets/Audios/revert.mp3");
        CSoundManager.s_arySE.push(  sound);

     }
 
     public static PlaySE( eId:Global.eSE ):void
     {
         
         
      //  CSoundManager.s_arySE[eId].play( 0, 1 );
      var sound:egret.Sound  = CSoundManager.s_arySE[eId] as egret.Sound;
      if ( sound == null || sound == undefined )
      {
          return;
      }
      sound.play(0, 1);

     }

     protected static s_bBMGPlaying:boolean = false;
     public static PlayBMG():void
     {
         if ( CSoundManager.s_bBMGPlaying )
         {
             return;
         }
         CSoundManager.s_soundBMG.play();
         CSoundManager.s_bBMGPlaying = true;
     }

 } // end class