window.skins={};
                function __extends(d, b) {
                    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
                        function __() {
                            this.constructor = d;
                        }
                    __.prototype = b.prototype;
                    d.prototype = new __();
                };
                window.generateEUI = {};
                generateEUI.paths = {};
                generateEUI.styles = undefined;
                generateEUI.skins = {"eui.Button":"resource/eui_skins/ButtonSkin.exml","eui.CheckBox":"resource/eui_skins/CheckBoxSkin.exml","eui.HScrollBar":"resource/eui_skins/HScrollBarSkin.exml","eui.HSlider":"resource/eui_skins/HSliderSkin.exml","eui.Panel":"resource/eui_skins/PanelSkin.exml","eui.TextInput":"resource/eui_skins/TextInputSkin.exml","eui.ProgressBar":"resource/eui_skins/ProgressBarSkin.exml","eui.RadioButton":"resource/eui_skins/RadioButtonSkin.exml","eui.Scroller":"resource/eui_skins/ScrollerSkin.exml","eui.ToggleSwitch":"resource/eui_skins/ToggleSwitchSkin.exml","eui.VScrollBar":"resource/eui_skins/VScrollBarSkin.exml","eui.VSlider":"resource/eui_skins/VSliderSkin.exml","eui.ItemRenderer":"resource/eui_skins/ItemRendererSkin.exml"};generateEUI.paths['resource/eui_skins/ButtonSkin.exml'] = window.skins.ButtonSkin = (function (_super) {
	__extends(ButtonSkin, _super);
	function ButtonSkin() {
		_super.call(this);
		this.skinParts = ["labelDisplay","iconDisplay"];
		
		this.minHeight = 50;
		this.minWidth = 100;
		this.elementsContent = [this._Image1_i(),this.labelDisplay_i(),this.iconDisplay_i()];
		this.states = [
			new eui.State ("up",
				[
				])
			,
			new eui.State ("down",
				[
					new eui.SetProperty("_Image1","source","button_down_png")
				])
			,
			new eui.State ("disabled",
				[
					new eui.SetProperty("_Image1","alpha",0.5)
				])
		];
	}
	var _proto = ButtonSkin.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		this._Image1 = t;
		t.percentHeight = 100;
		t.scale9Grid = new egret.Rectangle(1,3,8,8);
		t.source = "button_up_png";
		t.percentWidth = 100;
		return t;
	};
	_proto.labelDisplay_i = function () {
		var t = new eui.Label();
		this.labelDisplay = t;
		t.bottom = 8;
		t.left = 8;
		t.right = 8;
		t.size = 20;
		t.textAlign = "center";
		t.textColor = 0xFFFFFF;
		t.top = 8;
		t.verticalAlign = "middle";
		return t;
	};
	_proto.iconDisplay_i = function () {
		var t = new eui.Image();
		this.iconDisplay = t;
		t.horizontalCenter = 0;
		t.verticalCenter = 0;
		return t;
	};
	return ButtonSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/CheckBoxSkin.exml'] = window.skins.CheckBoxSkin = (function (_super) {
	__extends(CheckBoxSkin, _super);
	function CheckBoxSkin() {
		_super.call(this);
		this.skinParts = ["labelDisplay"];
		
		this.elementsContent = [this._Group1_i()];
		this.states = [
			new eui.State ("up",
				[
				])
			,
			new eui.State ("down",
				[
					new eui.SetProperty("_Image1","alpha",0.7)
				])
			,
			new eui.State ("disabled",
				[
					new eui.SetProperty("_Image1","alpha",0.5)
				])
			,
			new eui.State ("upAndSelected",
				[
					new eui.SetProperty("_Image1","source","checkbox_select_up_png")
				])
			,
			new eui.State ("downAndSelected",
				[
					new eui.SetProperty("_Image1","source","checkbox_select_down_png")
				])
			,
			new eui.State ("disabledAndSelected",
				[
					new eui.SetProperty("_Image1","source","checkbox_select_disabled_png")
				])
		];
	}
	var _proto = CheckBoxSkin.prototype;

	_proto._Group1_i = function () {
		var t = new eui.Group();
		t.percentHeight = 100;
		t.percentWidth = 100;
		t.layout = this._HorizontalLayout1_i();
		t.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
		return t;
	};
	_proto._HorizontalLayout1_i = function () {
		var t = new eui.HorizontalLayout();
		t.verticalAlign = "middle";
		return t;
	};
	_proto._Image1_i = function () {
		var t = new eui.Image();
		this._Image1 = t;
		t.alpha = 1;
		t.fillMode = "scale";
		t.source = "checkbox_unselect_png";
		return t;
	};
	_proto.labelDisplay_i = function () {
		var t = new eui.Label();
		this.labelDisplay = t;
		t.fontFamily = "Tahoma";
		t.size = 20;
		t.textAlign = "center";
		t.textColor = 0x707070;
		t.verticalAlign = "middle";
		return t;
	};
	return CheckBoxSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/HScrollBarSkin.exml'] = window.skins.HScrollBarSkin = (function (_super) {
	__extends(HScrollBarSkin, _super);
	function HScrollBarSkin() {
		_super.call(this);
		this.skinParts = ["thumb"];
		
		this.minHeight = 8;
		this.minWidth = 20;
		this.elementsContent = [this.thumb_i()];
	}
	var _proto = HScrollBarSkin.prototype;

	_proto.thumb_i = function () {
		var t = new eui.Image();
		this.thumb = t;
		t.height = 8;
		t.scale9Grid = new egret.Rectangle(3,3,2,2);
		t.source = "roundthumb_png";
		t.verticalCenter = 0;
		t.width = 30;
		return t;
	};
	return HScrollBarSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/HSliderSkin.exml'] = window.skins.HSliderSkin = (function (_super) {
	__extends(HSliderSkin, _super);
	function HSliderSkin() {
		_super.call(this);
		this.skinParts = ["track","thumb"];
		
		this.minHeight = 8;
		this.minWidth = 20;
		this.elementsContent = [this.track_i(),this.thumb_i()];
	}
	var _proto = HSliderSkin.prototype;

	_proto.track_i = function () {
		var t = new eui.Image();
		this.track = t;
		t.height = 6;
		t.scale9Grid = new egret.Rectangle(1,1,4,4);
		t.source = "track_sb_png";
		t.verticalCenter = 0;
		t.percentWidth = 100;
		return t;
	};
	_proto.thumb_i = function () {
		var t = new eui.Image();
		this.thumb = t;
		t.source = "thumb_png";
		t.verticalCenter = 0;
		return t;
	};
	return HSliderSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/ItemRendererSkin.exml'] = window.skins.ItemRendererSkin = (function (_super) {
	__extends(ItemRendererSkin, _super);
	function ItemRendererSkin() {
		_super.call(this);
		this.skinParts = ["labelDisplay"];
		
		this.minHeight = 50;
		this.minWidth = 100;
		this.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
		this.states = [
			new eui.State ("up",
				[
				])
			,
			new eui.State ("down",
				[
					new eui.SetProperty("_Image1","source","button_down_png")
				])
			,
			new eui.State ("disabled",
				[
					new eui.SetProperty("_Image1","alpha",0.5)
				])
		];
		
		eui.Binding.$bindProperties(this, ["hostComponent.data"],[0],this.labelDisplay,"text");
	}
	var _proto = ItemRendererSkin.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		this._Image1 = t;
		t.percentHeight = 100;
		t.scale9Grid = new egret.Rectangle(1,3,8,8);
		t.source = "button_up_png";
		t.percentWidth = 100;
		return t;
	};
	_proto.labelDisplay_i = function () {
		var t = new eui.Label();
		this.labelDisplay = t;
		t.bottom = 8;
		t.fontFamily = "Tahoma";
		t.left = 8;
		t.right = 8;
		t.size = 20;
		t.textAlign = "center";
		t.textColor = 0xFFFFFF;
		t.top = 8;
		t.verticalAlign = "middle";
		return t;
	};
	return ItemRendererSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/PanelSkin.exml'] = window.skins.PanelSkin = (function (_super) {
	__extends(PanelSkin, _super);
	function PanelSkin() {
		_super.call(this);
		this.skinParts = ["titleDisplay","moveArea","closeButton"];
		
		this.minHeight = 230;
		this.minWidth = 450;
		this.elementsContent = [this._Image1_i(),this.moveArea_i(),this.closeButton_i()];
	}
	var _proto = PanelSkin.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.bottom = 0;
		t.left = 0;
		t.right = 0;
		t.scale9Grid = new egret.Rectangle(2,2,12,12);
		t.source = "border_png";
		t.top = 0;
		return t;
	};
	_proto.moveArea_i = function () {
		var t = new eui.Group();
		this.moveArea = t;
		t.height = 45;
		t.left = 0;
		t.right = 0;
		t.top = 0;
		t.elementsContent = [this._Image2_i(),this.titleDisplay_i()];
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.bottom = 0;
		t.left = 0;
		t.right = 0;
		t.source = "header_png";
		t.top = 0;
		return t;
	};
	_proto.titleDisplay_i = function () {
		var t = new eui.Label();
		this.titleDisplay = t;
		t.fontFamily = "Tahoma";
		t.left = 15;
		t.right = 5;
		t.size = 20;
		t.textColor = 0xFFFFFF;
		t.verticalCenter = 0;
		t.wordWrap = false;
		return t;
	};
	_proto.closeButton_i = function () {
		var t = new eui.Button();
		this.closeButton = t;
		t.bottom = 5;
		t.horizontalCenter = 0;
		t.label = "close";
		return t;
	};
	return PanelSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/ProgressBarSkin.exml'] = window.skins.ProgressBarSkin = (function (_super) {
	__extends(ProgressBarSkin, _super);
	function ProgressBarSkin() {
		_super.call(this);
		this.skinParts = ["thumb","labelDisplay"];
		
		this.minHeight = 18;
		this.minWidth = 30;
		this.elementsContent = [this._Image1_i(),this.thumb_i(),this.labelDisplay_i()];
	}
	var _proto = ProgressBarSkin.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.percentHeight = 100;
		t.scale9Grid = new egret.Rectangle(1,1,4,4);
		t.source = "track_pb_png";
		t.verticalCenter = 0;
		t.percentWidth = 100;
		return t;
	};
	_proto.thumb_i = function () {
		var t = new eui.Image();
		this.thumb = t;
		t.percentHeight = 100;
		t.source = "thumb_pb_png";
		t.percentWidth = 100;
		return t;
	};
	_proto.labelDisplay_i = function () {
		var t = new eui.Label();
		this.labelDisplay = t;
		t.fontFamily = "Tahoma";
		t.horizontalCenter = 0;
		t.size = 15;
		t.textAlign = "center";
		t.textColor = 0x707070;
		t.verticalAlign = "middle";
		t.verticalCenter = 0;
		return t;
	};
	return ProgressBarSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/RadioButtonSkin.exml'] = window.skins.RadioButtonSkin = (function (_super) {
	__extends(RadioButtonSkin, _super);
	function RadioButtonSkin() {
		_super.call(this);
		this.skinParts = ["labelDisplay"];
		
		this.elementsContent = [this._Group1_i()];
		this.states = [
			new eui.State ("up",
				[
				])
			,
			new eui.State ("down",
				[
					new eui.SetProperty("_Image1","alpha",0.7)
				])
			,
			new eui.State ("disabled",
				[
					new eui.SetProperty("_Image1","alpha",0.5)
				])
			,
			new eui.State ("upAndSelected",
				[
					new eui.SetProperty("_Image1","source","radiobutton_select_up_png")
				])
			,
			new eui.State ("downAndSelected",
				[
					new eui.SetProperty("_Image1","source","radiobutton_select_down_png")
				])
			,
			new eui.State ("disabledAndSelected",
				[
					new eui.SetProperty("_Image1","source","radiobutton_select_disabled_png")
				])
		];
	}
	var _proto = RadioButtonSkin.prototype;

	_proto._Group1_i = function () {
		var t = new eui.Group();
		t.percentHeight = 100;
		t.percentWidth = 100;
		t.layout = this._HorizontalLayout1_i();
		t.elementsContent = [this._Image1_i(),this.labelDisplay_i()];
		return t;
	};
	_proto._HorizontalLayout1_i = function () {
		var t = new eui.HorizontalLayout();
		t.verticalAlign = "middle";
		return t;
	};
	_proto._Image1_i = function () {
		var t = new eui.Image();
		this._Image1 = t;
		t.alpha = 1;
		t.fillMode = "scale";
		t.source = "radiobutton_unselect_png";
		return t;
	};
	_proto.labelDisplay_i = function () {
		var t = new eui.Label();
		this.labelDisplay = t;
		t.fontFamily = "Tahoma";
		t.size = 20;
		t.textAlign = "center";
		t.textColor = 0x707070;
		t.verticalAlign = "middle";
		return t;
	};
	return RadioButtonSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/ScrollerSkin.exml'] = window.skins.ScrollerSkin = (function (_super) {
	__extends(ScrollerSkin, _super);
	function ScrollerSkin() {
		_super.call(this);
		this.skinParts = ["horizontalScrollBar","verticalScrollBar"];
		
		this.minHeight = 20;
		this.minWidth = 20;
		this.elementsContent = [this.horizontalScrollBar_i(),this.verticalScrollBar_i()];
	}
	var _proto = ScrollerSkin.prototype;

	_proto.horizontalScrollBar_i = function () {
		var t = new eui.HScrollBar();
		this.horizontalScrollBar = t;
		t.bottom = 0;
		t.percentWidth = 100;
		return t;
	};
	_proto.verticalScrollBar_i = function () {
		var t = new eui.VScrollBar();
		this.verticalScrollBar = t;
		t.percentHeight = 100;
		t.right = 0;
		return t;
	};
	return ScrollerSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/TextInputSkin.exml'] = window.skins.TextInputSkin = (function (_super) {
	__extends(TextInputSkin, _super);
	function TextInputSkin() {
		_super.call(this);
		this.skinParts = ["textDisplay","promptDisplay"];
		
		this.minHeight = 40;
		this.minWidth = 300;
		this.elementsContent = [this._Image1_i(),this._Rect1_i(),this.textDisplay_i()];
		this.promptDisplay_i();
		
		this.states = [
			new eui.State ("normal",
				[
				])
			,
			new eui.State ("disabled",
				[
					new eui.SetProperty("textDisplay","textColor",0xff0000)
				])
			,
			new eui.State ("normalWithPrompt",
				[
					new eui.AddItems("promptDisplay","",1,"")
				])
			,
			new eui.State ("disabledWithPrompt",
				[
					new eui.AddItems("promptDisplay","",1,"")
				])
		];
	}
	var _proto = TextInputSkin.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.percentHeight = 100;
		t.scale9Grid = new egret.Rectangle(1,3,8,8);
		t.source = "button_up_png";
		t.percentWidth = 100;
		return t;
	};
	_proto._Rect1_i = function () {
		var t = new eui.Rect();
		t.fillColor = 0xffffff;
		t.percentHeight = 100;
		t.percentWidth = 100;
		return t;
	};
	_proto.textDisplay_i = function () {
		var t = new eui.EditableText();
		this.textDisplay = t;
		t.height = 24;
		t.left = "10";
		t.right = "10";
		t.size = 20;
		t.textColor = 0x000000;
		t.verticalCenter = "0";
		t.percentWidth = 100;
		return t;
	};
	_proto.promptDisplay_i = function () {
		var t = new eui.Label();
		this.promptDisplay = t;
		t.height = 24;
		t.left = 10;
		t.right = 10;
		t.size = 20;
		t.textColor = 0xa9a9a9;
		t.touchEnabled = false;
		t.verticalCenter = 0;
		t.percentWidth = 100;
		return t;
	};
	return TextInputSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/ToggleSwitchSkin.exml'] = window.skins.ToggleSwitchSkin = (function (_super) {
	__extends(ToggleSwitchSkin, _super);
	function ToggleSwitchSkin() {
		_super.call(this);
		this.skinParts = [];
		
		this.elementsContent = [this._Image1_i(),this._Image2_i()];
		this.states = [
			new eui.State ("up",
				[
					new eui.SetProperty("_Image1","source","off_png")
				])
			,
			new eui.State ("down",
				[
					new eui.SetProperty("_Image1","source","off_png")
				])
			,
			new eui.State ("disabled",
				[
					new eui.SetProperty("_Image1","source","off_png")
				])
			,
			new eui.State ("upAndSelected",
				[
					new eui.SetProperty("_Image2","horizontalCenter",18)
				])
			,
			new eui.State ("downAndSelected",
				[
					new eui.SetProperty("_Image2","horizontalCenter",18)
				])
			,
			new eui.State ("disabledAndSelected",
				[
					new eui.SetProperty("_Image2","horizontalCenter",18)
				])
		];
	}
	var _proto = ToggleSwitchSkin.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		this._Image1 = t;
		t.source = "on_png";
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		this._Image2 = t;
		t.horizontalCenter = -18;
		t.source = "handle_png";
		t.verticalCenter = 0;
		return t;
	};
	return ToggleSwitchSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/VScrollBarSkin.exml'] = window.skins.VScrollBarSkin = (function (_super) {
	__extends(VScrollBarSkin, _super);
	function VScrollBarSkin() {
		_super.call(this);
		this.skinParts = ["thumb"];
		
		this.minHeight = 20;
		this.minWidth = 8;
		this.elementsContent = [this.thumb_i()];
	}
	var _proto = VScrollBarSkin.prototype;

	_proto.thumb_i = function () {
		var t = new eui.Image();
		this.thumb = t;
		t.height = 30;
		t.horizontalCenter = 0;
		t.scale9Grid = new egret.Rectangle(3,3,2,2);
		t.source = "roundthumb_png";
		t.width = 8;
		return t;
	};
	return VScrollBarSkin;
})(eui.Skin);generateEUI.paths['resource/eui_skins/VSliderSkin.exml'] = window.skins.VSliderSkin = (function (_super) {
	__extends(VSliderSkin, _super);
	function VSliderSkin() {
		_super.call(this);
		this.skinParts = ["track","thumb"];
		
		this.minHeight = 30;
		this.minWidth = 25;
		this.elementsContent = [this.track_i(),this.thumb_i()];
	}
	var _proto = VSliderSkin.prototype;

	_proto.track_i = function () {
		var t = new eui.Image();
		this.track = t;
		t.percentHeight = 100;
		t.horizontalCenter = 0;
		t.scale9Grid = new egret.Rectangle(1,1,4,4);
		t.source = "track_png";
		t.width = 7;
		return t;
	};
	_proto.thumb_i = function () {
		var t = new eui.Image();
		this.thumb = t;
		t.horizontalCenter = 0;
		t.source = "thumb_png";
		return t;
	};
	return VSliderSkin;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/BottomButtons.exml'] = window.$exmlClass1 = (function (_super) {
	__extends($exmlClass1, _super);
	function $exmlClass1() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 1136;
		this.width = 640;
		this.elementsContent = [this._Image1_i(),this._Image2_i(),this._Image3_i(),this._Image4_i(),this._Image5_i(),this._Image6_i(),this._Image7_i(),this._Label1_i(),this._Image8_i(),this._Image9_i(),this._Label2_i()];
	}
	var _proto = $exmlClass1.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 1136;
		t.name = "xiaoguotu";
		t.scaleX = 1;
		t.scaleY = 1;
		t.source = "resource/assets/Spr/NewVersion12.1/UI/xiaoguotu1.jpg";
		t.visible = false;
		t.width = 640;
		t.x = 0;
		t.y = 0;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 190;
		t.name = "btnC";
		t.scaleX = 0.6;
		t.scaleY = 0.6;
		t.source = "resource/assets/Spr/NewVersion12.1/UI/ (11).png";
		t.width = 190;
		t.x = 262;
		t.y = 1008;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.height = 143;
		t.name = "imgCityHall";
		t.scaleX = 0.6;
		t.scaleY = 0.6;
		t.source = "resource/assets/Spr/NewVersion12.1/UI/ (7).png";
		t.width = 143;
		t.x = 24;
		t.y = 1021.6;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 143;
		t.name = "imgTimeBoost";
		t.scaleX = 0.6;
		t.scaleY = 0.6;
		t.source = "resource/assets/Spr/NewVersion12.1/UI/ (8).png";
		t.width = 143;
		t.x = 150.67;
		t.y = 1020.5;
		return t;
	};
	_proto._Image5_i = function () {
		var t = new eui.Image();
		t.height = 143;
		t.name = "imgBig";
		t.scaleX = 0.6;
		t.scaleY = 0.6;
		t.source = "resource/assets/Spr/NewVersion12.1/UI/ (9).png";
		t.width = 143;
		t.x = 402;
		t.y = 1019.2;
		return t;
	};
	_proto._Image6_i = function () {
		var t = new eui.Image();
		t.height = 143;
		t.name = "imgSettings";
		t.scaleX = 0.6;
		t.scaleY = 0.6;
		t.source = "resource/assets/Spr/NewVersion12.1/UI/ (10).png";
		t.width = 143;
		t.x = 529.25;
		t.y = 1020;
		return t;
	};
	_proto._Image7_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 24;
		t.name = "imgDoubleTimeLeftBg";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.visible = false;
		t.width = 69;
		t.x = 166.42;
		t.y = 1081;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 23;
		t.name = "txtDoubleTimeLeft";
		t.size = 22;
		t.text = "9分钟";
		t.textAlign = "center";
		t.verticalAlign = "middle";
		t.visible = false;
		t.width = 68.5;
		t.x = 159.32;
		t.y = 1108;
		return t;
	};
	_proto._Image8_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 54;
		t.name = "imgPigGreenNumBg";
		t.scaleX = 0.6;
		t.scaleY = 0.6;
		t.source = "resource/assets/Spr/NewVersion12.1/UI/ (1).png";
		t.visible = true;
		t.width = 130;
		t.x = 412.9;
		t.y = 1099;
		return t;
	};
	_proto._Image9_i = function () {
		var t = new eui.Image();
		t.height = 20;
		t.name = "NumBubble";
		t.width = 20;
		t.x = 356;
		t.y = 1020;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.bold = true;
		t.height = 24;
		t.name = "txtPigGreenNum";
		t.size = 22;
		t.text = "3";
		t.textAlign = "center";
		t.textColor = 0xffffff;
		t.verticalAlign = "middle";
		t.width = 78;
		t.x = 411.88;
		t.y = 1106;
		return t;
	};
	return $exmlClass1;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CBank.exml'] = window.$exmlClass2 = (function (_super) {
	__extends($exmlClass2, _super);
	function $exmlClass2() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 1136;
		this.width = 640;
		this.elementsContent = [this._Image1_i(),this._Image2_i(),this._Label1_i(),this._Label2_i(),this._Label3_i(),this._Label4_i(),this._Label5_i(),this._Label6_i(),this._Image3_i(),this._Label7_i(),this._Image4_i(),this._Image5_i(),this._Image6_i(),this._Image7_i()];
	}
	var _proto = $exmlClass2.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 848;
		t.name = "img0";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 600;
		t.x = 20;
		t.y = 167;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 108;
		t.name = "img1";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 566;
		t.x = 38;
		t.y = 241.3;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.bold = true;
		t.height = 56.67;
		t.name = "txt0";
		t.text = "多可粒世界第一家银行";
		t.textColor = 0x000000;
		t.verticalAlign = "middle";
		t.width = 564.67;
		t.x = 39;
		t.y = 184.97;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 34;
		t.name = "txt1";
		t.size = 25;
		t.text = "菁蓉镇 支行";
		t.textColor = 0x000000;
		t.verticalAlign = "middle";
		t.width = 279.33;
		t.x = 48;
		t.y = 248;
		return t;
	};
	_proto._Label3_i = function () {
		var t = new eui.Label();
		t.name = "txt2";
		t.size = 16;
		t.text = "等级 1：";
		t.textColor = 0x000000;
		t.x = 48;
		t.y = 291.98;
		return t;
	};
	_proto._Label4_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.name = "txt3";
		t.size = 16;
		t.text = "离开时自动产出，10% 直到 1小时 45分钟";
		t.textColor = 0x676767;
		t.width = 422;
		t.x = 104.08;
		t.y = 291.98;
		return t;
	};
	_proto._Label5_i = function () {
		var t = new eui.Label();
		t.name = "txt4";
		t.size = 16;
		t.text = "支行收益：";
		t.textColor = 0x000000;
		t.x = 48;
		t.y = 321.65;
		return t;
	};
	_proto._Label6_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 21.33;
		t.name = "txt5";
		t.size = 16;
		t.text = "1/秒";
		t.textColor = 0x676767;
		t.width = 112.67;
		t.x = 152.02;
		t.y = 321.65;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.height = 92;
		t.name = "img2";
		t.scaleX = 0.3;
		t.scaleY = 0.3;
		t.source = "resource/assets/Spr/JinBi/XiaoJinBi/jinbi_0.png";
		t.width = 73;
		t.x = 124.38;
		t.y = 313.32;
		return t;
	};
	_proto._Label7_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.name = "txt6";
		t.size = 22;
		t.text = "区域利润";
		t.textColor = 0x918c8c;
		t.width = 278;
		t.x = 39;
		t.y = 358.02;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 594;
		t.name = "img3";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 562;
		t.x = 38;
		t.y = 400;
		return t;
	};
	_proto._Image5_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 18;
		t.name = "img4";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 562;
		t.x = 38;
		t.y = 383;
		return t;
	};
	_proto._Image6_i = function () {
		var t = new eui.Image();
		t.height = 96;
		t.name = "img5";
		t.width = 420;
		t.x = 124;
		t.y = 1030;
		return t;
	};
	_proto._Image7_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 75;
		t.name = "img6";
		t.source = "resource/assets/Spr/1x1/1x1_1.png";
		t.width = 128;
		t.x = 464;
		t.y = 257;
		return t;
	};
	return $exmlClass2;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CBankBack.exml'] = window.$exmlClass3 = (function (_super) {
	__extends($exmlClass3, _super);
	function $exmlClass3() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 1136;
		this.width = 640;
		this.elementsContent = [this._Image1_i(),this._Label1_i(),this._Label2_i(),this._Image2_i(),this._Label3_i(),this._Label4_i(),this._Image3_i(),this._Image4_i()];
	}
	var _proto = $exmlClass3.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 413.94;
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 490.3;
		t.x = 70.79;
		t.y = 331;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 46;
		t.text = "欢迎回来";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.verticalAlign = "middle";
		t.width = 544;
		t.x = 50;
		t.y = 360;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 40;
		t.size = 25;
		t.text = "你不在的时候，你的城市获得了";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.verticalAlign = "middle";
		t.width = 548;
		t.x = 48;
		t.y = 410;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 45;
		t.source = "resource/assets/Spr/JinBi/XiaoJinBi/jinbi_0.png";
		t.width = 37;
		t.x = 232;
		t.y = 473;
		return t;
	};
	_proto._Label3_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.name = "txt0";
		t.scaleY = 1;
		t.size = 40;
		t.text = "0";
		t.textAlign = "left";
		t.textColor = 0x000000;
		t.verticalAlign = "middle";
		t.width = 275;
		t.x = 279;
		t.y = 478;
		return t;
	};
	_proto._Label4_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 20;
		t.size = 20;
		t.text = "升级银行可获得更多睡后收益！";
		t.textAlign = "center";
		t.textColor = 0x6d6b6b;
		t.verticalAlign = "middle";
		t.width = 548;
		t.x = 47;
		t.y = 545;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 64;
		t.name = "img0";
		t.scaleX = 0.8;
		t.scaleY = 0.8;
		t.width = 230;
		t.x = 108;
		t.y = 656;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 64;
		t.name = "img1";
		t.scaleX = 0.8;
		t.scaleY = 0.8;
		t.width = 230;
		t.x = 338;
		t.y = 656;
		return t;
	};
	return $exmlClass3;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CBoost.exml'] = window.$exmlClass4 = (function (_super) {
	__extends($exmlClass4, _super);
	function $exmlClass4() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 1136;
		this.width = 640;
		this.elementsContent = [this._Image1_i(),this._Image2_i(),this._Image3_i(),this._Image4_i(),this._Label1_i(),this._Image5_i(),this._Label2_i(),this._Label3_i(),this._Image6_i(),this._Label4_i(),this._Label5_i(),this._Image7_i(),this._Label6_i(),this._Label7_i(),this._Image8_i(),this._Label8_i(),this._Label9_i(),this._Label10_i(),this._Label11_i(),this._Image9_i(),this._Label12_i(),this._Label13_i(),this._Image10_i(),this._Label14_i(),this._Image11_i(),this._Label15_i(),this._Image12_i(),this._Label16_i(),this._Label17_i(),this._Image13_i()];
	}
	var _proto = $exmlClass4.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 600;
		t.name = "img0";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 604;
		t.x = 18;
		t.y = 376;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 570;
		t.name = "img1";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 577.33;
		t.x = 31.67;
		t.y = 387.18;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 49.69;
		t.name = "img2";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 576;
		t.x = 32;
		t.y = 391.18;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 96;
		t.name = "btnClose";
		t.width = 420;
		t.x = 110;
		t.y = 1038;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.size = 26;
		t.text = "加速";
		t.textAlign = "center";
		t.width = 595.21;
		t.x = 23.92;
		t.y = 400.49;
		return t;
	};
	_proto._Image5_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 128;
		t.name = "btn0";
		t.scaleX = 0.7;
		t.scaleY = 0.7;
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 128;
		t.x = 65.2;
		t.y = 470;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.name = "txtTitle0";
		t.size = 25;
		t.text = "双倍时间";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.verticalAlign = "middle";
		t.x = 338;
		t.y = 470;
		return t;
	};
	_proto._Label3_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 80;
		t.size = 20;
		t.text = "状态持续10分钟，期间双倍金币收益\n（耗费：看广告）";
		t.textAlign = "center";
		t.textColor = 0x676767;
		t.verticalAlign = "middle";
		t.width = 358;
		t.x = 201.6;
		t.y = 488;
		return t;
	};
	_proto._Image6_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 128;
		t.name = "btn1";
		t.scaleX = 0.7;
		t.scaleY = 0.7;
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 128;
		t.x = 65.2;
		t.y = 597;
		return t;
	};
	_proto._Label4_i = function () {
		var t = new eui.Label();
		t.name = "txtTitle0";
		t.size = 25;
		t.text = "双倍时间";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.verticalAlign = "middle";
		t.x = 337.73;
		t.y = 603.5;
		return t;
	};
	_proto._Label5_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 80;
		t.size = 20;
		t.text = "状态永久持续，期间双倍金币收益\n（耗费：30元人民币）";
		t.textAlign = "center";
		t.textColor = 0x676767;
		t.verticalAlign = "middle";
		t.width = 392;
		t.x = 182.6;
		t.y = 616;
		return t;
	};
	_proto._Image7_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 128;
		t.name = "btn2";
		t.scaleX = 0.7;
		t.scaleY = 0.7;
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 128;
		t.x = 66;
		t.y = 723;
		return t;
	};
	_proto._Label6_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 27;
		t.name = "txtTitle0";
		t.size = 25;
		t.text = "快进";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.verticalAlign = "middle";
		t.width = 118;
		t.x = 324.73;
		t.y = 720.5;
		return t;
	};
	_proto._Label7_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 26;
		t.size = 20;
		t.text = "直接获得当前金币产出率下10分钟的金币收益：";
		t.textAlign = "center";
		t.textColor = 0x676767;
		t.verticalAlign = "middle";
		t.width = 422;
		t.x = 173.6;
		t.y = 756.8;
		return t;
	};
	_proto._Image8_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 128;
		t.scaleX = 0.7;
		t.scaleY = 0.7;
		t.source = "resource/assets/Spr/1x1/1x1_2.png";
		t.width = 128;
		t.x = 66;
		t.y = 847.5;
		return t;
	};
	_proto._Label8_i = function () {
		var t = new eui.Label();
		t.name = "txtTitle0";
		t.size = 25;
		t.text = "快进";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.verticalAlign = "middle";
		t.x = 360.73;
		t.y = 843.02;
		return t;
	};
	_proto._Label9_i = function () {
		var t = new eui.Label();
		t.size = 20;
		t.text = "获得：";
		t.textColor = 0x676767;
		t.x = 182;
		t.y = 788;
		return t;
	};
	_proto._Label10_i = function () {
		var t = new eui.Label();
		t.size = 20;
		t.text = "获得：";
		t.textColor = 0x676767;
		t.x = 182.6;
		t.y = 914.61;
		return t;
	};
	_proto._Label11_i = function () {
		var t = new eui.Label();
		t.size = 20;
		t.text = "耗费：";
		t.textColor = 0x676767;
		t.x = 400;
		t.y = 788;
		return t;
	};
	_proto._Image9_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 32;
		t.source = "resource/assets/Spr/JinBi/XiaoJinBi/jinbi_0.png";
		t.width = 26;
		t.x = 234;
		t.y = 780;
		return t;
	};
	_proto._Label12_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 26;
		t.name = "txtFastForward0_Gain";
		t.size = 24;
		t.text = "0";
		t.textAlign = "left";
		t.textColor = 0x676767;
		t.verticalAlign = "middle";
		t.width = 129;
		t.x = 268;
		t.y = 786;
		return t;
	};
	_proto._Label13_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 26;
		t.name = "txtFastForward0_Cost";
		t.size = 24;
		t.text = "0";
		t.textAlign = "left";
		t.textColor = 0x676767;
		t.verticalAlign = "middle";
		t.width = 129;
		t.x = 494;
		t.y = 786;
		return t;
	};
	_proto._Image10_i = function () {
		var t = new eui.Image();
		t.height = 32;
		t.source = "resource/assets/Spr/UI/zuanshi.png";
		t.width = 32;
		t.x = 454;
		t.y = 782;
		return t;
	};
	_proto._Label14_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 26;
		t.size = 20;
		t.text = "直接获得当前金币产出率下10分钟的金币收益：";
		t.textAlign = "center";
		t.textColor = 0x676767;
		t.verticalAlign = "middle";
		t.width = 422;
		t.x = 172.26;
		t.y = 879.3;
		return t;
	};
	_proto._Image11_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 128;
		t.name = "btn3";
		t.scaleX = 0.7;
		t.scaleY = 0.7;
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 128;
		t.x = 66;
		t.y = 847.5;
		return t;
	};
	_proto._Label15_i = function () {
		var t = new eui.Label();
		t.size = 20;
		t.text = "耗费：";
		t.textColor = 0x676767;
		t.x = 400;
		t.y = 914.61;
		return t;
	};
	_proto._Image12_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 32;
		t.source = "resource/assets/Spr/JinBi/XiaoJinBi/jinbi_0.png";
		t.width = 26;
		t.x = 234;
		t.y = 905.95;
		return t;
	};
	_proto._Label16_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 26;
		t.name = "txtFastForward1_Gain";
		t.size = 24;
		t.text = "0";
		t.textAlign = "left";
		t.textColor = 0x676767;
		t.verticalAlign = "middle";
		t.width = 129;
		t.x = 268;
		t.y = 911.95;
		return t;
	};
	_proto._Label17_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 26;
		t.name = "txtFastForward1_Cost";
		t.size = 24;
		t.text = "0";
		t.textAlign = "left";
		t.textColor = 0x676767;
		t.verticalAlign = "middle";
		t.width = 129;
		t.x = 491.11;
		t.y = 910.96;
		return t;
	};
	_proto._Image13_i = function () {
		var t = new eui.Image();
		t.height = 32;
		t.source = "resource/assets/Spr/UI/zuanshi.png";
		t.width = 32;
		t.x = 452.67;
		t.y = 907.96;
		return t;
	};
	return $exmlClass4;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CBulldozeLotConfirm.exml'] = window.$exmlClass5 = (function (_super) {
	__extends($exmlClass5, _super);
	function $exmlClass5() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 1136;
		this.width = 640;
		this.elementsContent = [this._Image1_i(),this._Image2_i(),this._Label1_i(),this._Label2_i(),this._Image3_i(),this._Image4_i()];
	}
	var _proto = $exmlClass5.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 1136;
		t.name = "img0";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 640;
		t.x = 0;
		t.y = 0;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 435.15;
		t.name = "img1";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 538.18;
		t.x = 50.91;
		t.y = 350.42;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 69.39;
		t.name = "txt0";
		t.text = "推平这块地？";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.verticalAlign = "middle";
		t.width = 538;
		t.x = 51.09;
		t.y = 362.54;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 190;
		t.name = "txt1";
		t.text = "    推平之后，这块地包括等级在内的一切属性归零，需重新购买和建造。请谨慎操作！";
		t.textAlign = "left";
		t.textColor = 0x000000;
		t.verticalAlign = "middle";
		t.width = 496;
		t.x = 85.47;
		t.y = 463.03;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 64;
		t.name = "img2";
		t.scaleX = 0.95;
		t.scaleY = 0.95;
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 230;
		t.x = 85.47;
		t.y = 692.42;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.height = 64;
		t.name = "img3";
		t.scaleX = 0.95;
		t.scaleY = 0.95;
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 230;
		t.x = 341.47;
		t.y = 692.42;
		return t;
	};
	return $exmlClass5;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CButtomTimeBooste.exml'] = window.$exmlClass6 = (function (_super) {
	__extends($exmlClass6, _super);
	function $exmlClass6() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 98;
		this.width = 98;
		this.elementsContent = [this._Image1_i()];
	}
	var _proto = $exmlClass6.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 98;
		t.source = "resource/assets/Spr/NewVersion12.1/UI/ (8).png";
		t.width = 98;
		t.x = 0;
		t.y = 0;
		return t;
	};
	return $exmlClass6;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CButtonCityHall.exml'] = window.$exmlClass7 = (function (_super) {
	__extends($exmlClass7, _super);
	function $exmlClass7() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 98;
		this.width = 98;
		this.elementsContent = [this._Image1_i()];
	}
	var _proto = $exmlClass7.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 98;
		t.scaleX = 1;
		t.scaleY = 1;
		t.source = "resource/assets/Spr/NewVersion12.1/UI/ (7).png";
		t.width = 98;
		t.x = 0;
		t.y = 0;
		return t;
	};
	return $exmlClass7;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CButtonPig.exml'] = window.$exmlClass8 = (function (_super) {
	__extends($exmlClass8, _super);
	function $exmlClass8() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 98;
		this.width = 98;
		this.elementsContent = [this._Image1_i()];
	}
	var _proto = $exmlClass8.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 98;
		t.name = "img0";
		t.source = "resource/assets/Spr/NewVersion12.1/UI/ (9).png";
		t.width = 98;
		t.x = 0;
		t.y = 0;
		return t;
	};
	return $exmlClass8;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CButtonSettings.exml'] = window.$exmlClass9 = (function (_super) {
	__extends($exmlClass9, _super);
	function $exmlClass9() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 98;
		this.width = 98;
		this.elementsContent = [this._Image1_i()];
	}
	var _proto = $exmlClass9.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 98;
		t.source = "resource/assets/Spr/NewVersion12.1/UI/ (10).png";
		t.width = 98;
		t.x = 0;
		t.y = 0;
		return t;
	};
	return $exmlClass9;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CBuyLotPanel.exml'] = window.$exmlClass10 = (function (_super) {
	__extends($exmlClass10, _super);
	function $exmlClass10() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 1136;
		this.width = 640;
		this.elementsContent = [this._Image1_i(),this._Image2_i(),this._Image3_i(),this._Image4_i(),this._Label1_i(),this._Label2_i(),this._Label3_i(),this._Image5_i(),this._Label4_i(),this._Image6_i(),this._Image7_i(),this._Image8_i(),this._Label5_i(),this._Image9_i(),this._Label6_i(),this._Image10_i()];
	}
	var _proto = $exmlClass10.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 600;
		t.name = "img0";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 604;
		t.x = 18;
		t.y = 376;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 570;
		t.name = "img1";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 577.33;
		t.x = 31.67;
		t.y = 391.18;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 96;
		t.name = "btnClose";
		t.width = 420;
		t.x = 110;
		t.y = 1038;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 50;
		t.name = "img2";
		t.source = "resource/assets/Spr/1x1/1x1_1.png";
		t.width = 576;
		t.x = 32;
		t.y = 391.18;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.size = 26;
		t.text = "小型空地";
		t.textAlign = "center";
		t.width = 595.21;
		t.x = 23.92;
		t.y = 400.49;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 40;
		t.size = 22;
		t.text = "买下并建设这块地，让你的城市成长！";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.width = 590;
		t.x = 32;
		t.y = 450.18;
		return t;
	};
	_proto._Label3_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 23;
		t.size = 22;
		t.text = "买这块地";
		t.textAlign = "center";
		t.textColor = 0x555555;
		t.width = 598;
		t.x = 21.13;
		t.y = 498.36;
		return t;
	};
	_proto._Image5_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 92;
		t.scaleX = 0.4;
		t.scaleY = 0.4;
		t.source = "resource/assets/Spr/JinBi/XiaoJinBi/jinbi_0.png";
		t.width = 73;
		t.x = 263.13;
		t.y = 528;
		return t;
	};
	_proto._Label4_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.name = "txtBuyOneCost";
		t.size = 26;
		t.text = "0";
		t.textColor = 0x000000;
		t.width = 296;
		t.x = 298;
		t.y = 536.66;
		return t;
	};
	_proto._Image6_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 100;
		t.name = "btnBuyResidentialLot";
		t.scaleX = 1.1;
		t.scaleY = 1.1;
		t.source = "resource/assets/Spr/1x1/1x1_2.png";
		t.width = 160;
		t.x = 42;
		t.y = 586;
		return t;
	};
	_proto._Image7_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 100;
		t.name = "btnBuyBusinessLot";
		t.pixelHitTest = false;
		t.scaleX = 1.1;
		t.scaleY = 1.1;
		t.source = "resource/assets/Spr/1x1/1x1_3.png";
		t.width = 160;
		t.x = 232;
		t.y = 586;
		return t;
	};
	_proto._Image8_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 100;
		t.name = "btnBuyServiceLot";
		t.scaleX = 1.1;
		t.scaleY = 1.1;
		t.source = "resource/assets/Spr/1x1/1x1_4.png";
		t.width = 160;
		t.x = 420;
		t.y = 586;
		return t;
	};
	_proto._Label5_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 23;
		t.size = 22;
		t.text = "批量买同规格多块地";
		t.textAlign = "center";
		t.textColor = 0x555555;
		t.width = 603;
		t.x = 20;
		t.y = 768.6;
		return t;
	};
	_proto._Image9_i = function () {
		var t = new eui.Image();
		t.height = 92;
		t.scaleX = 0.4;
		t.scaleY = 0.4;
		t.source = "resource/assets/Spr/JinBi/XiaoJinBi/jinbi_0.png";
		t.width = 73;
		t.x = 257.46;
		t.y = 796.28;
		return t;
	};
	_proto._Label6_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 39.09;
		t.name = "txtBulkBuyCost";
		t.size = 26;
		t.text = "----";
		t.textAlign = "left";
		t.textColor = 0x000000;
		t.width = 133.61;
		t.x = 292.33;
		t.y = 804.37;
		return t;
	};
	_proto._Image10_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 90;
		t.name = "btnBulkBuy";
		t.scaleX = 0.85;
		t.scaleY = 0.85;
		t.width = 400;
		t.x = 145;
		t.y = 842.68;
		return t;
	};
	return $exmlClass10;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CCityHall.exml'] = window.$exmlClass11 = (function (_super) {
	__extends($exmlClass11, _super);
	function $exmlClass11() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 1136;
		this.width = 640;
		this.elementsContent = [this._Image1_i(),this._Image2_i(),this._Image3_i(),this._Image4_i(),this._Image5_i(),this._Image6_i(),this._Image7_i(),this._Image8_i(),this._Image9_i()];
	}
	var _proto = $exmlClass11.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 848;
		t.name = "img0";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 600;
		t.x = 20;
		t.y = 167;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 760;
		t.name = "img1";
		t.width = 560;
		t.x = 39;
		t.y = 238.4;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 57.88;
		t.name = "img2";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 191.21;
		t.x = 20;
		t.y = 110;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 57.88;
		t.name = "img3";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 191.21;
		t.x = 428.79;
		t.y = 110;
		return t;
	};
	_proto._Image5_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 29.88;
		t.name = "img4";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 219.21;
		t.x = 211.21;
		t.y = 144.09;
		return t;
	};
	_proto._Image6_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 60;
		t.name = "img5";
		t.source = "resource/assets/Spr/1x1/1x1_1.png";
		t.width = 240;
		t.x = 358;
		t.y = 170.97;
		return t;
	};
	_proto._Image7_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 60;
		t.name = "img6";
		t.source = "resource/assets/Spr/1x1/1x1_1.png";
		t.width = 240;
		t.x = 39.67;
		t.y = 170.4;
		return t;
	};
	_proto._Image8_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 9.33;
		t.name = "img7";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 559;
		t.x = 39;
		t.y = 230;
		return t;
	};
	_proto._Image9_i = function () {
		var t = new eui.Image();
		t.height = 96;
		t.name = "img8";
		t.width = 420;
		t.x = 124;
		t.y = 1030.68;
		return t;
	};
	return $exmlClass11;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CCityHallItem.exml'] = window.$exmlClass12 = (function (_super) {
	__extends($exmlClass12, _super);
	function $exmlClass12() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 110;
		this.width = 576;
		this.elementsContent = [this._Image1_i(),this._Image2_i(),this._Image3_i(),this._Image4_i(),this._Label1_i(),this._Label2_i(),this._Label3_i(),this._Image5_i(),this._Image6_i()];
	}
	var _proto = $exmlClass12.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 110;
		t.name = "img0";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 576;
		t.x = 0;
		t.y = 0;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 80;
		t.name = "img1";
		t.width = 160;
		t.x = 394;
		t.y = 15;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 70;
		t.name = "img2";
		t.width = 70;
		t.x = 16;
		t.y = 8;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 13;
		t.name = "img3";
		t.width = 70;
		t.x = 16;
		t.y = 86;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.name = "label0";
		t.size = 25;
		t.text = "提高建筑金币产出";
		t.width = 288;
		t.x = 95.5;
		t.y = 16;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 21;
		t.name = "label1";
		t.size = 20;
		t.text = "银行持续产出时间提升";
		t.textColor = 0xffffff;
		t.width = 202;
		t.x = 95;
		t.y = 55.5;
		return t;
	};
	_proto._Label3_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.name = "label2";
		t.size = 20;
		t.text = "10%";
		t.width = 85.5;
		t.x = 327.5;
		t.y = 56;
		return t;
	};
	_proto._Image5_i = function () {
		var t = new eui.Image();
		t.height = 20;
		t.name = "img4";
		t.width = 20;
		t.x = 300.5;
		t.y = 52;
		return t;
	};
	_proto._Image6_i = function () {
		var t = new eui.Image();
		t.alpha = 0.4;
		t.height = 110;
		t.name = "img5";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 576;
		t.x = 0;
		t.y = 1;
		return t;
	};
	return $exmlClass12;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CCosmosButton1.exml'] = window.$exmlClass13 = (function (_super) {
	__extends($exmlClass13, _super);
	function $exmlClass13() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 90;
		this.width = 400;
		this.elementsContent = [this._Image1_i(),this._Image2_i(),this._Image3_i(),this._Image4_i(),this._Label1_i()];
	}
	var _proto = $exmlClass13.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 90;
		t.name = "img0";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 400;
		t.x = 0;
		t.y = 0;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 57.33;
		t.name = "img2";
		t.source = "resource/assets/Spr/1x1/1x1_1.png";
		t.width = 368;
		t.x = 17;
		t.y = 11.34;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 8;
		t.name = "img3";
		t.source = "resource/assets/Spr/1x1/1x1_2.png";
		t.width = 368;
		t.x = 17;
		t.y = 11;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 8;
		t.name = "img1";
		t.width = 401;
		t.x = 0;
		t.y = 82;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 36;
		t.name = "txt0";
		t.size = 25;
		t.text = "关闭";
		t.textAlign = "center";
		t.verticalAlign = "middle";
		t.width = 362;
		t.x = 20;
		t.y = 24;
		return t;
	};
	return $exmlClass13;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CCosmosButton2.exml'] = window.$exmlClass14 = (function (_super) {
	__extends($exmlClass14, _super);
	function $exmlClass14() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 105;
		this.width = 160;
		this.elementsContent = [this._Image1_i(),this._Image2_i(),this._Image3_i(),this._Image4_i(),this._Label1_i(),this._Label2_i(),this._Label3_i(),this._Image5_i(),this._Image6_i(),this._Label4_i()];
	}
	var _proto = $exmlClass14.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.anchorOffsetY = 0;
		t.height = 105;
		t.name = "img0";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 160;
		t.x = 0;
		t.y = 0;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 64;
		t.name = "img1";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 132;
		t.x = 15;
		t.y = 12;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 55.6;
		t.name = "img2";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 124;
		t.x = 19;
		t.y = 16;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.height = 5;
		t.name = "img3";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 160;
		t.x = 0;
		t.y = 100;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 22.8;
		t.multiline = true;
		t.name = "txt0";
		t.size = 20;
		t.text = "住宅";
		t.textAlign = "center";
		t.textColor = 0x0f0808;
		t.verticalAlign = "middle";
		t.width = 131.2;
		t.x = 15;
		t.y = 21.4;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.name = "txt1";
		t.size = 16;
		t.text = "占本市土地10%";
		t.textAlign = "center";
		t.textColor = 0x2d2828;
		t.width = 123.8;
		t.x = 19.4;
		t.y = 50;
		return t;
	};
	_proto._Label3_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 13.2;
		t.name = "txt2";
		t.size = 12;
		t.text = "正常需求";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.width = 132;
		t.x = 15.4;
		t.y = 80.8;
		return t;
	};
	_proto._Image5_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 50;
		t.name = "img4";
		t.width = 50;
		t.x = 57;
		t.y = 18;
		return t;
	};
	_proto._Image6_i = function () {
		var t = new eui.Image();
		t.height = 20;
		t.name = "img5";
		t.visible = true;
		t.width = 20;
		t.x = 57;
		t.y = 78;
		return t;
	};
	_proto._Label4_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 20;
		t.name = "txt3";
		t.size = 15;
		t.textAlign = "left";
		t.textColor = 0x000000;
		t.verticalAlign = "middle";
		t.visible = true;
		t.width = 49;
		t.x = 83.5;
		t.y = 79.4;
		return t;
	};
	return $exmlClass14;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CCosmosButton3.exml'] = window.$exmlClass15 = (function (_super) {
	__extends($exmlClass15, _super);
	function $exmlClass15() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 128;
		this.width = 128;
		this.elementsContent = [this._Image1_i(),this._Image2_i(),this._Label1_i()];
	}
	var _proto = $exmlClass15.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 128;
		t.name = "img0";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 128;
		t.x = 0;
		t.y = 0;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 102.4;
		t.name = "img1";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 100.2;
		t.x = 13.8;
		t.y = 13.6;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.bold = true;
		t.height = 101.6;
		t.name = "txt0";
		t.scaleY = 0.9;
		t.size = 64;
		t.text = "2X";
		t.textAlign = "center";
		t.textColor = 0xffffff;
		t.verticalAlign = "middle";
		t.width = 82.4;
		t.x = 22.8;
		t.y = 18.28;
		return t;
	};
	return $exmlClass15;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CCosmosButton4.exml'] = window.$exmlClass16 = (function (_super) {
	__extends($exmlClass16, _super);
	function $exmlClass16() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 80;
		this.width = 160;
		this.elementsContent = [this._Image1_i(),this._Image2_i(),this._Label1_i(),this._Label2_i(),this._Image3_i(),this._Label3_i()];
	}
	var _proto = $exmlClass16.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.anchorOffsetY = 0;
		t.height = 80;
		t.name = "img0";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 160;
		t.x = 0;
		t.y = 0;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.height = 10;
		t.name = "img1";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 160;
		t.x = 0;
		t.y = 70;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 22.8;
		t.multiline = true;
		t.name = "txt0";
		t.size = 20;
		t.textAlign = "right";
		t.textColor = 0xffffff;
		t.verticalAlign = "top";
		t.width = 62.2;
		t.x = 15;
		t.y = 10;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 17;
		t.name = "txt1";
		t.size = 20;
		t.textAlign = "left";
		t.textColor = 0xffffff;
		t.width = 70.8;
		t.x = 85.5;
		t.y = 10;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.height = 20;
		t.name = "img2";
		t.scaleX = 1;
		t.scaleY = 1.2;
		t.source = "resource/assets/Spr/JinBi/XiaoJinBi/jinbi_0.png";
		t.visible = true;
		t.width = 20;
		t.x = 40;
		t.y = 35.5;
		return t;
	};
	_proto._Label3_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 21;
		t.name = "txt2";
		t.size = 20;
		t.textAlign = "left";
		t.textColor = 0xffffff;
		t.verticalAlign = "middle";
		t.visible = true;
		t.width = 158;
		t.x = 65;
		t.y = 37.5;
		return t;
	};
	return $exmlClass16;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CCosmosButton5.exml'] = window.$exmlClass17 = (function (_super) {
	__extends($exmlClass17, _super);
	function $exmlClass17() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 60;
		this.width = 240;
		this.elementsContent = [this._Image1_i(),this._Label1_i(),this._Image2_i(),this._Label2_i()];
	}
	var _proto = $exmlClass17.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 60;
		t.name = "img0";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 240;
		t.x = 0;
		t.y = 0;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 33;
		t.name = "txt0";
		t.size = 25;
		t.text = "城市";
		t.textAlign = "center";
		t.textColor = 0xffffff;
		t.verticalAlign = "middle";
		t.width = 239;
		t.x = 1;
		t.y = 14;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 32;
		t.name = "img1";
		t.source = "resource/assets/Spr/UI/zuanshi.png";
		t.visible = true;
		t.width = 32;
		t.x = 46;
		t.y = 14;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.height = 25;
		t.name = "txt1";
		t.size = 25;
		t.strokeColor = 0x000000;
		t.text = "游戏(14)";
		t.textAlign = "left";
		t.textColor = 0xffffff;
		t.verticalAlign = "middle";
		t.width = 157;
		t.x = 82;
		t.y = 18;
		return t;
	};
	return $exmlClass17;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CCosmosButton6.exml'] = window.$exmlClass18 = (function (_super) {
	__extends($exmlClass18, _super);
	function $exmlClass18() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 75;
		this.width = 128;
		this.elementsContent = [this._Image1_i(),this._Image2_i(),this._Image3_i(),this._Image4_i(),this._Image5_i(),this._Label1_i(),this._Label2_i()];
	}
	var _proto = $exmlClass18.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 70;
		t.name = "img0";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 128;
		t.x = 0;
		t.y = 0;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.height = 5;
		t.name = "img1";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 128;
		t.x = 0;
		t.y = 70;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 40.4;
		t.name = "img2";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 113.2;
		t.x = 8.4;
		t.y = 6.2;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 32.8;
		t.name = "img3";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 104.8;
		t.x = 12.8;
		t.y = 10;
		return t;
	};
	_proto._Image5_i = function () {
		var t = new eui.Image();
		t.height = 15;
		t.name = "img4";
		t.source = "resource/assets/Spr/JinBi/XiaoJinBi/jinbi_0.png";
		t.width = 15;
		t.x = 27;
		t.y = 49.8;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 12.8;
		t.name = "txt1";
		t.size = 16;
		t.text = "32767";
		t.textAlign = "left";
		t.textColor = 0x212020;
		t.verticalAlign = "middle";
		t.width = 77.2;
		t.x = 44.8;
		t.y = 50.6;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.name = "txt0";
		t.size = 20;
		t.text = "升级";
		t.textAlign = "center";
		t.textColor = 0xef8721;
		t.verticalAlign = "middle";
		t.x = 43.6;
		t.y = 16;
		return t;
	};
	return $exmlClass18;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CCosmosButton7.exml'] = window.$exmlClass19 = (function (_super) {
	__extends($exmlClass19, _super);
	function $exmlClass19() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 80;
		this.width = 64;
		this.elementsContent = [this._Image1_i(),this._Image2_i(),this._Image3_i(),this._Label1_i(),this._Image4_i()];
	}
	var _proto = $exmlClass19.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 80;
		t.name = "img0";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 64;
		t.x = 0;
		t.y = 0;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 42;
		t.name = "img1";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 49;
		t.x = 8;
		t.y = 6.4;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 12;
		t.name = "img2";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 49;
		t.x = 7.6;
		t.y = 53.2;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 46.8;
		t.name = "txt0";
		t.text = "1";
		t.textAlign = "center";
		t.verticalAlign = "middle";
		t.width = 48;
		t.x = 8;
		t.y = 5;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.anchorOffsetY = 0;
		t.height = 6.4;
		t.name = "img3";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 64;
		t.x = 0;
		t.y = 74.8;
		return t;
	};
	return $exmlClass19;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CCosmosButton8.exml'] = window.$exmlClass20 = (function (_super) {
	__extends($exmlClass20, _super);
	function $exmlClass20() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 116;
		this.width = 116;
		this.elementsContent = [this._Image1_i(),this._Image2_i()];
	}
	var _proto = $exmlClass20.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 116;
		t.name = "img0";
		t.source = "resource/assets/Spr/UI/next_city.png";
		t.width = 116;
		t.x = 0;
		t.y = 0;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.height = 80;
		t.name = "img1";
		t.scaleX = 0.6;
		t.scaleY = 0.6;
		t.source = "resource/assets/Spr/UI/1.png";
		t.visible = false;
		t.width = 59;
		t.x = 25;
		t.y = 22;
		return t;
	};
	return $exmlClass20;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CCosmosSimpleButton.exml'] = window.$exmlClass21 = (function (_super) {
	__extends($exmlClass21, _super);
	function $exmlClass21() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 64;
		this.width = 230;
		this.elementsContent = [this._Image1_i(),this._Label1_i()];
	}
	var _proto = $exmlClass21.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 64;
		t.name = "img0";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 230;
		t.x = 0;
		t.y = 0;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 62;
		t.name = "txt0";
		t.text = "推倒";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.verticalAlign = "middle";
		t.width = 228;
		t.x = 1;
		t.y = 1;
		return t;
	};
	return $exmlClass21;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CDeveloperEditor.exml'] = window.$exmlClass22 = (function (_super) {
	__extends($exmlClass22, _super);
	function $exmlClass22() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 1136;
		this.width = 640;
		this.elementsContent = [this._List1_i(),this._Button1_i(),this._Button2_i(),this._Button3_i(),this._List2_i()];
	}
	var _proto = $exmlClass22.prototype;

	_proto._List1_i = function () {
		var t = new eui.List();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 1000;
		t.name = "lstMainMenu";
		t.scaleX = 0.6;
		t.scaleY = 0.6;
		t.width = 34;
		t.x = 0;
		t.y = 125;
		return t;
	};
	_proto._Button1_i = function () {
		var t = new eui.Button();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 24;
		t.label = "-";
		t.name = "btnZoomOut";
		t.width = 50;
		t.x = 0;
		t.y = 94;
		return t;
	};
	_proto._Button2_i = function () {
		var t = new eui.Button();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 24;
		t.label = "+";
		t.name = "btnZoomIn";
		t.width = 50;
		t.x = 51;
		t.y = 94;
		return t;
	};
	_proto._Button3_i = function () {
		var t = new eui.Button();
		t.height = 24;
		t.label = "存档";
		t.name = "btnSave";
		t.width = 50;
		t.x = 102;
		t.y = 94;
		return t;
	};
	_proto._List2_i = function () {
		var t = new eui.List();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 332;
		t.name = "lstTownNo";
		t.scaleX = 0.6;
		t.scaleY = 0.6;
		t.width = 53;
		t.x = 92;
		t.y = 121;
		return t;
	};
	return $exmlClass22;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CDistrictBtn.exml'] = window.$exmlClass23 = (function (_super) {
	__extends($exmlClass23, _super);
	function $exmlClass23() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 93;
		this.width = 89;
		this.elementsContent = [this._Image1_i()];
	}
	var _proto = $exmlClass23.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 93;
		t.source = "resource/assets/Spr/dangqianweizhi.png";
		t.width = 89;
		t.x = 0;
		t.y = 0;
		return t;
	};
	return $exmlClass23;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CPosButton.exml'] = window.$exmlClass24 = (function (_super) {
	__extends($exmlClass24, _super);
	function $exmlClass24() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 132;
		this.width = 132;
		this.elementsContent = [this._Image1_i()];
	}
	var _proto = $exmlClass24.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 190;
		t.scaleX = 0.6;
		t.scaleY = 0.6;
		t.source = "resource/assets/Spr/NewVersion12.1/UI/ (11).png";
		t.width = 190;
		t.x = 9;
		t.y = 8;
		return t;
	};
	return $exmlClass24;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CProgressBar.exml'] = window.$exmlClass25 = (function (_super) {
	__extends($exmlClass25, _super);
	function $exmlClass25() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 32;
		this.width = 100;
		this.elementsContent = [this._Label1_i(),this._Image1_i()];
	}
	var _proto = $exmlClass25.prototype;

	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.bold = true;
		t.height = 14;
		t.name = "labelTitle";
		t.size = 14;
		t.stroke = 2;
		t.text = "升级";
		t.textAlign = "center";
		t.textColor = 0xffffff;
		t.verticalAlign = "middle";
		t.width = 99;
		t.x = 1;
		t.y = 2.2;
		return t;
	};
	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 10;
		t.name = "pbMain";
		t.width = 100;
		t.x = 0;
		t.y = 20;
		return t;
	};
	return $exmlClass25;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CProgressBar2.exml'] = window.$exmlClass26 = (function (_super) {
	__extends($exmlClass26, _super);
	function $exmlClass26() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 15;
		this.width = 70;
		this.elementsContent = [this._Image1_i()];
	}
	var _proto = $exmlClass26.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 15;
		t.name = "pbMain";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 70;
		t.x = 0;
		t.y = 0;
		return t;
	};
	return $exmlClass26;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CSpecailPanel.exml'] = window.$exmlClass27 = (function (_super) {
	__extends($exmlClass27, _super);
	function $exmlClass27() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 1136;
		this.width = 640;
		this.elementsContent = [this._Image1_i(),this._Image2_i(),this._Image3_i(),this._Image4_i(),this._Image5_i(),this._Image6_i(),this._Image7_i(),this._Label1_i(),this._Label2_i(),this._Label3_i(),this._Label4_i(),this._Label5_i(),this._Label6_i(),this._Label7_i(),this._Label8_i(),this._Image8_i(),this._Image9_i(),this._Image10_i(),this._Image11_i()];
	}
	var _proto = $exmlClass27.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 848;
		t.name = "img0";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 600;
		t.x = 20;
		t.y = 167;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetY = 0;
		t.height = 466;
		t.name = "img9";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 600;
		t.x = 20;
		t.y = 549;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.height = 362;
		t.name = "img1";
		t.pixelHitTest = false;
		t.scaleX = 0.6;
		t.scaleY = 0.6;
		t.width = 632;
		t.x = 128.27;
		t.y = 231.5;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 80;
		t.name = "img2";
		t.scaleX = 0.8;
		t.scaleY = 0.8;
		t.width = 230;
		t.x = 30.94;
		t.y = 482;
		return t;
	};
	_proto._Image5_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 80;
		t.name = "img4";
		t.scaleX = 0.8;
		t.scaleY = 0.8;
		t.width = 230;
		t.x = 425;
		t.y = 482;
		return t;
	};
	_proto._Image6_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 80;
		t.name = "img3";
		t.scaleX = 0.8;
		t.scaleY = 0.8;
		t.width = 230;
		t.x = 229;
		t.y = 482;
		return t;
	};
	_proto._Image7_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 51.1;
		t.name = "img5";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 600;
		t.x = 20;
		t.y = 546.08;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 44;
		t.name = "txt1";
		t.size = 26;
		t.text = "1 / 47";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.verticalAlign = "middle";
		t.width = 602;
		t.x = 20;
		t.y = 602.24;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 44;
		t.name = "txt0";
		t.size = 26;
		t.text = "桃可梦乐园";
		t.textAlign = "center";
		t.textColor = 0xffffff;
		t.verticalAlign = "middle";
		t.width = 602;
		t.x = 18;
		t.y = 551.11;
		return t;
	};
	_proto._Label3_i = function () {
		var t = new eui.Label();
		t.size = 26;
		t.text = "物业属性：";
		t.textColor = 0x676767;
		t.x = 50;
		t.y = 672.91;
		return t;
	};
	_proto._Label4_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.name = "txt2";
		t.size = 26;
		t.text = "住宅";
		t.textColor = 0x000000;
		t.width = 133;
		t.x = 175;
		t.y = 672.91;
		return t;
	};
	_proto._Label5_i = function () {
		var t = new eui.Label();
		t.size = 26;
		t.text = "地块产出加成：";
		t.textColor = 0x676767;
		t.x = 48;
		t.y = 710.47;
		return t;
	};
	_proto._Label6_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.name = "txt3";
		t.size = 26;
		t.text = "+100%";
		t.textColor = 0x04993c;
		t.width = 133;
		t.x = 226.94;
		t.y = 710.47;
		return t;
	};
	_proto._Label7_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 54.24;
		t.size = 20;
		t.text = "“特殊建筑”是独一无二的，它能帮助地块收获更多的金币，并且自动标记为“历史的”（地块升级时不会用其它建筑取而代之）";
		t.textColor = 0x676767;
		t.width = 557.09;
		t.x = 47;
		t.y = 754.56;
		return t;
	};
	_proto._Label8_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 27.24;
		t.name = "txt4";
		t.size = 20;
		t.text = "“当前所选地块类型：住宅";
		t.textAlign = "center";
		t.textColor = 0x676767;
		t.width = 594.09;
		t.x = 24;
		t.y = 981.56;
		return t;
	};
	_proto._Image8_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 148;
		t.name = "img6";
		t.rotation = 0;
		t.source = "resource/assets/Spr/UI/arrow1.png";
		t.width = 104;
		t.x = 50;
		t.y = 824;
		return t;
	};
	_proto._Image9_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 150;
		t.name = "img7";
		t.width = 256;
		t.x = 197.15;
		t.y = 826;
		return t;
	};
	_proto._Image10_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 148;
		t.name = "img8";
		t.rotation = 0;
		t.source = "resource/assets/Spr/UI/arrow1.png";
		t.width = 104;
		t.x = 477.27;
		t.y = 830;
		return t;
	};
	_proto._Image11_i = function () {
		var t = new eui.Image();
		t.height = 96;
		t.name = "btnClose";
		t.width = 420;
		t.x = 110;
		t.y = 1040;
		return t;
	};
	return $exmlClass27;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CTaskButton.exml'] = window.$exmlClass28 = (function (_super) {
	__extends($exmlClass28, _super);
	function $exmlClass28() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 93;
		this.width = 89;
		this.elementsContent = [this._Image1_i()];
	}
	var _proto = $exmlClass28.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 93;
		t.source = "resource/assets/Spr/UI/task.png";
		t.width = 89;
		t.x = 0;
		t.y = 0;
		return t;
	};
	return $exmlClass28;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CToNextCityPanel.exml'] = window.$exmlClass29 = (function (_super) {
	__extends($exmlClass29, _super);
	function $exmlClass29() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 1136;
		this.width = 641;
		this.elementsContent = [this._Image1_i(),this._Image2_i(),this._Image3_i(),this._Image4_i(),this._Image5_i(),this._Label1_i(),this._Label2_i()];
	}
	var _proto = $exmlClass29.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.alpha = 0.5;
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 1136;
		t.name = "img0";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 641;
		t.x = 0;
		t.y = 1;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 422;
		t.name = "img1";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 500;
		t.x = 69;
		t.y = 332;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 62;
		t.name = "img2";
		t.width = 228;
		t.x = 83;
		t.y = 630;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 62;
		t.name = "img3";
		t.width = 228;
		t.x = 327;
		t.y = 630;
		return t;
	};
	_proto._Image5_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 62;
		t.name = "img4";
		t.width = 228;
		t.x = 206;
		t.y = 630;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.bold = true;
		t.height = 60;
		t.name = "txt0";
		t.size = 40;
		t.text = "恭喜";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.verticalAlign = "middle";
		t.width = 499;
		t.x = 71;
		t.y = 352;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 140;
		t.name = "txt1";
		t.text = "人口数已达到升级到下一个城市的标准。";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.verticalAlign = "middle";
		t.width = 372;
		t.x = 132;
		t.y = 429;
		return t;
	};
	return $exmlClass29;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CUpgradePanel.exml'] = window.$exmlClass30 = (function (_super) {
	__extends($exmlClass30, _super);
	function $exmlClass30() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 1136;
		this.width = 641;
		this.elementsContent = [this._Image1_i(),this._Image2_i(),this._Image3_i(),this._Image4_i(),this._Label1_i(),this._Label2_i(),this._Label3_i(),this._Label4_i(),this._Label5_i(),this._Label6_i(),this._Label7_i(),this._CheckBox1_i(),this._Label8_i(),this._Label9_i(),this._Label10_i(),this._Image5_i(),this._Label11_i(),this._Label12_i(),this._Image6_i(),this._Image7_i(),this._Image8_i()];
	}
	var _proto = $exmlClass30.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.anchorOffsetY = 0;
		t.height = 848;
		t.name = "img0";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 600;
		t.x = 20;
		t.y = 167;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.height = 362;
		t.name = "imgAvatar";
		t.pixelHitTest = false;
		t.scaleX = 0.6;
		t.scaleY = 0.6;
		t.width = 632;
		t.x = 128.27;
		t.y = 231.5;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 489.83;
		t.name = "img2";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 560;
		t.x = 37.87;
		t.y = 496.91;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.anchorOffsetY = 0;
		t.height = 50;
		t.name = "img1";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 560;
		t.x = 37.87;
		t.y = 499.52;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.name = "labelBuildingName";
		t.size = 25;
		t.text = "公园";
		t.textAlign = "center";
		t.textColor = 0xffffff;
		t.width = 560;
		t.x = 40;
		t.y = 512.64;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.name = "labelBuildingLevel";
		t.size = 25;
		t.text = "17  级";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.width = 560;
		t.x = 40;
		t.y = 555.5;
		return t;
	};
	_proto._Label3_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.size = 20;
		t.text = "金币产出率：";
		t.textColor = 0x778da9;
		t.width = 131.82;
		t.x = 65.42;
		t.y = 631.28;
		return t;
	};
	_proto._Label4_i = function () {
		var t = new eui.Label();
		t.name = "labelBuildingCPS";
		t.size = 20;
		t.text = "46";
		t.textColor = 0x000000;
		t.x = 180.94;
		t.y = 631.28;
		return t;
	};
	_proto._Label5_i = function () {
		var t = new eui.Label();
		t.size = 20;
		t.text = "/ 秒";
		t.textColor = 0x778da9;
		t.x = 215;
		t.y = 631.28;
		return t;
	};
	_proto._Label6_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.size = 20;
		t.text = "当前需求：";
		t.textAlign = "left";
		t.textColor = 0x778da9;
		t.width = 101.27;
		t.x = 65.42;
		t.y = 661.25;
		return t;
	};
	_proto._Label7_i = function () {
		var t = new eui.Label();
		t.name = "labelDemand";
		t.size = 20;
		t.text = "常规";
		t.textColor = 0x000000;
		t.x = 181.86;
		t.y = 661.25;
		return t;
	};
	_proto._CheckBox1_i = function () {
		var t = new eui.CheckBox();
		t.name = "cbHistorical";
		t.x = 427;
		t.y = 629.28;
		return t;
	};
	_proto._Label8_i = function () {
		var t = new eui.Label();
		t.anchorOffsetY = 0;
		t.height = 32.12;
		t.size = 20;
		t.text = "锁定建筑";
		t.textColor = 0x000000;
		t.x = 456.32;
		t.y = 632.92;
		return t;
	};
	_proto._Label9_i = function () {
		var t = new eui.Label();
		t.anchorOffsetY = 0;
		t.height = 40.76;
		t.multiline = true;
		t.size = 20;
		t.text = "锁定后建筑\n不会随地块升级而改变";
		t.textColor = 0x778da9;
		t.visible = false;
		t.x = 396.32;
		t.y = 640.2;
		return t;
	};
	_proto._Label10_i = function () {
		var t = new eui.Label();
		t.size = 20;
		t.text = "推土机";
		t.textColor = 0x778da9;
		t.x = 100.8;
		t.y = 794.64;
		return t;
	};
	_proto._Image5_i = function () {
		var t = new eui.Image();
		t.height = 105;
		t.name = "btnRevert";
		t.scaleX = 1;
		t.scaleY = 1;
		t.source = "resource/assets/Spr/1x1/1x1_1.png";
		t.width = 160;
		t.x = 49.76;
		t.y = 824.36;
		return t;
	};
	_proto._Label11_i = function () {
		var t = new eui.Label();
		t.size = 20;
		t.text = "紧急升级";
		t.textColor = 0x778da9;
		t.x = 270.64;
		t.y = 793.64;
		return t;
	};
	_proto._Label12_i = function () {
		var t = new eui.Label();
		t.size = 20;
		t.text = "手动摆放";
		t.textColor = 0x778da9;
		t.x = 458.64;
		t.y = 793.64;
		return t;
	};
	_proto._Image6_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 105;
		t.name = "btnInstantUpgrade";
		t.scaleX = 1;
		t.scaleY = 1;
		t.source = "resource/assets/Spr/1x1/1x1_1.png";
		t.width = 160;
		t.x = 234.32;
		t.y = 824;
		return t;
	};
	_proto._Image7_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 105;
		t.name = "btnMannual";
		t.scaleX = 1;
		t.scaleY = 1;
		t.source = "resource/assets/Spr/1x1/1x1_1.png";
		t.width = 160;
		t.x = 416.32;
		t.y = 824;
		return t;
	};
	_proto._Image8_i = function () {
		var t = new eui.Image();
		t.height = 96;
		t.name = "btnClose";
		t.width = 420;
		t.x = 110;
		t.y = 1038;
		return t;
	};
	return $exmlClass30;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/CWindTurbine.exml'] = window.$exmlClass31 = (function (_super) {
	__extends($exmlClass31, _super);
	function $exmlClass31() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 1136;
		this.width = 640;
		this.elementsContent = [this._Image1_i(),this._Image2_i(),this._Image3_i(),this._Image4_i(),this._Label1_i(),this._Label2_i(),this._Label3_i(),this._Label4_i(),this._Label5_i(),this._Label6_i(),this._Label7_i(),this._Image5_i()];
	}
	var _proto = $exmlClass31.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.anchorOffsetY = 0;
		t.height = 848.48;
		t.name = "img0";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 600;
		t.x = 20;
		t.y = 166.67;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 96;
		t.name = "btnClose";
		t.width = 420;
		t.x = 110;
		t.y = 1038;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 500;
		t.name = "img1";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 560;
		t.x = 40;
		t.y = 486.67;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 60;
		t.name = "img2";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 560;
		t.x = 40;
		t.y = 486.67;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetY = 0;
		t.height = 60;
		t.size = 32;
		t.text = "1座风车";
		t.textAlign = "center";
		t.textColor = 0xffffff;
		t.verticalAlign = "middle";
		t.width = 600;
		t.x = 20;
		t.y = 489.64;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.height = 60;
		t.text = "等级 1";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.verticalAlign = "middle";
		t.width = 600;
		t.x = 18.79;
		t.y = 551.09;
		return t;
	};
	_proto._Label3_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 33.03;
		t.text = "每座建筑金币产出率";
		t.textAlign = "center";
		t.textColor = 0x778da9;
		t.verticalAlign = "middle";
		t.width = 600;
		t.x = 18.79;
		t.y = 651.45;
		return t;
	};
	_proto._Label4_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 33.03;
		t.name = "labelEarningPerBuilding";
		t.text = "0 / 秒";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.width = 600;
		t.x = 20;
		t.y = 691.06;
		return t;
	};
	_proto._Label5_i = function () {
		var t = new eui.Label();
		t.text = "金币总产出率";
		t.textAlign = "center";
		t.textColor = 0x778da9;
		t.width = 600;
		t.x = 20;
		t.y = 755;
		return t;
	};
	_proto._Label6_i = function () {
		var t = new eui.Label();
		t.name = "labelTotalEarning";
		t.text = "0 / 秒";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.width = 600;
		t.x = 20;
		t.y = 793.88;
		return t;
	};
	_proto._Label7_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 142.12;
		t.text = "能源随着人口增长自动增长！";
		t.textAlign = "center";
		t.textColor = 0x778da9;
		t.verticalAlign = "middle";
		t.width = 600;
		t.x = 20;
		t.y = 836.79;
		return t;
	};
	_proto._Image5_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 256;
		t.width = 256;
		t.x = 192;
		t.y = 196.97;
		return t;
	};
	return $exmlClass31;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/DistrictSketch.exml'] = window.$exmlClass32 = (function (_super) {
	__extends($exmlClass32, _super);
	function $exmlClass32() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 91;
		this.width = 158;
		this.elementsContent = [this._Image1_i(),this._Label1_i()];
	}
	var _proto = $exmlClass32.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 91;
		t.source = "resource/assets/Spr/dibiao-baise.png";
		t.width = 158;
		t.x = 0;
		t.y = 0;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 38;
		t.name = "txtDistrictId";
		t.text = "1区";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.verticalAlign = "middle";
		t.width = 89.8;
		t.x = 35.2;
		t.y = 27;
		return t;
	};
	return $exmlClass32;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/Distrixt.exml'] = window.$exmlClass33 = (function (_super) {
	__extends($exmlClass33, _super);
	function $exmlClass33() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 1136;
		this.width = 640;
		this.elementsContent = [this._Image1_i(),this._Label1_i(),this._Label2_i(),this._Image2_i(),this._Image3_i()];
	}
	var _proto = $exmlClass33.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 879.79;
		t.name = "imgBg";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 598.79;
		t.x = 19.48;
		t.y = 26;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 58.79;
		t.name = "txtTitle";
		t.text = "“宝石湾”分区图";
		t.textAlign = "center";
		t.verticalAlign = "middle";
		t.width = 466.42;
		t.x = 97.52;
		t.y = 45.03;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 58.79;
		t.name = "txtTitle";
		t.text = "“宝石湾”分区图";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.verticalAlign = "middle";
		t.width = 466.42;
		t.x = 97.52;
		t.y = 45.03;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 38;
		t.name = "imgCurDistrict";
		t.source = "resource/assets/Spr/dangqianweizhi.png";
		t.width = 38;
		t.x = 35;
		t.y = 115.82;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 96;
		t.name = "btnClose";
		t.width = 420;
		t.x = 110;
		t.y = 769;
		return t;
	};
	return $exmlClass33;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/UIWelcome.exml'] = window.$exmlClass34 = (function (_super) {
	__extends($exmlClass34, _super);
	function $exmlClass34() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 1136;
		this.width = 640;
		this.elementsContent = [this._Image1_i(),this._Image2_i(),this._Label1_i(),this._Label2_i()];
	}
	var _proto = $exmlClass34.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 1136;
		t.name = "img0";
		t.source = "resource/assets/Spr/BaseRect.png";
		t.width = 640;
		t.x = 0;
		t.y = 0;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 206;
		t.source = "resource/assets/Spr/duokeli.png";
		t.width = 204;
		t.x = 222;
		t.y = 290;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.bold = true;
		t.height = 183;
		t.size = 60;
		t.textAlign = "center";
		t.verticalAlign = "middle";
		t.visible = false;
		t.width = 304;
		t.x = 274;
		t.y = 180;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.bold = true;
		t.height = 409;
		t.size = 65;
		t.text = "《开心工坊》\n\n中国人自己的乐高";
		t.textAlign = "center";
		t.textColor = 0xffffff;
		t.verticalAlign = "middle";
		t.width = 562;
		t.x = 39;
		t.y = 464;
		return t;
	};
	return $exmlClass34;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/test_ui.exml'] = window.$exmlClass35 = (function (_super) {
	__extends($exmlClass35, _super);
	function $exmlClass35() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 1136;
		this.width = 640;
		this.elementsContent = [this._Image1_i(),this._Image2_i(),this._Image3_i(),this._Image4_i(),this._Image5_i(),this._Label1_i(),this._Label2_i(),this._Label3_i(),this._Label4_i(),this._Label5_i(),this._Label6_i(),this._Label7_i(),this._Label8_i(),this._Image6_i(),this._Image7_i(),this._Label9_i(),this._Label10_i(),this._Image8_i(),this._Image9_i()];
	}
	var _proto = $exmlClass35.prototype;

	_proto._Image1_i = function () {
		var t = new eui.Image();
		t.height = 1136;
		t.name = "xiaoguotu";
		t.scaleX = 1;
		t.scaleY = 1;
		t.source = "resource/assets/Spr/NewVersion12.1/UI/xiaoguotu1.jpg";
		t.visible = false;
		t.width = 640;
		t.x = 0;
		t.y = 0;
		return t;
	};
	_proto._Image2_i = function () {
		var t = new eui.Image();
		t.height = 62;
		t.name = "coin_bg";
		t.scaleX = 0.6;
		t.scaleY = 0.6;
		t.source = "resource/assets/Spr/NewVersion12.1/UI/coin_bg.png";
		t.width = 307;
		t.x = 12;
		t.y = 8.63;
		return t;
	};
	_proto._Image3_i = function () {
		var t = new eui.Image();
		t.height = 62;
		t.name = "coin_bg";
		t.scaleX = 0.59;
		t.scaleY = 0.59;
		t.source = "resource/assets/Spr/NewVersion12.1/UI/coin_bg1.png";
		t.width = 369;
		t.x = 211.15;
		t.y = 8.34;
		return t;
	};
	_proto._Image4_i = function () {
		var t = new eui.Image();
		t.height = 62;
		t.name = "coin_bg";
		t.scaleX = 0.6;
		t.scaleY = 0.6;
		t.source = "resource/assets/Spr/NewVersion12.1/UI/coin_bg.png";
		t.width = 307;
		t.x = 440.43;
		t.y = 8.8;
		return t;
	};
	_proto._Image5_i = function () {
		var t = new eui.Image();
		t.height = 46;
		t.name = "coin_bg";
		t.scaleX = 0.6;
		t.scaleY = 0.6;
		t.source = "resource/assets/Spr/NewVersion12.1/UI/ (6).png";
		t.width = 400;
		t.x = 386.43;
		t.y = 54.8;
		return t;
	};
	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.bold = true;
		t.height = 35.33;
		t.name = "labelCityName";
		t.scaleY = 1;
		t.size = 18;
		t.text = "闪金平原";
		t.textAlign = "center";
		t.textColor = 0xffffff;
		t.verticalAlign = "middle";
		t.width = 107;
		t.x = 386.75;
		t.y = 50.7;
		return t;
	};
	_proto._Label2_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 30.67;
		t.name = "labelUnit";
		t.scaleY = 1.2;
		t.size = 18;
		t.text = "Million";
		t.textAlign = "left";
		t.textColor = 0x778da9;
		t.verticalAlign = "middle";
		t.visible = false;
		t.width = 55.5;
		t.x = 342.66;
		t.y = 38.66;
		return t;
	};
	_proto._Label3_i = function () {
		var t = new eui.Label();
		t.text = "+";
		t.textAlign = "right";
		t.textColor = 0xb5b1b1;
		t.visible = false;
		t.x = 541;
		t.y = 30.16;
		return t;
	};
	_proto._Label4_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 26;
		t.name = "labelCPS";
		t.size = 14;
		t.text = "2547";
		t.textAlign = "right";
		t.textColor = 0xffffff;
		t.verticalAlign = "middle";
		t.width = 54;
		t.x = 471.53;
		t.y = 56.7;
		return t;
	};
	_proto._Label5_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 26;
		t.name = "labelPerSec";
		t.size = 14;
		t.text = "/秒";
		t.textAlign = "right";
		t.textColor = 0xffffff;
		t.verticalAlign = "middle";
		t.width = 24;
		t.x = 523.53;
		t.y = 56.7;
		return t;
	};
	_proto._Label6_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 26.67;
		t.name = "txtKeysBoost";
		t.size = 14;
		t.text = "(+0%)";
		t.textAlign = "left";
		t.textColor = 0x12ec00;
		t.verticalAlign = "middle";
		t.width = 88.34;
		t.x = 567.91;
		t.y = 55.7;
		return t;
	};
	_proto._Label7_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.name = "txtDebugInfo";
		t.size = 20;
		t.text = "Debug Info";
		t.textAlign = "left";
		t.textColor = 0x595656;
		t.verticalAlign = "middle";
		t.visible = false;
		t.width = 359;
		t.x = 19;
		t.y = 72;
		return t;
	};
	_proto._Label8_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.bold = true;
		t.borderColor = 0x000000;
		t.height = 22.03;
		t.name = "labelDiamond";
		t.rotation = 359.8;
		t.size = 24;
		t.text = "500";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.width = 148;
		t.x = 474.55;
		t.y = 15.52;
		return t;
	};
	_proto._Image6_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 50;
		t.name = "imgCoinIcon";
		t.scaleX = 0.45;
		t.scaleY = 0.45;
		t.source = "resource/assets/Spr/NewVersion12.1/UI/jinbi.png";
		t.width = 53;
		t.x = 218.24;
		t.y = 15.98;
		return t;
	};
	_proto._Image7_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 47;
		t.name = "imgPopulation";
		t.scaleX = 0.6;
		t.scaleY = 0.6;
		t.source = "resource/assets/Spr/NewVersion12.1/UI/renkou.png";
		t.width = 49;
		t.x = 39.86;
		t.y = 13;
		return t;
	};
	_proto._Label9_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.bold = true;
		t.fontFamily = "Arial";
		t.height = 34;
		t.name = "labelPopulation";
		t.size = 23;
		t.text = "9200";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.verticalAlign = "middle";
		t.width = 143;
		t.x = 56.2;
		t.y = 9.83;
		return t;
	};
	_proto._Label10_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.bold = true;
		t.name = "labelTotoalcoin";
		t.scaleX = 1;
		t.scaleY = 1;
		t.size = 23;
		t.text = "50,000";
		t.textAlign = "center";
		t.textColor = 0x000000;
		t.width = 183.66;
		t.x = 239.09;
		t.y = 16;
		return t;
	};
	_proto._Image8_i = function () {
		var t = new eui.Image();
		t.anchorOffsetX = 0;
		t.anchorOffsetY = 0;
		t.height = 48;
		t.name = "imgDiamond";
		t.scaleX = 0.6;
		t.scaleY = 0.6;
		t.source = "resource/assets/Spr/NewVersion12.1/UI/lvpiao.png";
		t.visible = true;
		t.width = 67;
		t.x = 452.9;
		t.y = 14;
		return t;
	};
	_proto._Image9_i = function () {
		var t = new eui.Image();
		t.height = 116;
		t.name = "imgNextCity";
		t.scaleX = 0.6;
		t.scaleY = 0.6;
		t.source = "resource/assets/Spr/NewVersion12.1/UI/(12).png";
		t.visible = false;
		t.width = 116;
		t.x = 18;
		t.y = 69.7;
		return t;
	};
	return $exmlClass35;
})(eui.Skin);generateEUI.paths['resource/assets/MyExml/UIBase/CUIBaseNumBubble.exml'] = window.$exmlClass36 = (function (_super) {
	__extends($exmlClass36, _super);
	function $exmlClass36() {
		_super.call(this);
		this.skinParts = [];
		
		this.height = 64;
		this.width = 64;
		this.elementsContent = [this._Label1_i()];
	}
	var _proto = $exmlClass36.prototype;

	_proto._Label1_i = function () {
		var t = new eui.Label();
		t.anchorOffsetX = 0;
		t.bold = true;
		t.height = 64;
		t.name = "txtNum";
		t.size = 40;
		t.text = "12";
		t.textAlign = "center";
		t.verticalAlign = "middle";
		t.width = 64;
		t.x = 0;
		t.y = 0;
		return t;
	};
	return $exmlClass36;
})(eui.Skin);