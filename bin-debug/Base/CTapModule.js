/*
     “点击”控制模块。多点触控功能就在这里面

*/
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CTapModule = (function (_super) {
    __extends(CTapModule, _super);
    function CTapModule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CTapModule.AddTapPoint = function (touch_id, x, y) {
        var bExist = false;
        for (var i = 0; i < CTapModule.s_aryTouchPoint.length; i++) {
            if (touch_id == CTapModule.s_aryTouchPoint[i].id) {
                CTapModule.s_aryTouchPoint[i].x = x;
                CTapModule.s_aryTouchPoint[i].y = y;
                bExist = true;
                break;
            }
        }
        if (!bExist) {
            if (CTapModule.s_aryTouchPoint.length >= 2) {
                CResourceManager.DeleteVector(CTapModule.s_aryTouchPoint[0]);
                CTapModule.s_aryTouchPoint.shift();
            }
            var point = CResourceManager.NewVector();
            point.id = touch_id;
            point.x = x;
            point.y = y;
            CTapModule.s_aryTouchPoint.push(point);
        }
        if (CTapModule.s_aryTouchPoint.length == 2) {
            //   CUIManager.s_uiMainTitle.ShowDebugInfo("cur evt =" +  CTapModule.s_nCurDisBetweenTwoPoints );
            if (!CTapModule.s_bZooming) {
                CTapModule.BeginZoom();
            }
        }
    };
    CTapModule.BeginZoom = function () {
        CTapModule.s_bZooming = true;
        CTapModule.m_fTimeElapse = 0;
        CTapModule.s_nCurDisBetweenTwoPoints = CTapModule.CalculateDisBetweenTwoPoints();
    };
    CTapModule.EndZoom = function () {
        CTapModule.s_bZooming = false;
    };
    CTapModule.FixedUpdate = function () {
        if (!CTapModule.s_bZooming) {
            return;
        }
        CTapModule.m_fTimeElapse += CDef.s_fFixedDeltaTime;
        if (CTapModule.m_fTimeElapse < 0.5) {
            return;
        }
        CTapModule.m_fTimeElapse = 0;
        if (CTapModule.s_aryTouchPoint.length != 2) {
            CTapModule.EndZoom();
            return;
        }
        var nCurDis = CTapModule.CalculateDisBetweenTwoPoints();
        var fBiLi = (nCurDis - CTapModule.s_nCurDisBetweenTwoPoints);
        // CUIManager.s_uiMainTitle.ShowDebugInfo("cur evt =" + CTapModule.s_aryTouchPoint[0].y + "_" + CTapModule.s_aryTouchPoint[1].y  );
        CTapModule.s_nCurDisBetweenTwoPoints = nCurDis;
        if (fBiLi < 0) {
            Main.s_CurTown.BeginZoom(-1);
            // Main.s_CurTown.Zoom( -0.1 );
        }
        else if (fBiLi > 0) {
            Main.s_CurTown.BeginZoom(1);
            //    Main.s_CurTown.Zoom( 0.1 );
        }
    };
    CTapModule.CalculateDisBetweenTwoPoints = function () {
        var x0 = CTapModule.s_aryTouchPoint[0].x;
        var y0 = CTapModule.s_aryTouchPoint[0].y;
        var x1 = CTapModule.s_aryTouchPoint[1].x;
        var y1 = CTapModule.s_aryTouchPoint[1].y;
        var dis = (x0 - x1) * (x0 - x1) + (y0 - y1) * (y0 - y1);
        return dis; //Math.sqrt( dis );
    };
    CTapModule.RemovePoint = function (touch_id) {
        for (var i = 0; i < CTapModule.s_aryTouchPoint.length; i++) {
            if (CTapModule.s_aryTouchPoint[i].id == touch_id) {
                CResourceManager.DeleteVector(CTapModule.s_aryTouchPoint[i]);
                CTapModule.s_aryTouchPoint.splice(i, 1);
                return;
            }
        }
        //  console.log( "有bug，没找到对应的touch_id" );
    };
    //  直行双点触控以缩放
    CTapModule.TwoPointsTapToZoom = function () {
        if (CTapModule.s_aryTouchPoint.length < 2) {
            return false;
        }
        /*
        var nCurDis:number = CTapModule.CalculateDisBetweenTwoPoints();
        
        if ( CTapModule.s_nCurDisBetweenTwoPoints <= 0 )
        {
            CTapModule.s_nCurDisBetweenTwoPoints = 1;
        }
        var fBiLi:number = ( nCurDis - CTapModule.s_nCurDisBetweenTwoPoints ) / CTapModule.s_nCurDisBetweenTwoPoints;
        CUIManager.s_uiMainTitle.ShowDebugInfo("cur evt =" + nCurDis+ "_" + CTapModule.s_nCurDisBetweenTwoPoints );
        CTapModule.s_nCurDisBetweenTwoPoints = nCurDis;
   
        Main.s_CurTown.Zoom( fBiLi );
        */
        return true;
    };
    CTapModule.s_aryTouchPoint = new Array();
    CTapModule.s_nCurDisBetweenTwoPoints = 0;
    CTapModule.s_bZooming = false;
    CTapModule.m_fTimeElapse = 0;
    return CTapModule;
}(egret.DisplayObjectContainer)); // end class
__reflect(CTapModule.prototype, "CTapModule");
//# sourceMappingURL=CTapModule.js.map