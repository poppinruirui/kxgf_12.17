var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CTiaoZiManager = (function (_super) {
    __extends(CTiaoZiManager, _super);
    function CTiaoZiManager() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CTiaoZiManager.Init = function () {
        CTiaoZiManager.s_fAlphaChangeSpeed = -1 / (CDef.s_fTiaoZiTotalTime * 0.5);
        CTiaoZiManager.s_fPosChangeA = CyberTreeMath.GetA(CDef.s_fTiaoZiTotalDistance, CDef.s_fTiaoZiTotalTime);
        CTiaoZiManager.s_fPosChangeV0 = CyberTreeMath.GetV0(CDef.s_fTiaoZiTotalDistance, CDef.s_fTiaoZiTotalTime);
    };
    CTiaoZiManager.NewTiaoZi = function () {
        var tiaozi = null;
        if (CTiaoZiManager.s_lstRecycledTiaoZi.numChildren > 0) {
            tiaozi = CTiaoZiManager.s_lstRecycledTiaoZi.getChildAt(0);
            CTiaoZiManager.s_lstRecycledTiaoZi.removeChildAt(0);
            tiaozi.Reset();
        }
        else {
            tiaozi = new CTiaoZi();
            tiaozi.anchorOffsetX = tiaozi.width / 2;
            tiaozi.anchorOffsetY = tiaozi.height;
        }
        CTiaoZiManager.s_lstcontainerTiaoZi.addChild(tiaozi);
        return tiaozi;
    };
    CTiaoZiManager.DeleteTiaoZi = function (tiaozi) {
        CTiaoZiManager.s_lstRecycledTiaoZi.addChild(tiaozi);
    };
    CTiaoZiManager.FixedUpdate = function () {
        for (var i = CTiaoZiManager.s_lstcontainerTiaoZi.numChildren - 1; i >= 0; i--) {
            var tiaozi = CTiaoZiManager.s_lstcontainerTiaoZi.getChildAt(i);
            tiaozi.FixedUpdate();
        }
    };
    CTiaoZiManager.s_lstRecycledTiaoZi = new egret.DisplayObjectContainer();
    CTiaoZiManager.s_lstcontainerTiaoZi = new egret.DisplayObjectContainer();
    CTiaoZiManager.s_fAlphaChangeSpeed = 0;
    CTiaoZiManager.s_fPosChangeA = 0;
    CTiaoZiManager.s_fPosChangeV0 = 0;
    return CTiaoZiManager;
}(CObj)); // end class
__reflect(CTiaoZiManager.prototype, "CTiaoZiManager");
//# sourceMappingURL=CTiaoZiManager.js.map