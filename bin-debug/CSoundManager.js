var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/*
 音频系统
*/
var Global;
(function (Global) {
    var eSE;
    (function (eSE) {
        eSE[eSE["bank"] = 0] = "bank";
        eSE[eSE["big_coin"] = 1] = "big_coin";
        eSE[eSE["car"] = 2] = "car";
        eSE[eSE["click_button"] = 3] = "click_button";
        eSE[eSE["common_page_1"] = 4] = "common_page_1";
        eSE[eSE["common_page_2"] = 5] = "common_page_2";
        eSE[eSE["green_bux"] = 6] = "green_bux";
        eSE[eSE["small_coin"] = 7] = "small_coin";
        eSE[eSE["congratulations"] = 8] = "congratulations";
        eSE[eSE["begin_upgrade"] = 9] = "begin_upgrade";
        eSE[eSE["upgrad_complete"] = 10] = "upgrad_complete";
        eSE[eSE["revert"] = 11] = "revert";
        eSE[eSE["num"] = 12] = "num";
    })(eSE = Global.eSE || (Global.eSE = {}));
})(Global || (Global = {})); // end module Global
var CSoundManager = (function (_super) {
    __extends(CSoundManager, _super);
    function CSoundManager() {
        return _super.call(this) || this;
    }
    CSoundManager.onLoadMusicComplete = function (event) {
        CSoundManager.s_soundBMG = event;
        CSoundManager.s_soundBMG.play();
    };
    CSoundManager.onLoadAudioComplete_SmallCoin = function (event) {
        //    CUIManager.s_uiMainTitle.ShowDebugInfo(  "load audio completed:" +  event.type );
        CSoundManager.s_dicSE[Global.eSE.small_coin] = event;
    };
    CSoundManager.Init = function () {
        // var sound1:egret.Sound = RES.getResByUrl();
        CSoundManager.s_soundBMG = new egret.Sound();
        CSoundManager.s_soundBMG.addEventListener(egret.Event.COMPLETE, function loadOver(event) {
            console.log("loaded bmg completed!");
        }, this);
        CSoundManager.s_soundBMG.addEventListener(egret.IOErrorEvent.IO_ERROR, function loadError(event) {
            console.log("loaded bmg error!");
        }, this);
        //resource/assets/Audios/small_coin
        var music_url = CResourceManager.url + "bmg1.mp3";
        RES.getResByUrl(music_url, this.onLoadMusicComplete, this, RES.ResourceItem.TYPE_SOUND);
        /*
        var loader:egret.URLLoader = new egret.URLLoader();
         loader.addEventListener(egret.Event.COMPLETE, function loadOver(event:egret.Event) {
         console.log( "背景音乐加载成功" );
             var sound:egret.Sound = loader.data;
             sound.play();
         }, this);
         loader.dataFormat = egret.URLLoaderDataFormat.SOUND;
         loader.load(new egret.URLRequest(music_url));
       */
        //  var audio_url:string = "http://39.108.53.61:5566/data/small_coin.mp3";
        //  RES.getResByUrl( audio_url ,this.onLoadAudioComplete_SmallCoin,this,RES.ResourceItem.TYPE_SOUND);
        /*
        CSoundManager.s_soundBMG = new egret.Sound();
       CSoundManager.s_soundBMG.addEventListener(egret.Event.COMPLETE, function loadOver(event:egret.Event) {
          
            
       }, this);
       CSoundManager.s_soundBMG.addEventListener(egret.IOErrorEvent.IO_ERROR, function loadError(event:egret.IOErrorEvent) {
           console.log("loaded bmg error!");
       }, this);
       CSoundManager.s_soundBMG.load("resource/assets/Music/duokeli.mp3");
       */
        CSoundManager.s_arySE = new Array();
        var sound = new egret.Sound();
        sound.load("resource/assets/Audios/bank.mp3");
        CSoundManager.s_arySE.push(sound);
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/big_coin.mp3");
        CSoundManager.s_arySE.push(sound);
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/car.mp3");
        CSoundManager.s_arySE.push(sound);
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/click_button.mp3");
        CSoundManager.s_arySE.push(sound);
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/common_page_1.mp3");
        CSoundManager.s_arySE.push(sound);
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/common_page_2.mp3");
        CSoundManager.s_arySE.push(sound);
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/green_bux.mp3");
        CSoundManager.s_arySE.push(sound);
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/small_coin.mp3");
        CSoundManager.s_arySE.push(sound);
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/congratulations.mp3");
        CSoundManager.s_arySE.push(sound);
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/start_upgrade.mp3");
        CSoundManager.s_arySE.push(sound);
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/end_upgrade.mp3");
        CSoundManager.s_arySE.push(sound);
        sound = new egret.Sound();
        sound.load("resource/assets/Audios/revert.mp3");
        CSoundManager.s_arySE.push(sound);
    };
    CSoundManager.PlaySE = function (eId) {
        //  CSoundManager.s_arySE[eId].play( 0, 1 );
        var sound = CSoundManager.s_arySE[eId];
        if (sound == null || sound == undefined) {
            return;
        }
        sound.play(0, 1);
    };
    CSoundManager.PlayBMG = function () {
        if (CSoundManager.s_bBMGPlaying) {
            return;
        }
        CSoundManager.s_soundBMG.play();
        CSoundManager.s_bBMGPlaying = true;
    };
    CSoundManager.s_dicSE = new Object();
    CSoundManager.s_bBMGPlaying = false;
    return CSoundManager;
}(egret.DisplayObjectContainer)); // end class
__reflect(CSoundManager.prototype, "CSoundManager");
//# sourceMappingURL=CSoundManager.js.map