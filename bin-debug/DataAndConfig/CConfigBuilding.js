var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CConfigBuilding = (function (_super) {
    __extends(CConfigBuilding, _super);
    function CConfigBuilding() {
        var _this = _super.call(this) || this;
        _this.ePropertyType = Global.eLotPsroperty.business;
        _this.nID = 0;
        _this.szDesc = "";
        _this.szName = "";
        _this.nPrice = 0;
        _this.szResName = "";
        _this.bUnLocked = true;
        _this.fGainRate = 0; // 增益率
        _this.bSpecial = false;
        _this.bUsed = false; // 已使用（仅针对特殊建筑）
        _this.nIndexOfThisType = 0;
        return _this;
    } // end constructor
    return CConfigBuilding;
}(egret.DisplayObjectContainer)); // end class
__reflect(CConfigBuilding.prototype, "CConfigBuilding");
//# sourceMappingURL=CConfigBuilding.js.map