var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
/*
显示类配置在这个文件里面

逻辑类数值在CConfigmanager里面
*/
var Global;
(function (Global) {
    var eOperate;
    (function (eOperate) {
        eOperate[eOperate["toggle_grid"] = 0] = "toggle_grid";
        eOperate[eOperate["edit_obj"] = 1] = "edit_obj";
        eOperate[eOperate["del_obj"] = 2] = "del_obj";
        eOperate[eOperate["add_diamond"] = 3] = "add_diamond";
        eOperate[eOperate["add_coin"] = 4] = "add_coin";
        eOperate[eOperate["add_cityhall"] = 5] = "add_cityhall";
        eOperate[eOperate["add_garage"] = 6] = "add_garage";
        eOperate[eOperate["add_bank"] = 7] = "add_bank";
        eOperate[eOperate["add_windturbine"] = 8] = "add_windturbine";
        eOperate[eOperate["add_airport"] = 9] = "add_airport";
        eOperate[eOperate["add_harbour"] = 10] = "add_harbour";
        // (老的添加马路流程废弃)
        eOperate[eOperate["add_road_zuoshangyouxia"] = 11] = "add_road_zuoshangyouxia";
        eOperate[eOperate["add_road_youshangzuoxia"] = 12] = "add_road_youshangzuoxia";
        eOperate[eOperate["add_road_corner_shang"] = 13] = "add_road_corner_shang";
        eOperate[eOperate["add_road_corner_xia"] = 14] = "add_road_corner_xia";
        eOperate[eOperate["add_road_corner_zuo"] = 15] = "add_road_corner_zuo";
        eOperate[eOperate["add_road_corner_you"] = 16] = "add_road_corner_you";
        // 新的添加马路流程
        eOperate[eOperate["add_road"] = 17] = "add_road";
        eOperate[eOperate["add_test_car"] = 18] = "add_test_car";
        eOperate[eOperate["toggle_show_road"] = 19] = "toggle_show_road";
        eOperate[eOperate["toggle_tile_touch_enabled"] = 20] = "toggle_tile_touch_enabled";
        eOperate[eOperate["add_road_t_zuoxia"] = 21] = "add_road_t_zuoxia";
        eOperate[eOperate["add_tile_2x2"] = 22] = "add_tile_2x2";
        eOperate[eOperate["add_tile_4x4"] = 23] = "add_tile_4x4";
        eOperate[eOperate["add_dibiao"] = 24] = "add_dibiao";
        eOperate[eOperate["del_dibiao"] = 25] = "del_dibiao";
        eOperate[eOperate["add_tree"] = 26] = "add_tree";
        eOperate[eOperate["add_arrow_zuoshang"] = 27] = "add_arrow_zuoshang";
        eOperate[eOperate["add_arrow_zuoxia"] = 28] = "add_arrow_zuoxia";
        eOperate[eOperate["add_arrow_youshang"] = 29] = "add_arrow_youshang";
        eOperate[eOperate["add_arrow_youxia"] = 30] = "add_arrow_youxia";
        eOperate[eOperate["do_nothing"] = 31] = "do_nothing";
        eOperate[eOperate["num"] = 32] = "num";
    })(eOperate = Global.eOperate || (Global.eOperate = {}));
    // 尺寸规格
    var eObjSize;
    (function (eObjSize) {
        eObjSize[eObjSize["size_1x1"] = 0] = "size_1x1";
        eObjSize[eObjSize["size_2x2"] = 1] = "size_2x2";
        eObjSize[eObjSize["size_4x4"] = 2] = "size_4x4";
    })(eObjSize = Global.eObjSize || (Global.eObjSize = {}));
    // 地块状态
    var eBuildingLandStatus;
    (function (eBuildingLandStatus) {
        eBuildingLandStatus[eBuildingLandStatus["empty"] = 0] = "empty";
        eBuildingLandStatus[eBuildingLandStatus["contructing"] = 1] = "contructing";
        eBuildingLandStatus[eBuildingLandStatus["building_exist"] = 2] = "building_exist";
    })(eBuildingLandStatus = Global.eBuildingLandStatus || (Global.eBuildingLandStatus = {}));
    // 物件的功能分类
    var eObjFunc;
    (function (eObjFunc) {
        eObjFunc[eObjFunc["none"] = 0] = "none";
        eObjFunc[eObjFunc["road"] = 1] = "road";
        eObjFunc[eObjFunc["building"] = 2] = "building";
        eObjFunc[eObjFunc["car"] = 3] = "car";
        eObjFunc[eObjFunc["airplane"] = 4] = "airplane";
        eObjFunc[eObjFunc["ship"] = 5] = "ship";
        eObjFunc[eObjFunc["tile"] = 6] = "tile";
    })(eObjFunc = Global.eObjFunc || (Global.eObjFunc = {}));
    // 特殊职能建筑，由系统生成，玩家不能去建设或修改
    var eBuildinSpeical;
    (function (eBuildinSpeical) {
        eBuildinSpeical[eBuildinSpeical["none"] = 0] = "none";
        // ----- 以下为特殊职能建筑
        eBuildinSpeical[eBuildinSpeical["cityhall"] = 1] = "cityhall";
        eBuildinSpeical[eBuildinSpeical["bank"] = 2] = "bank";
        eBuildinSpeical[eBuildinSpeical["garage"] = 3] = "garage";
        eBuildinSpeical[eBuildinSpeical["airport"] = 4] = "airport";
        eBuildinSpeical[eBuildinSpeical["num"] = 5] = "num";
    })(eBuildinSpeical = Global.eBuildinSpeical || (Global.eBuildinSpeical = {}));
    // 汽车的运行方向
    var eAutomobileDir;
    (function (eAutomobileDir) {
        eAutomobileDir[eAutomobileDir["zuoshang"] = 0] = "zuoshang";
        eAutomobileDir[eAutomobileDir["youshang"] = 1] = "youshang";
        eAutomobileDir[eAutomobileDir["zuoxia"] = 2] = "zuoxia";
        eAutomobileDir[eAutomobileDir["youxia"] = 3] = "youxia";
    })(eAutomobileDir = Global.eAutomobileDir || (Global.eAutomobileDir = {}));
    // 汽车转弯的方向
    var eNeighbourDir;
    (function (eNeighbourDir) {
        eNeighbourDir[eNeighbourDir["qian"] = 0] = "qian";
        eNeighbourDir[eNeighbourDir["hou"] = 1] = "hou";
        eNeighbourDir[eNeighbourDir["zuo"] = 2] = "zuo";
        eNeighbourDir[eNeighbourDir["you"] = 3] = "you";
    })(eNeighbourDir = Global.eNeighbourDir || (Global.eNeighbourDir = {}));
    // 汽车的运行状态
    var eAutomobileStatus;
    (function (eAutomobileStatus) {
        eAutomobileStatus[eAutomobileStatus["none"] = 0] = "none";
        eAutomobileStatus[eAutomobileStatus["normal"] = 1] = "normal";
        eAutomobileStatus[eAutomobileStatus["u_turn"] = 2] = "u_turn";
        eAutomobileStatus[eAutomobileStatus["turn_left"] = 3] = "turn_left";
        eAutomobileStatus[eAutomobileStatus["turn_right"] = 4] = "turn_right";
    })(eAutomobileStatus = Global.eAutomobileStatus || (Global.eAutomobileStatus = {}));
    var eDoubleRectBgType;
    (function (eDoubleRectBgType) {
        eDoubleRectBgType[eDoubleRectBgType["up_down"] = 0] = "up_down";
        eDoubleRectBgType[eDoubleRectBgType["in_out"] = 1] = "in_out";
    })(eDoubleRectBgType = Global.eDoubleRectBgType || (Global.eDoubleRectBgType = {}));
    // 各功能界面的ID号
    var eUiId;
    (function (eUiId) {
        eUiId[eUiId["garage"] = 0] = "garage";
        eUiId[eUiId["main_title"] = 1] = "main_title";
        eUiId[eUiId["developer_editor"] = 2] = "developer_editor";
        eUiId[eUiId["bottom_buttons"] = 3] = "bottom_buttons";
        eUiId[eUiId["buy_lot_panel"] = 4] = "buy_lot_panel";
        eUiId[eUiId["wind_turbine"] = 5] = "wind_turbine";
        eUiId[eUiId["lot_upgrade"] = 6] = "lot_upgrade";
        eUiId[eUiId["revert_lot_confirm"] = 7] = "revert_lot_confirm";
        eUiId[eUiId["boost_panel"] = 8] = "boost_panel";
        eUiId[eUiId["city_hall"] = 9] = "city_hall";
        eUiId[eUiId["bank"] = 10] = "bank";
        eUiId[eUiId["bank_back"] = 11] = "bank_back";
        eUiId[eUiId["special_panel"] = 12] = "special_panel";
        eUiId[eUiId["to_next_city"] = 13] = "to_next_city";
        eUiId[eUiId["welcome"] = 14] = "welcome";
        eUiId[eUiId["district"] = 15] = "district";
    })(eUiId = Global.eUiId || (Global.eUiId = {}));
    // 空各自种类
    var eEmptyGridType;
    (function (eEmptyGridType) {
        eEmptyGridType[eEmptyGridType["inside_city"] = 0] = "inside_city";
        eEmptyGridType[eEmptyGridType["city_edge"] = 1] = "city_edge";
        eEmptyGridType[eEmptyGridType["outside_city"] = 2] = "outside_city";
    })(eEmptyGridType = Global.eEmptyGridType || (Global.eEmptyGridType = {}));
    // 地块的属性
    var eLotPsroperty;
    (function (eLotPsroperty) {
        eLotPsroperty[eLotPsroperty["residential"] = 0] = "residential";
        eLotPsroperty[eLotPsroperty["business"] = 1] = "business";
        eLotPsroperty[eLotPsroperty["service"] = 2] = "service";
        eLotPsroperty[eLotPsroperty["length"] = 3] = "length";
    })(eLotPsroperty = Global.eLotPsroperty || (Global.eLotPsroperty = {}));
    // 车商城里面条目的状态
    var eGarageItemStatus;
    (function (eGarageItemStatus) {
        eGarageItemStatus[eGarageItemStatus["locked_and_can_unlock"] = 0] = "locked_and_can_unlock";
        eGarageItemStatus[eGarageItemStatus["unlocked"] = 1] = "unlocked";
        eGarageItemStatus[eGarageItemStatus["can_not_unlock"] = 2] = "can_not_unlock";
    })(eGarageItemStatus = Global.eGarageItemStatus || (Global.eGarageItemStatus = {}));
    // 币的种类
    var eMoneySubType;
    (function (eMoneySubType) {
        eMoneySubType[eMoneySubType["small_coin"] = 0] = "small_coin";
        eMoneySubType[eMoneySubType["big_coin"] = 1] = "big_coin";
        eMoneySubType[eMoneySubType["small_diamond"] = 2] = "small_diamond";
        eMoneySubType[eMoneySubType["big_diamond"] = 3] = "big_diamond";
    })(eMoneySubType = Global.eMoneySubType || (Global.eMoneySubType = {}));
    // 进度条的种类
    var eProgressBarType;
    (function (eProgressBarType) {
        eProgressBarType[eProgressBarType["none"] = 0] = "none";
        eProgressBarType[eProgressBarType["constructing"] = 1] = "constructing";
        eProgressBarType[eProgressBarType["upgrading"] = 2] = "upgrading";
    })(eProgressBarType = Global.eProgressBarType || (Global.eProgressBarType = {}));
    // 跳字的种类
    var eTiaoZiType;
    (function (eTiaoZiType) {
        eTiaoZiType[eTiaoZiType["money"] = 0] = "money";
        eTiaoZiType[eTiaoZiType["level"] = 1] = "level";
    })(eTiaoZiType = Global.eTiaoZiType || (Global.eTiaoZiType = {}));
    var eGridCornerPosType;
    (function (eGridCornerPosType) {
        eGridCornerPosType[eGridCornerPosType["top"] = 0] = "top";
        eGridCornerPosType[eGridCornerPosType["bottom"] = 1] = "bottom";
        eGridCornerPosType[eGridCornerPosType["left"] = 2] = "left";
        eGridCornerPosType[eGridCornerPosType["right"] = 3] = "right";
        eGridCornerPosType[eGridCornerPosType["one_fourth_0"] = 4] = "one_fourth_0";
        eGridCornerPosType[eGridCornerPosType["one_fourth_1"] = 5] = "one_fourth_1";
        eGridCornerPosType[eGridCornerPosType["one_fourth_2"] = 6] = "one_fourth_2";
        eGridCornerPosType[eGridCornerPosType["one_fourth_3"] = 7] = "one_fourth_3";
        eGridCornerPosType[eGridCornerPosType["one_fourth_4"] = 8] = "one_fourth_4";
        eGridCornerPosType[eGridCornerPosType["one_fourth_5"] = 9] = "one_fourth_5";
        eGridCornerPosType[eGridCornerPosType["one_fourth_6"] = 10] = "one_fourth_6";
        eGridCornerPosType[eGridCornerPosType["one_fourth_7"] = 11] = "one_fourth_7";
    })(eGridCornerPosType = Global.eGridCornerPosType || (Global.eGridCornerPosType = {}));
    var eValueShowStyle;
    (function (eValueShowStyle) {
        eValueShowStyle[eValueShowStyle["directly"] = 0] = "directly";
        eValueShowStyle[eValueShowStyle["percent"] = 1] = "percent";
        eValueShowStyle[eValueShowStyle["per_second"] = 2] = "per_second";
    })(eValueShowStyle = Global.eValueShowStyle || (Global.eValueShowStyle = {}));
    var eCityHallPageType;
    (function (eCityHallPageType) {
        eCityHallPageType[eCityHallPageType["city"] = 0] = "city";
        eCityHallPageType[eCityHallPageType["game"] = 1] = "game";
    })(eCityHallPageType = Global.eCityHallPageType || (Global.eCityHallPageType = {}));
    var eMoneyType;
    (function (eMoneyType) {
        eMoneyType[eMoneyType["coin"] = 0] = "coin";
        eMoneyType[eMoneyType["diamond"] = 1] = "diamond";
    })(eMoneyType = Global.eMoneyType || (Global.eMoneyType = {}));
})(Global || (Global = {})); // end Global
var CDef = (function () {
    function CDef() {
    }
    CDef.GetAnchorOffsetY = function (obj_size) {
        switch (obj_size) {
            case Global.eObjSize.size_1x1:
                {
                    return 11;
                }
                break;
            case Global.eObjSize.size_2x2:
                {
                    return 175;
                }
                break;
            case Global.eObjSize.size_4x4:
                {
                    return 175;
                }
                break;
        } // end switch
        return 0;
    };
    CDef.GetAnchorOffsetY_Middle = function (obj_size) {
        switch (obj_size) {
            case Global.eObjSize.size_1x1:
                {
                    return 22;
                }
                break;
            case Global.eObjSize.size_2x2:
                {
                    return 90;
                }
                break;
            case Global.eObjSize.size_4x4:
                {
                    return 180;
                }
                break;
        } // end switch
        return 0;
    };
    CDef.GetBuildingOffsetY = function (obj_size) {
        switch (obj_size) {
            case Global.eObjSize.size_2x2:
                {
                    return 180;
                }
                break;
            case Global.eObjSize.size_4x4:
                {
                    return 185;
                }
                break;
        }
        return 0;
    };
    CDef.GetBuildingOffsetY_Middle = function (obj_size) {
        switch (obj_size) {
            case Global.eObjSize.size_2x2:
                {
                    return 65;
                }
                break;
            case Global.eObjSize.size_4x4:
                {
                    return 160;
                }
                break;
        }
        return 0;
    };
    CDef.EDITOR_MODE = false;
    CDef.s_nConstructionProgressBarPosOffsetX = 120;
    CDef.s_nConstructionProgressBarPosOffsetY = 450;
    // 最大规格的格子尺寸
    CDef.s_nTileWidth = 82; //316;
    CDef.s_nTileHeight = 47; // 181;
    CDef.s_nGridNumX = 50;
    CDef.s_nGridNumY = 50;
    CDef.s_nMaxBuildingLevel = 24;
    CDef.s_nSaveTypeStartIndex = Global.eOperate.add_road_zuoshangyouxia;
    CDef.s_nSaveTypeEndIndex = Global.eOperate.num - 1;
    CDef.s_fAutomobileMoveSpeed = 2;
    CDef.s_fAutomobileMoveDirX = 0;
    CDef.s_fAutomobileMoveDirY = 0;
    CDef.s_fFixedDeltaTime = 0.04;
    CDef.s_fTiaoZiTotalTime = 0.5;
    CDef.s_fTiaoZiTotalDistance = 10;
    CDef.s_fCraneFrameInterval = 0.5; //  “塔吊”帧动画的时间间隔
    CDef.s_fBuildingLandLoopInterval = 0.5; //
    CDef.s_fCarReInsertSortInterval = 0.3;
    //// car garage
    CDef.s_fGarageItemWitdh = 550;
    CDef.s_fGarageItemHeight = 300;
    //// end car garage
    //// JinBI
    CDef.s_fJinBiAniInterval = 0.3;
    CDef.s_fJinBiAniFrameNum = 4;
    //// end JinBI
    CDef.RES_1x1 = "1X1_png";
    CDef.COIN_UPDATE_STEP = 15;
    CDef.s_nDoubleTimes = 600;
    CDef.s_nFastForward_10 = 10;
    CDef.s_nFastForward_60 = 60;
    CDef.JINBI_RES = "jinbi_0_png";
    CDef.ZUANSHI_RES = "zuanshi_png";
    return CDef;
}()); // end CDef
__reflect(CDef.prototype, "CDef");
//# sourceMappingURL=CDef.js.map