var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CBuildingManager = (function (_super) {
    __extends(CBuildingManager, _super);
    function CBuildingManager() {
        return _super.call(this) || this;
    } // end constructor
    CBuildingManager.InsertBuildingConfig2List = function (nCommonOrSpecial, config) {
        var ary = null;
        if (nCommonOrSpecial == 0) {
            ary = CBuildingManager.m_aryCommonBuildings;
        }
        else {
            ary = CBuildingManager.m_aryLockedSpecialBuildings;
        }
        ary.push(config);
    };
    CBuildingManager.LoadConfig = function () {
        if (CBuildingManager.m_bBuildingConfigLoaded) {
            return;
        }
        var dicBuildings = CConfigManager.GetBuildingConfigDic();
        var dicSpecialBuildings = CConfigManager.GetSpecialBuildingConfigDic();
        //// 普通建筑
        for (var i = Global.eLotPsroperty.residential; i < Global.eLotPsroperty.length; i++) {
            // 2x2
            var ary_2x2 = dicBuildings[i + "_" + Global.eObjSize.size_2x2];
            for (var j = 0; j < ary_2x2.length; j++) {
                CBuildingManager.InsertBuildingConfig2List(0, ary_2x2[j]);
            }
            // 4x4
            var ary_4x4 = dicBuildings[i + "_" + Global.eObjSize.size_4x4];
            if (ary_4x4 != undefined) {
                for (var j = 0; j < ary_4x4.length; j++) {
                    CBuildingManager.InsertBuildingConfig2List(0, ary_4x4[j]);
                }
            }
        }
        //// 特殊建筑
        for (var i = Global.eLotPsroperty.residential; i < Global.eLotPsroperty.length; i++) {
            // 2x2
            var ary_2x2 = dicSpecialBuildings[i + "_" + Global.eObjSize.size_2x2];
            for (var j = 0; j < ary_2x2.length; j++) {
                CBuildingManager.InsertBuildingConfig2List(1, ary_2x2[j]);
            }
            // 4x4
        }
        CBuildingManager.m_bBuildingConfigLoaded = true;
    };
    CBuildingManager.GetCommonBuildings = function () {
        return CBuildingManager.m_aryCommonBuildings;
    };
    CBuildingManager.GetLockedSpecialBuildings = function () {
        return CBuildingManager.m_aryLockedSpecialBuildings;
    };
    CBuildingManager.GetUnLockSpecialBuildings = function () {
        return CBuildingManager.m_aryUnLockedSpecialBuildings;
    };
    CBuildingManager.PropertyType2String = function (eType) {
        switch (eType) {
            case Global.eLotPsroperty.residential:
                {
                    return "住宅";
                }
                break;
            case Global.eLotPsroperty.business:
                {
                    return "商业";
                }
                break;
            case Global.eLotPsroperty.service:
                {
                    return "服务";
                }
                break;
        }
        return "有Bug!";
    };
    CBuildingManager.UnlockBuilding = function (data) {
        data.bUnLocked = true;
        CBuildingManager.RemoveFromLockedList(data);
        CBuildingManager.AddToUnLockedList(data);
        CPlayer.SetDiamond(CPlayer.GetDiamond() - data.nPrice);
        CUIManager.s_uiSpecialPanel.SelectLastBulding();
        CUIManager.s_uiSpecialPanel.UpdateInfo();
        CUIManager.s_uiSpecialPanel.SwitchPage(1);
    };
    CBuildingManager.RemoveFromLockedList = function (data) {
        for (var i = 0; i < CBuildingManager.m_aryLockedSpecialBuildings.length; i++) {
            var node = CBuildingManager.m_aryLockedSpecialBuildings[i];
            if (node.nID == data.nID) {
                CBuildingManager.m_aryLockedSpecialBuildings.splice(i, 1);
                return;
            }
        }
    };
    CBuildingManager.AddToUnLockedList = function (data) {
        CBuildingManager.m_aryUnLockedSpecialBuildings.push(data);
    };
    CBuildingManager.UseBuilding = function (data) {
        var obj = Main.s_CurTown.GetCurEditProcessingLot();
        obj.SetBuilding(data);
        obj.SetHistorical(true);
        if (data.bSpecial) {
            Main.s_CurTown.UpdateCurLotStatus();
        }
    };
    CBuildingManager.RecycleSpecialBuilding = function (building) {
        building.bUsed = false;
        CUIManager.s_uiSpecialPanel.UpdateInfo();
        Main.s_CurTown.UpdateCurLotStatus();
    };
    CBuildingManager.m_aryCommonBuildings = new Array();
    CBuildingManager.m_aryLockedSpecialBuildings = new Array();
    CBuildingManager.m_aryUnLockedSpecialBuildings = new Array();
    CBuildingManager.m_bBuildingConfigLoaded = false;
    CBuildingManager.m_nNumOfUnlockSpecialBuildings = 0;
    return CBuildingManager;
}(egret.DisplayObjectContainer)); // end class
__reflect(CBuildingManager.prototype, "CBuildingManager");
//# sourceMappingURL=CBuildingManager.js.map