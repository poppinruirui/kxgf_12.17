var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIDistrictSketch = (function (_super) {
    __extends(CUIDistrictSketch, _super);
    function CUIDistrictSketch() {
        var _this = _super.call(this) || this;
        _this.m_uiContainer = new eui.Component();
        _this.m_nId = 0;
        _this.m_uiContainer.skinName = "resource/assets/MyExml/DistrictSketch.exml";
        _this.addChild(_this.m_uiContainer);
        _this.m_txtDistrictId = _this.m_uiContainer.getChildByName("txtDistrictId");
        return _this;
    } // end constructor
    CUIDistrictSketch.prototype.SetDistrictId = function (nId) {
        this.m_nId = nId;
        this.m_txtDistrictId.text = nId + "区";
    };
    CUIDistrictSketch.prototype.GetDistrictId = function () {
        return this.m_nId;
    };
    return CUIDistrictSketch;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIDistrictSketch.prototype, "CUIDistrictSketch");
//# sourceMappingURL=CUIDistrictSketch.js.map