var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIBaseCommonButton = (function (_super) {
    __extends(CUIBaseCommonButton, _super);
    function CUIBaseCommonButton() {
        var _this = _super.call(this) || this;
        var nOutWidth = 400;
        var nOutHeight = 80;
        _this.m_uiBg1 = new CUIBaseDoubleRectBg();
        _this.addChild(_this.m_uiBg1);
        _this.m_uiBg1.SetType(Global.eDoubleRectBgType.up_down);
        _this.m_uiBg1.SetPartSizePercent(0, 1, 0.9);
        _this.m_uiBg1.SetPartColor(0, 1, 1, 1, 1);
        _this.m_uiBg1.SetPartColor(1, 0.663, 0.663, 0.663, 1);
        _this.m_uiBg1.SetSize(nOutWidth, nOutHeight);
        _this.m_uiBg2 = new CUIBaseDoubleRectBg();
        _this.addChild(_this.m_uiBg2);
        _this.m_uiBg2.SetType(Global.eDoubleRectBgType.up_down);
        _this.m_uiBg2.SetPartSizePercent(0, 1, 0.1);
        _this.m_uiBg2.SetPartColor(0, 0.753, 0.2, 0.145, 1);
        _this.m_uiBg2.SetPartColor(1, 0.922, 0.312, 0.227, 1);
        _this.m_uiBg2.SetSize(380, 50);
        _this.m_uiBg2.x = 10;
        _this.m_uiBg2.y = 10;
        _this.m_txtTitle = new eui.Label();
        _this.addChild(_this.m_txtTitle);
        _this.m_txtTitle.textAlign = egret.HorizontalAlign.CENTER;
        _this.m_txtTitle.verticalAlign = egret.VerticalAlign.MIDDLE;
        _this.m_txtTitle.text = "关闭";
        _this.m_txtTitle.width = nOutWidth;
        _this.m_txtTitle.height = nOutHeight * 0.9;
        _this.m_txtTitle.size = 25;
        return _this;
    } // end constructor
    return CUIBaseCommonButton;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIBaseCommonButton.prototype, "CUIBaseCommonButton");
//# sourceMappingURL=CUIBaseCommonButton.js.map