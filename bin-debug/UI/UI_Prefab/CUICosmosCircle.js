var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUICosmosCircle = (function (_super) {
    __extends(CUICosmosCircle, _super);
    function CUICosmosCircle() {
        var _this = _super.call(this) || this;
        _this.m_shapeCircleInner = new egret.Shape();
        _this.m_shapeCircleOut = new egret.Shape();
        _this.m_imgIcon = new eui.Image();
        _this.addChild(_this.m_shapeCircleOut);
        _this.addChild(_this.m_shapeCircleInner);
        _this.addChild(_this.m_imgIcon);
        return _this;
    } // end constructor
    CUICosmosCircle.prototype.SetParams = function (out_radius, out_thickness, out_outline_color, out_color, inner_radius, inner_thickness, inner_outline_color, inner_color) {
        this.m_shapeCircleOut.graphics.clear();
        this.m_shapeCircleOut.graphics.lineStyle(out_thickness, out_outline_color);
        this.m_shapeCircleOut.graphics.beginFill(out_color);
        this.m_shapeCircleOut.graphics.drawCircle(0, 0, out_radius);
        this.m_shapeCircleOut.graphics.endFill();
        this.m_shapeCircleInner.graphics.clear();
        this.m_shapeCircleInner.graphics.lineStyle(inner_thickness, inner_outline_color);
        this.m_shapeCircleInner.graphics.beginFill(inner_color);
        this.m_shapeCircleInner.graphics.drawCircle(0, 0, inner_radius);
        this.m_shapeCircleInner.graphics.endFill();
    };
    return CUICosmosCircle;
}(eui.Component)); // end class
__reflect(CUICosmosCircle.prototype, "CUICosmosCircle");
//# sourceMappingURL=CUICosmosCircle.js.map