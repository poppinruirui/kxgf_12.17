var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIBaseProgressBar = (function (_super) {
    __extends(CUIBaseProgressBar, _super);
    function CUIBaseProgressBar(szExmlSource) {
        var _this = _super.call(this) || this;
        _this.m_BarTotalWidth = 0;
        _this.m_nTotalTime = 0;
        _this.m_fTimeElapse = 0;
        _this.m_bProcessing = false;
        _this.m_shapeMask = new egret.Shape();
        _this.m_nBarTotalWidth = 0;
        _this.m_nBarTotalHeight = 0;
        _this.m_nBarPosX = 0;
        _this.m_nBarPosY = 0;
        _this.SetExml("resource/assets/MyExml/CProgressBar.exml");
        _this.m_labelTitle = _this.getChildByName("labelTitle");
        var imgBar = _this.getChildByName("pbBar");
        imgBar.mask = _this.m_shapeMask;
        imgBar.anchorOffsetX = 0;
        imgBar.anchorOffsetY = 0;
        _this.m_nBarPosX = imgBar.x;
        _this.m_nBarPosY = imgBar.y;
        _this.addChild(_this.m_shapeMask);
        _this.m_nBarTotalWidth = imgBar.width;
        _this.m_nBarTotalHeight = imgBar.height;
        return _this;
        /*
        this.m_imgBG = this.getChildByName( "imgBg" ) as eui.Image;
        if ( this.m_imgBG )
        {
            this.m_imgBG.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255( 211,211,211 ) ];
        }


        this.m_imgFilledBar = this.getChildByName( "imgFilledBar" ) as eui.Image;
        this.m_imgFilledBar.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255(0,255,0) ];
        this.m_BarTotalWidth = this.m_imgFilledBar.width;
        this.m_imgFilledBar.width = 0;

        this.m_imgFilledBar_Bg = this.getChildByName( "imgFilledBarBg" ) as eui.Image;
        this.m_imgFilledBar_Bg.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255( 0,100,0 ) ];

       
        */
    } // end constructor
    CUIBaseProgressBar.prototype.SetTitle = function (szTitle) {
        this.m_labelTitle.text = szTitle;
    };
    CUIBaseProgressBar.prototype.SetExml = function (source) {
        this.skinName = source;
    };
    CUIBaseProgressBar.prototype.Begin = function (nTotalTime) {
        this.m_nTotalTime = nTotalTime;
        this.m_fTimeElapse = 0;
        this.m_bProcessing = true;
    };
    CUIBaseProgressBar.prototype.Loop = function (fPercent) {
        if (fPercent > 1) {
            return;
        }
        //this.m_imgFilledBar.width = this.m_BarTotalWidth * fPercent;
        //this.m_pbMain.SetPercent( fPercent );
        this.m_shapeMask.graphics.clear();
        this.m_shapeMask.graphics.beginFill(0xFF0000);
        this.m_shapeMask.graphics.drawRect(this.m_nBarPosX, this.m_nBarPosY, this.m_nBarTotalWidth * fPercent, this.m_nBarTotalHeight);
        console.log(this.m_BarTotalWidth);
        this.m_shapeMask.graphics.endFill();
    };
    CUIBaseProgressBar.prototype.SetFilledBarColor = function (r, g, b) {
        //this.m_imgFilledBar.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255(r, g, b) ];
    };
    CUIBaseProgressBar.prototype.SetFilledBarBgColor = function (r, g, b) {
        //this.m_imgFilledBar_Bg.filters = [ CColorFucker.GetColorMatrixFilterByRGBA_255(r, g, b) ];
    };
    CUIBaseProgressBar.prototype.SetFilledBarWidth = function (nWidth) {
        //this.m_imgFilledBar.width = nWidth;
    };
    CUIBaseProgressBar.prototype.SetFilledBarTotalWidth = function (nWidth) {
        /*
        this.m_BarTotalWidth = nWidth;

        this.m_imgFilledBar_Bg.width = nWidth;
        */
    };
    CUIBaseProgressBar.prototype.SetFilledBarTotalHeight = function (nHeight) {
        /*
        this.m_imgFilledBar_Bg.height = nHeight;
        this.m_imgFilledBar.height = nHeight;
        */
    };
    CUIBaseProgressBar.prototype.SetPercent = function (nCurValue, nTotalValue) {
        /*
        var fPercent:number = nCurValue / nTotalValue;
        this.m_imgFilledBar.width = this.m_BarTotalWidth * fPercent;
        */
    };
    CUIBaseProgressBar.prototype.SetPercentByPercent = function (fPercent) {
        this.m_pbMain.SetPercent(fPercent);
        // this.m_imgFilledBar.width = this.m_BarTotalWidth * fPercent;
    };
    return CUIBaseProgressBar;
}(eui.Component)); // end class
__reflect(CUIBaseProgressBar.prototype, "CUIBaseProgressBar");
//# sourceMappingURL=CUIBaseProgressBar.js.map