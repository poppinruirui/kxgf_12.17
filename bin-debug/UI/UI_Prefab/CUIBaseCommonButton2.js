var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIBaseCommonButton2 = (function (_super) {
    __extends(CUIBaseCommonButton2, _super);
    function CUIBaseCommonButton2() {
        var _this = _super.call(this) || this;
        var nOutWidth = 150;
        var nOutHeight = 90;
        _this.m_uiBg1 = new CUIBaseDoubleRectBg();
        _this.addChild(_this.m_uiBg1);
        _this.m_uiBg1.SetType(Global.eDoubleRectBgType.up_down);
        _this.m_uiBg1.SetPartSizePercent(0, 1, 0.9);
        _this.m_uiBg1.SetPartColor(0, 0.337, 0.737, 0.212, 1);
        _this.m_uiBg1.SetPartColor(1, 0.216, 0.498, 0.133, 1);
        _this.m_uiBg1.SetSize(nOutWidth, nOutHeight);
        _this.m_txtTitle = new eui.Label();
        _this.addChild(_this.m_txtTitle);
        _this.m_txtTitle.text = "解锁";
        _this.m_txtTitle.y = 15;
        _this.m_txtTitle.x = 60;
        _this.m_txtTitle.size = 20;
        _this.m_txtCost = new eui.Label();
        _this.addChild(_this.m_txtCost);
        _this.m_txtCost.text = "~";
        _this.m_txtCost.x = 50;
        _this.m_txtCost.y = 52;
        _this.m_txtCost.size = 22;
        _this.m_uiIcon = new eui.Image(CJinBiManager.GetFrameTexture(0));
        _this.addChild(_this.m_uiIcon);
        _this.m_uiIcon.scaleX = 0.25;
        _this.m_uiIcon.scaleY = 0.22;
        _this.m_uiIcon.x = 24;
        _this.m_uiIcon.y = 52;
        return _this;
    } // end constructor
    CUIBaseCommonButton2.prototype.SetCost = function (nCost) {
        this.m_txtCost.text = nCost.toString();
    };
    return CUIBaseCommonButton2;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIBaseCommonButton2.prototype, "CUIBaseCommonButton2");
//# sourceMappingURL=CUIBaseCommonButton2.js.map