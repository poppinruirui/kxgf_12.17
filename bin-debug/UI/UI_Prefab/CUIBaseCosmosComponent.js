var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIBaseCosmosComponent = (function (_super) {
    __extends(CUIBaseCosmosComponent, _super);
    function CUIBaseCosmosComponent() {
        return _super.call(this) || this;
    } // end constructor
    CUIBaseCosmosComponent.prototype.SetExml = function (source) {
        this.skinName = source;
    };
    return CUIBaseCosmosComponent;
}(eui.Component)); // end class
__reflect(CUIBaseCosmosComponent.prototype, "CUIBaseCosmosComponent");
//# sourceMappingURL=CUIBaseCosmosComponent.js.map