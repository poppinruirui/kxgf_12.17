var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIBoost = (function (_super) {
    __extends(CUIBoost, _super);
    function CUIBoost() {
        var _this = _super.call(this) || this;
        _this.m_uiContainer = new eui.Component();
        _this.nWereToGain_10 = 0;
        _this.nWereToCost_10 = 0;
        _this.nWereToGain_60 = 0;
        _this.nWereToCost_60 = 0;
        _this.m_uiContainer.skinName = "resource/assets/MyExml/CBoost.exml";
        _this.addChild(_this.m_uiContainer);
        var imgBgMask = _this.m_uiContainer.getChildByName("imgBgMask");
        imgBgMask.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(0, 0, 0)];
        imgBgMask.alpha = 0.6;
        var imgBtnClose = _this.m_uiContainer.getChildByName("btnClose");
        imgBtnClose.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Close, _this);
        var imgAccelerate = _this.m_uiContainer.getChildByName("btnAccelerate");
        imgAccelerate.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_DoubleTime, _this);
        var imgGainImmediately = _this.m_uiContainer.getChildByName("btnGainImmediately");
        imgGainImmediately.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_FastForward0, _this);
        return _this;
        /*
                this.m_labelFastForward0_Gain = this.m_uiContainer.getChildByName( "txtFastForward0_Gain" ) as eui.Label;
                this.m_labelFastForward1_Gain = this.m_uiContainer.getChildByName( "txtFastForward1_Gain" ) as eui.Label;
                this.m_labelFastForward0_Cost = this.m_uiContainer.getChildByName( "txtFastForward0_Cost" ) as eui.Label;
                this.m_labelFastForward1_Cost = this.m_uiContainer.getChildByName( "txtFastForward1_Cost" ) as eui.Label;
        
                var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "btnClose" ) as eui.Image;
                this.m_btnClose = new CCosmosImage();
                this.m_btnClose.SetExml( "resource/assets/MyExml/CCosmosButton1.exml" );
                this.m_uiContainer.addChild( this.m_btnClose );
                this.m_btnClose.x = imgTemp.x;
                this.m_btnClose.y = imgTemp.y;
                this.m_uiContainer.removeChild( imgTemp );
                this.m_btnClose.UseColorSolution(0);
                this.m_btnClose.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Close, this);
        
                for ( var i:number = 0; i < 4; i++ )
                {
                    var btn:CCosmosImage = new CCosmosImage();
                    this.m_uiContainer.addChild( btn );
                    imgTemp = this.m_uiContainer.getChildByName( "btn" + i ) as eui.Image;
                    btn.x = imgTemp.x;
                    btn.y = imgTemp.y;
                    btn.scaleX = imgTemp.scaleX;
                    btn.scaleY = imgTemp.scaleY;
                    btn.width = imgTemp.width;
                    btn.height = imgTemp.height;
                    this.m_uiContainer.removeChild( imgTemp );
                    btn.skinName = "resource/assets/MyExml/CCosmosButton3.exml";
                    btn.SetImageColor_255( null, 1, 88, 192, 56, 255 );
        
                    switch(i)
                    {
                        case 0:
                        {
                            this.m_btnDoubleTime = btn;
                            btn.SetText( 0, "2X" );
                            btn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_DoubleTime, this);
                        }
                        break;
        
                        case 1:
                        {
                          btn.SetText( 0, "2X\n∞" );
                          btn.SetEnabled( false );
                        }
                        break;
        
                        case 2:
                        {
                           btn.SetText( 0, ">" );
                           btn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_FastForward0, this);
                           this.m_btnFastForward0 = btn;
                        }
                        break;
        
                        case 3:
                        {
                            btn.SetText( 0, ">>" );
                            btn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_FastForward1, this);
                            this.m_btnFastForward1 = btn;
                        }
                        break;
                    } // end switch
        
                } // end i
        
        
        */
        /*
                var img2:eui.Image = this.m_uiContainer.getChildByName( "img1" ) as eui.Image;
                img2.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255( 221, 221, 221 )];
        
                var img2:eui.Image = this.m_uiContainer.getChildByName( "img2" ) as eui.Image;
                img2.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255( 111, 189, 95 )];
                */
    } // end constructor
    CUIBoost.prototype.onButtonClick_Close = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.boost_panel, false);
    };
    CUIBoost.prototype.onButtonClick_DoubleTime = function (evt) {
        CPlayer.BeginDoubleTime(CDef.s_nDoubleTimes);
        //this.m_btnDoubleTime.SetEnabled( false );
        CUIManager.SetUiVisible(Global.eUiId.boost_panel, false);
    };
    CUIBoost.prototype.onButtonClick_FastForward0 = function (evt) {
        this.UpdateFastForwardInfo();
        if (CPlayer.GetDiamond() < this.nWereToCost_10) {
            return;
        }
        CPlayer.SetDiamond(CPlayer.GetDiamond() - this.nWereToCost_10);
        Main.s_CurTown.SetCoins(Main.s_CurTown.GetCoins() + this.nWereToGain_10, true);
        CUIManager.SetUiVisible(Global.eUiId.boost_panel, false);
        CUIManager.s_uiMainTitle.BeginCoinsFly();
    };
    CUIBoost.prototype.onButtonClick_FastForward1 = function (evt) {
        this.UpdateFastForwardInfo();
        if (CPlayer.GetDiamond() < this.nWereToCost_60) {
            return;
        }
        CPlayer.SetDiamond(CPlayer.GetDiamond() - this.nWereToCost_60);
        Main.s_CurTown.SetCoins(Main.s_CurTown.GetCoins() + this.nWereToGain_60, true);
    };
    CUIBoost.prototype.isShowing = function () {
        return this.visible;
    };
    CUIBoost.prototype.MainLoop_1 = function () {
    };
    CUIBoost.prototype.UpdateFastForwardInfo = function () {
        return;
        this.nWereToGain_10 = Main.s_CurTown.GetCpsWithoutDoubleTime() * CDef.s_nFastForward_10 * 60;
        this.nWereToGain_60 = Main.s_CurTown.GetCpsWithoutDoubleTime() * CDef.s_nFastForward_60 * 60;
        this.m_labelFastForward0_Gain.text = this.nWereToGain_10.toString();
        this.m_labelFastForward1_Gain.text = this.nWereToGain_60.toString();
        this.nWereToCost_10 = CConfigManager.GetFastforwardCost(0);
        this.nWereToCost_60 = CConfigManager.GetFastforwardCost(1);
        this.m_labelFastForward0_Cost.text = this.nWereToCost_10.toString();
        this.m_labelFastForward1_Cost.text = this.nWereToCost_60.toString();
        if (this.nWereToCost_10 > CPlayer.GetDiamond()) {
            this.m_btnFastForward0.SetEnabled(false);
        }
        else {
            this.m_btnFastForward0.SetEnabled(true);
        }
        if (this.nWereToCost_60 > CPlayer.GetDiamond()) {
            this.m_btnFastForward1.SetEnabled(false);
        }
        else {
            this.m_btnFastForward1.SetEnabled(true);
        }
    };
    CUIBoost.prototype.FixedUpdate = function () {
    };
    return CUIBoost;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIBoost.prototype, "CUIBoost");
//# sourceMappingURL=CUIBoost.js.map