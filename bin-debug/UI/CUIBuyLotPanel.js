var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIBuyLotPanel = (function (_super) {
    __extends(CUIBuyLotPanel, _super);
    function CUIBuyLotPanel() {
        var _this = _super.call(this) || this;
        _this.m_uiContainer = new eui.Component();
        _this.m_aryBuyOneLotButtons = new Array();
        //// Data
        _this.m_nBuyOneCost = 0;
        _this.m_uiContainer.skinName = "resource/assets/MyExml/CBuyLotPanel.exml";
        _this.addChild(_this.m_uiContainer);
        var imgBgMask = _this.m_uiContainer.getChildByName("imgBgMask");
        imgBgMask.filters = [CColorFucker.GetColorMatrixFilterByRGBA(0, 0, 0, 0.6)];
        imgBgMask.alpha = 0.6;
        _this.m_labelBuyOnePrice = _this.m_uiContainer.getChildByName("txtBuyOneCost");
        var imgBuyOneButton_Residential = _this.m_uiContainer.getChildByName("btnBuyResidentialLot");
        var imgBuyOneButton_Business = _this.m_uiContainer.getChildByName("btnBuyBusinessLot");
        var imgBuyOneButton_Service = _this.m_uiContainer.getChildByName("btnBuyServiceLot");
        imgBuyOneButton_Residential.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_BuyOne_0, _this);
        imgBuyOneButton_Business.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_BuyOne_1, _this);
        imgBuyOneButton_Service.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_BuyOne_2, _this);
        var imgClose = _this.m_uiContainer.getChildByName("btnClose");
        imgClose.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Close, _this);
        return _this;
        /*
                this.m_labelBuyOnePrice = this.m_uiContainer.getChildByName( "txtBuyOneCost" ) as eui.Label;
                this.m_labelBulkBuyInfo = this.m_uiContainer.getChildByName( "txtBulkBuyCost" ) as eui.Label;
        
                var imgTemp:eui.Image = this.m_uiContainer.getChildByName( "btnClose" ) as eui.Image;
                this.m_btnClose = new CCosmosImage();
                this.m_btnClose.SetExml( "resource/assets/MyExml/CCosmosButton1.exml" );
                this.m_uiContainer.addChild( this.m_btnClose );
                this.m_btnClose.x = imgTemp.x;
                this.m_btnClose.y = imgTemp.y;
                this.m_uiContainer.removeChild( imgTemp );
                this.m_btnClose.UseColorSolution(0);
                this.m_btnClose.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Close, this);
        
        
                imgTemp = this.m_uiContainer.getChildByName( "btnBulkBuy" ) as eui.Image;
                this.m_btnBulkBuy = new CCosmosImage();
                this.m_btnBulkBuy.SetExml( "resource/assets/MyExml/CCosmosButton1.exml" );
                this.m_uiContainer.addChild( this.m_btnBulkBuy );
                this.m_btnBulkBuy.scaleX = imgTemp.scaleX;
                this.m_btnBulkBuy.scaleY = imgTemp.scaleY;
                this.m_btnBulkBuy.width = imgTemp.width;
                this.m_btnBulkBuy.height = imgTemp.height;
                this.m_btnBulkBuy.x = imgTemp.x;
                this.m_btnBulkBuy.y = imgTemp.y;
                this.m_btnBulkBuy.SetLabelContent( 0, "批量购买2块小型地" );
                this.m_uiContainer.removeChild( imgTemp );
                this.m_btnBulkBuy.UseColorSolution(1);
                this.m_btnBulkBuy.SetEnabled( false );
        
                var img:eui.Image = this.m_uiContainer.getChildByName( "img1" ) as eui.Image;
                img.filters = [ CColorFucker.GetColorMatrixFilterByRGBA( 0.867, 0.867, 0.867, 1 ) ];
        
                img = this.m_uiContainer.getChildByName( "img2" ) as eui.Image;
                img.filters = [ CColorFucker.GetColorMatrixFilterByRGBA( 0.2, 0.2, 0.2, 1 ) ];
        
                //// 按钮
                for ( var i:number = 0; i < 3; i++ )
                {
                   var btnBuyOne:CCosmosImage = new CCosmosImage();
                   this.m_uiContainer.addChild( btnBuyOne);
                   btnBuyOne.SetExml( "resource/assets/MyExml/CCosmosButton2.exml" );
                   var szKey = "";
                   var szLotType = "";
                   switch( i )
                   {
                     case 0:
                     {
                        szKey = "btnBuyResidentialLot";
                        szLotType = "住宅";
                     }
                     break;
                    
                     case 1:
                     {
                        szKey = "btnBuyBusinessLot";
                        szLotType = "商业";
                     }
                     break;
                     
                     case 2:
                     {
                        szKey = "btnBuyServiceLot";
                        szLotType = "服务";
                     }
                     break;
                   } // end switch
                   var btnTemp:eui.Image = this.m_uiContainer.getChildByName( szKey ) as eui.Image;
                   btnBuyOne.x = btnTemp.x;
                   btnBuyOne.y = btnTemp.y;
                   btnBuyOne.scaleX = btnTemp.scaleX;
                   btnBuyOne.scaleY = btnTemp.scaleY;
                   this.m_uiContainer.removeChild( btnTemp );
                   btnBuyOne.UseColorSolution( i + 2 );
        
                   btnBuyOne.SetLabelContent( 0, szLotType );
                   btnBuyOne.SetLabelContent( 1, "占本市土地0%" );
                   if ( i == 0 )
                   {
                    btnBuyOne.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_BuyOne_0, this);
                   }else if ( i == 1 )
                   {
                    btnBuyOne.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_BuyOne_1, this);
                   }else if ( i == 2 )
                   {
                    btnBuyOne.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_BuyOne_2, this);
                   }
        
                   this.m_aryBuyOneLotButtons.push( btnBuyOne );
                 
                }  */
    } // constructor 
    CUIBuyLotPanel.prototype.onButtonClick_Close = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.buy_lot_panel, false);
    };
    CUIBuyLotPanel.prototype.onButtonClick_BuyOne_0 = function () {
        Main.s_CurTown.ProcessBuyOneLot(this.m_nBuyOneCost, 0);
        CUIManager.SetUiVisible(Global.eUiId.buy_lot_panel, false);
    };
    CUIBuyLotPanel.prototype.onButtonClick_BuyOne_1 = function () {
        Main.s_CurTown.ProcessBuyOneLot(this.m_nBuyOneCost, 1);
        CUIManager.SetUiVisible(Global.eUiId.buy_lot_panel, false);
    };
    CUIBuyLotPanel.prototype.onButtonClick_BuyOne_2 = function () {
        Main.s_CurTown.ProcessBuyOneLot(this.m_nBuyOneCost, 2);
        CUIManager.SetUiVisible(Global.eUiId.buy_lot_panel, false);
    };
    CUIBuyLotPanel.prototype.SetParams = function (nBuyOnePrice, nBulkBuyNum, nBulkBuyPrice, fPercentResidential, fPercentBusiness, fPercentServie) {
        if (nBuyOnePrice == undefined) {
            nBuyOnePrice = 0; // poppin test
        }
        this.m_labelBuyOnePrice.text = nBuyOnePrice.toString();
        this.m_nBuyOneCost = nBuyOnePrice;
        if (Main.s_CurTown.GetCoins() < this.m_nBuyOneCost) {
            this.UpdateBuyOneButtonsStatus(false);
        }
        else {
            this.UpdateBuyOneButtonsStatus(true);
        }
        //this.m_aryBuyOneLotButtons[0].SetLabelContent( 1, "占本市土地" + Math.floor( fPercentResidential * 100 ) + "%" );
        //this.m_aryBuyOneLotButtons[1].SetLabelContent( 1, "占本市土地" + Math.floor( fPercentBusiness * 100 ) + "%" );
        //this.m_aryBuyOneLotButtons[2].SetLabelContent( 1, "占本市土地" + Math.floor( fPercentServie * 100 ) + "%" );
    };
    CUIBuyLotPanel.prototype.UpdateBuyOneButtonsStatus = function (bCan) {
        for (var i = 0; i < this.m_aryBuyOneLotButtons.length; i++) {
            this.m_aryBuyOneLotButtons[i].SetEnabled(bCan);
        }
    };
    return CUIBuyLotPanel;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIBuyLotPanel.prototype, "CUIBuyLotPanel");
//# sourceMappingURL=CUIBuyLotPanel.js.map