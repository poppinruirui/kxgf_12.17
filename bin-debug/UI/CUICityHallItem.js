var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUICityHallItem = (function (_super) {
    __extends(CUICityHallItem, _super);
    function CUICityHallItem() {
        var _this = _super.call(this) || this;
        _this.m_uiContainer = new eui.Component();
        _this.m_nCityNo = 0;
        _this.m_nItemIndex = 0;
        _this.m_nConfig = null;
        _this.m_uiContainer.skinName = "resource/assets/MyExml/CCityHallItem.exml";
        _this.addChild(_this.m_uiContainer);
        var imgTemp = _this.m_uiContainer.getChildByName("img1");
        _this.m_btnUpgrade = new CCosmosImage();
        _this.m_btnUpgrade.SetExml("resource/assets/MyExml/CCosmosButton4.exml");
        _this.m_uiContainer.addChild(_this.m_btnUpgrade);
        _this.m_btnUpgrade.x = imgTemp.x;
        _this.m_btnUpgrade.y = imgTemp.y;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnUpgrade.SetImageColor_255(null, 0, 0, 205, 0);
        _this.m_btnUpgrade.SetImageColor_255(null, 1, 69, 139, 0);
        _this.m_btnUpgrade.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Upgrde, _this);
        imgTemp = _this.m_uiContainer.getChildByName("img3");
        _this.m_progressBar = new CUIBaseProgressBar("resource/assets/MyExml/CProgressBar2.exml");
        _this.m_uiContainer.addChild(_this.m_progressBar);
        _this.m_progressBar.x = imgTemp.x;
        _this.m_progressBar.y = imgTemp.y;
        _this.m_progressBar.width = imgTemp.width;
        _this.m_progressBar.height = imgTemp.height;
        _this.m_progressBar.visible = false;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_progressBar.SetFilledBarWidth(0);
        _this.m_progressBar.SetFilledBarTotalWidth(imgTemp.width);
        _this.m_progressBar.SetFilledBarColor(86, 188, 54);
        _this.m_progressBar.SetFilledBarBgColor(172, 172, 172);
        _this.m_imgIcon = _this.m_uiContainer.getChildByName("img2");
        _this.m_imgMoneyIcon = _this.m_uiContainer.getChildByName("img4");
        _this.m_imgMask = _this.m_uiContainer.getChildByName("img5");
        _this.m_uiContainer.addChild(_this.m_imgMask);
        _this.m_txtName = _this.m_uiContainer.getChildByName("label0");
        _this.m_txtDesc = _this.m_uiContainer.getChildByName("label1");
        _this.m_txtGain = _this.m_uiContainer.getChildByName("label2");
        return _this;
    } // end constructor
    CUICityHallItem.prototype.SetBgColor = function (r, g, b) {
        var img0 = this.m_uiContainer.getChildByName("img0");
        img0.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(r, g, b)];
    };
    CUICityHallItem.prototype.SetIcon = function (tex) {
        this.m_imgIcon.texture = tex;
    };
    CUICityHallItem.prototype.SetCityNo = function (nCityNo) {
        this.m_nCityNo = nCityNo;
    };
    CUICityHallItem.prototype.SetUpgradeBtnTotalGain = function (nGain, szContent) {
        if (nGain >= 0) {
            this.m_btnUpgrade.SetText(0, "增加");
        }
        else {
            this.m_btnUpgrade.SetText(0, "减少");
        }
        this.m_btnUpgrade.SetText(1, szContent);
    };
    CUICityHallItem.prototype.SetUpgradeBtnCost = function (nCost) {
        this.m_btnUpgrade.SetText(2, nCost.toString());
    };
    CUICityHallItem.prototype.SetUPgradeMonetIcon = function (icon) {
        this.m_btnUpgrade.SetImageTexture(2, icon);
    };
    CUICityHallItem.prototype.SetItemIndex = function (nPageIndex, nItemIndex) {
        this.m_nItemIndex = nItemIndex;
        this.m_ePageType = nPageIndex;
        this.m_nConfig = CConfigManager.GetCityHallItemConfig(nPageIndex, nItemIndex);
        if (this.m_nConfig != null) {
            this.m_txtName.text = this.m_nConfig.szName;
            this.m_txtDesc.text = this.m_nConfig.szDesc;
            this.m_txtGain.text = CConfigManager.GenShowStyle(this.m_nConfig.eShowStyle, 0);
            var nGain = this.m_nConfig.nGain;
            this.SetUpgradeBtnTotalGain(nGain, CConfigManager.GenShowStyle(this.m_nConfig.eShowStyle, this.m_nConfig.nGain));
            this.SetUpgradeBtnCost(Math.floor(this.m_nConfig.aryValuesCost[0]));
            this.SetUPgradeMonetIcon(nPageIndex == 0 ? RES.getRes(CDef.JINBI_RES) : RES.getRes(CDef.ZUANSHI_RES));
        }
        else {
            this.m_txtName.text = "（功能研发中...）";
            this.m_txtDesc.text = "";
            this.m_txtGain.text = "";
            this.m_btnUpgrade.SetEnabled(false);
            this.SetUPgradeMonetIcon(nPageIndex == 0 ? RES.getRes(CDef.JINBI_RES) : RES.getRes(CDef.ZUANSHI_RES));
        }
    };
    CUICityHallItem.prototype.GetItemIndex = function () {
        return this.m_nItemIndex;
    };
    CUICityHallItem.prototype.GetCityNo = function () {
        return this.m_nCityNo;
    };
    CUICityHallItem.prototype.SetLock = function (bLock) {
        this.m_imgMask.visible = bLock;
    };
    CUICityHallItem.prototype.onButtonClick_Upgrde = function (evt) {
        if (this.m_nConfig == null) {
            console.log("有Bug，还没开发的功能怎么点得中");
            return;
        }
        var eMoneyType = this.m_ePageType == Global.eCityHallPageType.city ? Global.eMoneyType.coin : Global.eMoneyType.diamond;
        var data = Main.s_CurTown.GetCityHallData(this.m_ePageType, this.m_nItemIndex);
        var nCost = this.m_nConfig.aryValuesCost[data.m_nLevel];
        var nMyMoney = 0;
        if (this.m_ePageType == Global.eCityHallPageType.city) {
            nMyMoney = Main.s_CurTown.GetCoins();
        }
        else if (this.m_ePageType == Global.eCityHallPageType.game) {
            nMyMoney = CPlayer.GetDiamond();
        }
        if (nMyMoney < nCost) {
            console.log("有bug，钱不够按钮该灰掉，怎么点中的？？");
            return;
        }
        // 消费金币或钻石
        if (this.m_ePageType == Global.eCityHallPageType.city) {
            Main.s_CurTown.SetCoins(Main.s_CurTown.GetCoins() - nCost, true);
        }
        else if (this.m_ePageType == Global.eCityHallPageType.game) {
            CPlayer.SetDiamond(CPlayer.GetDiamond() - nCost);
        }
        // 获得加成
        // to do
        // 数据刷新 & CityHall界面刷新
        data.m_nLevel++;
        data.m_nCurTotalGain = data.m_nLevel * this.m_nConfig.nGain;
        this.m_txtGain.text = CConfigManager.GenShowStyle(Global.eValueShowStyle.percent, data.m_nCurTotalGain);
        if (data.m_nLevel >= this.m_nConfig.GetMaxLevel()) {
            this.m_btnUpgrade.visible = false;
        }
        else {
            var nMoneyForNextLevel = this.m_nConfig.aryValuesCost[data.m_nLevel];
            nMyMoney = CPlayer.GetMoney(eMoneyType);
            this.SetUpgradeBtnCost(Math.floor(nMoneyForNextLevel));
            if (nMyMoney < nMoneyForNextLevel) {
                this.m_btnUpgrade.SetEnabled(false);
            }
        }
        this.m_progressBar.SetPercent(data.m_nLevel, this.m_nConfig.GetMaxLevel());
        Main.s_CurTown.SetCityHallItemAffect(0, data.m_nCurTotalGain);
        Main.s_CurTown.UpdateCurLotStatus();
        Main.s_CurTown.OnBuildingCPSChanged();
    };
    CUICityHallItem.prototype.UpdateStatus = function () {
        if (this.m_nConfig == null) {
            return;
        }
        var data = Main.s_CurTown.GetCityHallData(this.m_ePageType, this.m_nItemIndex);
        if (data.m_nLevel >= this.m_nConfig.GetMaxLevel()) {
            return;
        }
        var nCost = this.m_nConfig.aryValuesCost[data.m_nLevel];
        var nMyMoney = 0;
        if (this.m_ePageType == Global.eCityHallPageType.city) {
            nMyMoney = CPlayer.GetMoney(Global.eMoneyType.coin);
        }
        else if (this.m_ePageType == Global.eCityHallPageType.game) {
            nMyMoney = CPlayer.GetMoney(Global.eMoneyType.diamond);
        }
        if (nMyMoney > nCost) {
            this.m_btnUpgrade.SetEnabled(true);
        }
        else {
            this.m_btnUpgrade.SetEnabled(false);
        }
    };
    return CUICityHallItem;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUICityHallItem.prototype, "CUICityHallItem");
//# sourceMappingURL=CUICityHallItem.js.map