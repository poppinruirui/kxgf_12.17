var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIMsgBox = (function (_super) {
    __extends(CUIMsgBox, _super);
    function CUIMsgBox() {
        var _this = _super.call(this) || this;
        _this.m_imgBg = new eui.Image();
        _this.m_imgBg.texture = RES.getRes("border_png");
        _this.addChild(_this.m_imgBg);
        _this.m_imgBg.scaleX = 3;
        _this.m_imgBg.scaleY = 6;
        _this.m_btnOK = new eui.Button();
        _this.addChild(_this.m_btnOK);
        _this.m_btnOK.label = "确定";
        _this.m_btnOK.x = 225;
        _this.m_btnOK.y = 200;
        _this.m_btnOK.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.btnTouchHandler, _this);
        _this.m_txtMsg = new eui.Label;
        _this.addChild(_this.m_txtMsg);
        _this.m_txtMsg.text = "我日你个龟";
        _this.m_txtMsg.size = 35;
        _this.m_txtMsg.textAlign = egret.HorizontalAlign.CENTER; //设置水平对齐方式
        _this.m_txtMsg.textColor = 0xFF0000; //设置颜色
        _this.m_txtMsg.x = 20;
        _this.m_txtMsg.y = 100;
        return _this;
    }
    CUIMsgBox.prototype.btnTouchHandler = function (evt) {
        this.visible = false;
    };
    CUIMsgBox.prototype.ShowMsg = function (msg) {
        this.visible = true;
        this.m_txtMsg.text = msg;
    };
    return CUIMsgBox;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIMsgBox.prototype, "CUIMsgBox");
//# sourceMappingURL=CUIMsgBox.js.map