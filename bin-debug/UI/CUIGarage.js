var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
/*
     “车库”类
*/
var CUIGarage = (function (_super) {
    __extends(CUIGarage, _super);
    function CUIGarage() {
        var _this = _super.call(this) || this;
        _this.m_groupForCarsList = new eui.Group();
        _this.m_scrollForCarsList = new eui.Scroller();
        _this.m_aryItems = Array();
        _this.m_uiContainer = new eui.Component();
        _this.sv = null;
        _this.m_uiContainer.skinName = "resource/assets/MyExml/Gararge.exml";
        _this.addChild(_this.m_uiContainer);
        var imgBgMask = _this.m_uiContainer.getChildByName("imgBgMask");
        imgBgMask.filters = [CColorFucker.GetColorMatrixFilterByRGBA(0, 0, 0, 0.6)];
        imgBgMask.alpha = 0.6;
        var nCarNum = 10; //CConfigManager.s_nTotalCarNum;
        _this.sv = new egret.ScrollView();
        _this.addChild(_this.sv);
        var contentContainer = new egret.DisplayObjectContainer();
        for (var i = 0; i < nCarNum; i++) {
            var item = new CUIGarageItem();
            contentContainer.addChild(item);
            item.y = i * (CDef.s_fGarageItemHeight + 5);
            item.SetIndex(i);
            _this.m_aryItems.push(item);
        } // end for i
        _this.sv.x = 20;
        _this.sv.y = 280;
        _this.sv.setContent(contentContainer);
        contentContainer.x = 90;
        contentContainer.y = 80;
        contentContainer.scaleX = 0.55;
        contentContainer.scaleY = 0.55;
        _this.sv.verticalScrollPolicy = "on";
        _this.sv.horizontalScrollPolicy = "off";
        _this.sv.height = 3 * CDef.s_fGarageItemHeight;
        _this.sv.width = 800;
        _this.sv.setScrollPosition(0, 0);
        var imgBtnClose = _this.m_uiContainer.getChildByName("btnClose");
        imgBtnClose.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Close, _this);
        return _this;
        /*
          
  
          this.m_bmpBG = new eui.Image( RES.getRes( "1X1_png" ) );
          this.addChild( this.m_bmpBG );
          this.m_bmpBG.width =600;
          this.m_bmpBG.height = 900;
         
          this.m_txtActiveCarTitle = new eui.Label();
          this.addChild( this.m_txtActiveCarTitle );
          this.m_txtActiveCarTitle.width = this.m_bmpBG.width;
          this.m_txtActiveCarTitle.textAlign = egret.HorizontalAlign.CENTER;
          this.m_txtActiveCarTitle.text = "活动的汽车数：10";
          this.m_txtActiveCarTitle.textColor = 0x000000;
          this.m_txtActiveCarTitle.y = 10;
          this.m_txtActiveCarTitle.bold = true;
  
          this.m_txtEarnigTitle = new eui.Label();
          this.addChild( this.m_txtEarnigTitle );
          this.m_txtEarnigTitle.textAlign = egret.HorizontalAlign.LEFT;
          this.m_txtEarnigTitle.text = "产出金币";
          this.m_txtEarnigTitle.textColor = 0x777777;
          this.m_txtEarnigTitle.x = 200;
          this.m_txtEarnigTitle.y = 50;
          this.m_txtEarnigTitle.size = 24;
  
          this.m_txtEarnigValue = new eui.Label();
          this.addChild( this.m_txtEarnigValue );
          this.m_txtEarnigValue.textAlign = egret.HorizontalAlign.LEFT;
          this.m_txtEarnigValue.text = "75/秒";
          this.m_txtEarnigValue.textColor = 0x777777;
          this.m_txtEarnigValue.x = 340;
          this.m_txtEarnigValue.y = 50;
          this.m_txtEarnigValue.size = 24;
          
  
          this.m_imgCoin = new eui.Image( CJinBiManager.GetFrameTexture(0) );
          this.addChild(this.m_imgCoin);
          this.m_imgCoin.scaleX = 0.4;
          this.m_imgCoin.scaleY = 0.4;
          this.m_imgCoin.x = 305;
          this.m_imgCoin.y = 40;
  
           var nCarNum:number = CConfigManager.s_nTotalCarNum;
  
        
  
          var contentContainer:egret.DisplayObjectContainer = new egret.DisplayObjectContainer();
  
          for ( var i:number = 0; i < nCarNum; i ++ )
          {
              var item:CUIGarageItem = new CUIGarageItem();
            //  this.m_groupForCarsList.addChild( item );
              contentContainer.addChild( item );
              item.y = i * ( CDef.s_fGarageItemHeight + 5 ) ;
  
              item.SetIndex( i );
              
             // item.SetLock( true );
         //    item.SetStatus();
              this.m_aryItems.push( item );
  
          } // end for i
  
         // poppin test
          var sv:egret.ScrollView = new egret.ScrollView();
          this.addChild( sv );
          sv.setContent( contentContainer );
          sv.x = 25;
          sv.y = 100;
          sv.width = 550;
          sv.height = 7 * CDef.s_fGarageItemHeight;
          sv.verticalScrollPolicy = "on";
          sv.horizontalScrollPolicy = "off";
  
          this.m_btnClose = new CUIBaseCommonButton();
          this.addChild( this.m_btnClose );
          this.m_btnClose.x = 100;
          this.m_btnClose.y = 910;
          this.m_btnClose.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onButtonClick_Close, this);
  */
    } // end constructor
    ;
    CUIGarage.prototype.onButtonClick_Close = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.garage, false);
    };
    CUIGarage.prototype.MainLoop = function () {
    };
    CUIGarage.prototype.UpdateStatus = function (nIndex, eStatus) {
        var item = this.m_aryItems[nIndex];
        item.SetStatus(Main.s_CurTown.GetGarageItemStatus(nIndex));
    };
    CUIGarage.prototype.SetMaskVisible = function (nIndex, bVisible) {
        if (nIndex >= this.m_aryItems.length) {
            return;
        }
        var item = this.m_aryItems[nIndex];
        item.SetMaskVisible(bVisible);
    };
    CUIGarage.prototype.ResetScrollPosition = function () {
        this.sv.setScrollPosition(0, 0);
        Main.s_CurTown.UpdateGarageStatus();
    };
    return CUIGarage;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIGarage.prototype, "CUIGarage");
//# sourceMappingURL=CUIGarage.js.map