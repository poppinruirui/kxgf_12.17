var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIBank = (function (_super) {
    __extends(CUIBank, _super);
    function CUIBank() {
        var _this = _super.call(this) || this;
        _this.m_uiContainer = new eui.Component();
        _this.m_uiContainer.skinName = "resource/assets/MyExml/CBank.exml";
        _this.addChild(_this.m_uiContainer);
        _this.m_txtMainValue = _this.m_uiContainer.getChildByName("txt3");
        _this.m_txtBranchProfit = _this.m_uiContainer.getChildByName("txt5");
        var imgTemp = _this.m_uiContainer.getChildByName("img1");
        CColorFucker.SetImageColor(imgTemp, 193, 255, 193);
        imgTemp = _this.m_uiContainer.getChildByName("img4");
        CColorFucker.SetImageColor(imgTemp, 190, 190, 190);
        imgTemp = _this.m_uiContainer.getChildByName("img3");
        CColorFucker.SetImageColor(imgTemp, 211, 211, 211);
        imgTemp = _this.m_uiContainer.getChildByName("img6");
        _this.m_btnUpgrade = new CCosmosImage();
        _this.m_uiContainer.addChild(_this.m_btnUpgrade);
        _this.m_btnUpgrade.SetExml("resource/assets/MyExml/CCosmosButton6.exml");
        _this.m_btnUpgrade.x = imgTemp.x;
        _this.m_btnUpgrade.y = imgTemp.y;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnUpgrade.SetImageColor_New(2, 255, 127, 36);
        _this.m_btnUpgrade.SetImageColor_New(3, 255, 215, 0);
        _this.m_btnUpgrade.SetImageColor_New(1, 211, 211, 211);
        _this.m_btnUpgrade.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_UpGrade, _this);
        imgTemp = _this.m_uiContainer.getChildByName("img5");
        _this.m_btnClose = new CCosmosImage();
        _this.m_btnClose.SetExml("resource/assets/MyExml/CCosmosButton1.exml");
        _this.m_uiContainer.addChild(_this.m_btnClose);
        _this.m_btnClose.x = imgTemp.x;
        _this.m_btnClose.y = imgTemp.y;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnClose.UseColorSolution(0);
        _this.m_btnClose.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Close, _this);
        return _this;
    } // constructor
    CUIBank.prototype.onButtonClick_Close = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.bank, false);
    };
    CUIBank.prototype.UpdateStatus = function () {
        var data = Main.s_CurTown.GetBankData();
        this.m_txtMainValue.text = "离开时自动产出，" +
            CConfigManager.GenShowStyle(Global.eValueShowStyle.percent, data.nCurProfitRate) +
            " 持续 " +
            data.nCurAvailableTime + "分钟";
        ;
        var nCurCost = CConfigManager.GetBankCostByLevel(data.nCurLevel);
        nCurCost = Math.floor(nCurCost);
        this.m_btnUpgrade.SetText(1, nCurCost.toString());
        if (CPlayer.GetMoney(Global.eMoneyType.coin) < nCurCost) {
            this.m_btnUpgrade.SetEnabled(false);
        }
        else {
            this.m_btnUpgrade.SetEnabled(true);
        }
    };
    CUIBank.prototype.onButtonClick_UpGrade = function (evt) {
        var data = Main.s_CurTown.GetBankData();
        var nCurCost = CConfigManager.GetBankCostByLevel(data.nCurLevel);
        if (CPlayer.GetMoney(Global.eMoneyType.coin) < nCurCost) {
            console.log("有bug，金币不够时按钮应该灰掉才对，");
            return;
        }
        // 消费金币
        CPlayer.SetMoney(Global.eMoneyType.coin, CPlayer.GetMoney(Global.eMoneyType.coin) - nCurCost);
        data.SetCurLevel(data.GetCurLevel() + 1);
        // 刷新界面状态
        this.UpdateStatus();
    };
    return CUIBank;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIBank.prototype, "CUIBank");
//# sourceMappingURL=CUIBank.js.map