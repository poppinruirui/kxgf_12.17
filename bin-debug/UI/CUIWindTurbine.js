var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIWindTurbine = (function (_super) {
    __extends(CUIWindTurbine, _super);
    function CUIWindTurbine() {
        var _this = _super.call(this) || this;
        _this.m_uiContainer = new eui.Component();
        _this.m_uiContainer.skinName = "resource/assets/MyExml/CWindTurbine.exml";
        _this.addChild(_this.m_uiContainer);
        _this.m_labelEarningPerBuilding = _this.m_uiContainer.getChildByName("labelEarningPerBuilding");
        _this.m_labelTotalEarning = _this.m_uiContainer.getChildByName("labelTotalEarning");
        var img1 = _this.m_uiContainer.getChildByName("img1");
        img1.filters = [CColorFucker.GetColorMatrixFilterByRGBA(0.867, 0.867, 0.867, 1)];
        var img2 = _this.m_uiContainer.getChildByName("img2");
        img2.filters = [CColorFucker.GetColorMatrixFilterByRGBA(0.2, 0.2, 0.2, 1)];
        var imgTemp = _this.m_uiContainer.getChildByName("btnClose");
        _this.m_btnClose = new CCosmosImage();
        _this.m_btnClose.SetExml("resource/assets/MyExml/CCosmosButton1.exml");
        _this.m_uiContainer.addChild(_this.m_btnClose);
        _this.m_btnClose.x = imgTemp.x;
        _this.m_btnClose.y = imgTemp.y;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnClose.UseColorSolution(0);
        _this.m_btnClose.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Close, _this);
        return _this;
    }
    CUIWindTurbine.prototype.SetParams = function (nEarningPerBuilding, nTotalEarning) {
        this.m_labelEarningPerBuilding.text = nEarningPerBuilding + " / 秒";
        this.m_labelTotalEarning.text = nTotalEarning + " / 秒";
    };
    CUIWindTurbine.prototype.onButtonClick_Close = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.wind_turbine, false);
    };
    return CUIWindTurbine;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIWindTurbine.prototype, "CUIWindTurbine");
//# sourceMappingURL=CUIWindTurbine.js.map