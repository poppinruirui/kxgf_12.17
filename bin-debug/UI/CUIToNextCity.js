var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CUIToNextCity = (function (_super) {
    __extends(CUIToNextCity, _super);
    function CUIToNextCity() {
        var _this = _super.call(this) || this;
        _this.m_uiContainer = new eui.Component();
        _this.m_uiContainer.skinName = "resource/assets/MyExml/CToNextCityPanel.exml";
        _this.addChild(_this.m_uiContainer);
        _this.m_txtTitle = _this.m_uiContainer.getChildByName("txt0");
        _this.m_txtPrompt = _this.m_uiContainer.getChildByName("txt1");
        var imgTemp = _this.m_uiContainer.getChildByName("img2");
        _this.m_btnCancel = new CCosmosImage();
        _this.m_btnCancel.SetExml("resource/assets/MyExml/CCosmosSimpleButton.exml");
        _this.m_uiContainer.addChild(_this.m_btnCancel);
        _this.m_btnCancel.x = imgTemp.x;
        _this.m_btnCancel.y = imgTemp.y;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnCancel.SetImageColor_New(0, 105, 105, 105);
        _this.m_btnCancel.SetTextColor(0, 0xFFFFFF);
        _this.m_btnCancel.SetText(0, "取消");
        _this.m_btnCancel.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Cancel, _this);
        imgTemp = _this.m_uiContainer.getChildByName("img3");
        _this.m_btnNext = new CCosmosImage();
        _this.m_btnNext.SetExml("resource/assets/MyExml/CCosmosSimpleButton.exml");
        _this.m_uiContainer.addChild(_this.m_btnNext);
        _this.m_btnNext.x = imgTemp.x;
        _this.m_btnNext.y = imgTemp.y;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btnNext.SetImageColor_New(0, 0, 0, 0);
        _this.m_btnNext.SetTextColor(0, 0xFFFFFF);
        _this.m_btnNext.SetText(0, "下一级城市");
        _this.m_btnNext.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Next, _this);
        imgTemp = _this.m_uiContainer.getChildByName("img4");
        _this.m_btncontinue = new CCosmosImage();
        _this.m_btncontinue.SetExml("resource/assets/MyExml/CCosmosSimpleButton.exml");
        _this.m_uiContainer.addChild(_this.m_btncontinue);
        _this.m_btncontinue.x = imgTemp.x;
        _this.m_btncontinue.y = imgTemp.y;
        _this.m_uiContainer.removeChild(imgTemp);
        _this.m_btncontinue.SetImageColor_New(0, 0, 0, 0);
        _this.m_btncontinue.SetTextColor(0, 0xFFFFFF);
        _this.m_btncontinue.SetText(0, "继续");
        _this.m_btncontinue.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onButtonClick_Continue, _this);
        imgTemp = _this.m_uiContainer.getChildByName("img0");
        imgTemp.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(0, 0, 0)];
        return _this;
    } // constructor
    CUIToNextCity.prototype.UpdateInfo = function () {
        var objectRet = Main.s_CurTown.CheckIfReachThePopulationToNextCity();
        if (objectRet["Result"]) {
            this.m_btnNext.visible = true;
            this.m_btnCancel.visible = true;
            this.m_btncontinue.visible = false;
            this.m_txtTitle.text = "恭喜";
            this.m_txtPrompt.text = "人口已达到升级标准，可升级到下一座城市。";
            // CSoundManager.PlaySE( Global.eSE.congratulations );
        }
        else {
            this.m_btnNext.visible = false;
            this.m_btnCancel.visible = false;
            this.m_btncontinue.visible = true;
            this.m_txtTitle.text = "继续努力";
            this.m_txtPrompt.text = "人口达到" + objectRet["NeedPopulation"] + "即可升级到下一座城市。";
        }
    };
    CUIToNextCity.prototype.onButtonClick_Cancel = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.to_next_city, false);
    };
    CUIToNextCity.prototype.onButtonClick_Next = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.to_next_city, false);
        Main.s_CurTown.MoveToNextCity();
    };
    CUIToNextCity.prototype.onButtonClick_Continue = function (evt) {
        CUIManager.SetUiVisible(Global.eUiId.to_next_city, false);
    };
    return CUIToNextCity;
}(egret.DisplayObjectContainer)); // end class
__reflect(CUIToNextCity.prototype, "CUIToNextCity");
//# sourceMappingURL=CUIToNextCity.js.map