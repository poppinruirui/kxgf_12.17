var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CResourceManager = (function (_super) {
    __extends(CResourceManager, _super);
    function CResourceManager() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CResourceManager.NewConstructingAni = function () {
        var ani = null;
        if (CResourceManager.s_containerRecycledConstructingAni.numChildren > 0) {
            ani = CResourceManager.s_containerRecycledConstructingAni.getChildAt(0);
            CResourceManager.s_containerRecycledConstructingAni.removeChildAt(0);
            ani.visible = true;
        }
        else {
            ani = new CConstructingAnimation();
        }
        return ani;
    };
    CResourceManager.DeleteConstructingAni = function (ani) {
        ani.visible = false;
        CResourceManager.s_containerRecycledConstructingAni.addChild(ani);
    };
    CResourceManager.FixedUpdate = function () {
    };
    CResourceManager.Init = function () {
        CResourceManager.s_bmpSegTop = new egret.Bitmap(RES.getRes("Hurdle_Top_png"));
        CResourceManager.s_bmpSeg = new egret.Bitmap(RES.getRes("Hurdle_Seg_png"));
    };
    CResourceManager.NewProgressBar = function () {
        var progressBar = null;
        if (CResourceManager.s_containerRecycledProgressBar.numChildren > 0) {
            progressBar = CResourceManager.s_containerRecycledProgressBar.getChildAt(0);
            CResourceManager.s_containerRecycledProgressBar.removeChildAt(0);
            progressBar.Loop(0);
        }
        else {
            progressBar = new CUIBaseProgressBar("resource/assets/MyExml/CProgressBar.exml");
        }
        return progressBar;
    };
    CResourceManager.DeleteProgressBar = function (progressBar) {
        CResourceManager.s_containerRecycledProgressBar.addChild(progressBar);
    };
    CResourceManager.NewDiBiao = function () {
        var dibiao = null;
        if (CResourceManager.s_containerRecycledDiBiao.numChildren > 0) {
            dibiao = CResourceManager.s_containerRecycledDiBiao.getChildAt(0);
            CResourceManager.s_containerRecycledDiBiao.removeChildAt(0);
        }
        else {
            dibiao = new CObj();
        }
        return dibiao;
    };
    CResourceManager.DeleteDiBiao = function (dibiao) {
        CResourceManager.s_containerRecycledDiBiao.addChild(dibiao);
    };
    CResourceManager.NewObj = function () {
        return new CObj();
        var obj = null;
        if (CResourceManager.s_lstRecycledObjs.numChildren > 0) {
            obj = CResourceManager.s_lstRecycledObjs.getChildAt(0);
            CResourceManager.s_lstRecycledObjs.removeChildAt(0);
            obj.Reset();
        }
        else {
            obj = new CObj();
            CResourceManager.s_nTotalObj++;
        }
        return obj;
    };
    CResourceManager.DeleteObj = function (obj) {
        CResourceManager.s_lstRecycledObjs.addChild(obj);
    };
    CResourceManager.GetDiBiaoTextureByTownId = function (nTownId) {
        var szResName = "dibiao" + nTownId + "_png";
        // console.log( "moutain res name=" + szResName );
        return RES.getRes(szResName);
    };
    CResourceManager.GetTreeTextureByTownId = function (nTownId) {
        return RES.getRes("tree" + nTownId + "_png");
    };
    CResourceManager.GetMoutainTextureByTownId = function (nTownId) {
        return RES.getRes("moutain" + nTownId + "_png");
    };
    CResourceManager.GetBubbleTextureByTownId = function (nTownId) {
        return RES.getRes("bubble" + nTownId + "_png");
    };
    CResourceManager.GetCloudResNameByTownId = function (nTownId, nCloudSubId) {
        if (nCloudSubId === void 0) { nCloudSubId = 0; }
        return CResourceManager.url + "Spr/TownsRes/" + nTownId + "/cloud.png";
    };
    CResourceManager.GetCloudResNameByTownId_Local = function (nTownId, nCloudSubId) {
        if (nCloudSubId === void 0) { nCloudSubId = 0; }
        if (nCloudSubId > 0) {
            return "cloud" + nTownId + "_" + nCloudSubId + "_png";
        }
        return "cloud" + nTownId + "_png";
    };
    CResourceManager.GetCloudTextureByTownId = function (nTownId, nCloudSubId) {
        if (nCloudSubId === void 0) { nCloudSubId = 0; }
        if (nCloudSubId == 0) {
            return RES.getRes("cloud" + nTownId + "_png");
        }
        else {
            return RES.getRes("cloud" + nTownId + "_" + nCloudSubId + "_png");
        }
    };
    CResourceManager.GetNumberTexture = function (nTownId) {
        return RES.getRes(nTownId + "_png");
    };
    CResourceManager.NewVector = function () {
        var vec = null;
        if (CResourceManager.s_lstRecycledVectors.numChildren > 0) {
            vec = CResourceManager.s_lstRecycledVectors.getChildAt(0);
            CResourceManager.s_lstRecycledVectors.removeChildAt(0);
        }
        else {
            vec = new CVector();
        }
        return vec;
    };
    CResourceManager.DeleteVector = function (vec) {
        CResourceManager.s_lstRecycledVectors.addChild(vec);
    };
    CResourceManager.prototype.NewShape = function () {
        var shape = null;
        if (CResourceManager.s_lstRecycledShapes.numChildren > 0) {
            shape = CResourceManager.s_lstRecycledShapes.getChildAt(0);
            CResourceManager.s_lstRecycledShapes.removeChildAt(0);
        }
        else {
            shape = new egret.Shape();
        }
        return shape;
    };
    CResourceManager.DeleteShape = function (shape) {
        CResourceManager.s_lstRecycledShapes.addChild(shape);
    };
    CResourceManager.DeleteDistrict = function (district) {
        CResourceManager.s_lstRecycledDistricts.addChild(district);
    };
    CResourceManager.NewDistrict = function () {
        var district = null;
        if (CResourceManager.s_lstRecycledDistricts.numChildren > 0) {
            district = CResourceManager.s_lstRecycledDistricts.getChildAt(0);
            CResourceManager.s_lstRecycledDistricts.removeChildAt(0);
            district.ResetAll();
        }
        else {
            district = new CDistrict();
        }
        return district;
    };
    CResourceManager.NewDistrictSketch = function () {
        var sketch = null;
        if (CResourceManager.s_lstRecycledDistrictSketchs.numChildren > 0) {
            sketch = CResourceManager.s_lstRecycledDistrictSketchs.getChildAt(0);
            CResourceManager.s_lstRecycledDistrictSketchs.removeChildAt(0);
        }
        else {
            sketch = new CUIDistrictSketch();
        }
        return sketch;
    };
    CResourceManager.DeleteDistrictSketch = function (sketch) {
        CResourceManager.s_lstRecycledDistrictSketchs.addChild(sketch);
    };
    CResourceManager.DeleteWindTurbine = function (obj) {
        CResourceManager.s_lstRecycledWindTurbine.addChild(obj);
    };
    CResourceManager.NewFlyJinBi = function () {
        return new CObj;
    };
    CResourceManager.DeleteFlyJinBi = function (obj) {
        CResourceManager.s_lstRecyceFlyJinBi.addChild(obj);
    };
    CResourceManager.url = "http://39.108.53.61:5566/resource/assets/Spr/";
    CResourceManager.url_root = "http://39.108.53.61:5566/resource/assets/";
    CResourceManager.url_local = "/resource/assets/";
    CResourceManager.s_containerRecycledConstructingAni = new egret.DisplayObjectContainer();
    CResourceManager.s_bmpSegTop = null;
    CResourceManager.s_bmpSeg = null;
    CResourceManager.s_containerRecycledProgressBar = new eui.Component();
    CResourceManager.s_containerRecycledDiBiao = new egret.DisplayObjectContainer();
    /////////////////////////
    CResourceManager.s_nTotalObj = 0;
    CResourceManager.s_lstRecycledObjs = new egret.DisplayObjectContainer();
    ///// 
    CResourceManager.s_lstRecycledVectors = new egret.DisplayObjectContainer();
    /////
    CResourceManager.s_lstRecycledShapes = new egret.DisplayObjectContainer();
    CResourceManager.s_lstRecycledDistricts = new egret.DisplayObjectContainer();
    CResourceManager.s_lstRecycledDistrictSketchs = new egret.DisplayObjectContainer();
    /////////
    CResourceManager.s_lstRecycledWindTurbine = new egret.DisplayObjectContainer();
    //// 
    CResourceManager.s_lstRecyceFlyJinBi = new egret.DisplayObjectContainer();
    return CResourceManager;
}(egret.DisplayObjectContainer)); // end class
__reflect(CResourceManager.prototype, "CResourceManager");
//# sourceMappingURL=CResourceManager.js.map