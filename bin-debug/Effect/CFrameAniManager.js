var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CFrameAniManager = (function (_super) {
    __extends(CFrameAniManager, _super);
    function CFrameAniManager() {
        return _super.call(this) || this;
    } // constructor
    CFrameAniManager.GetEffectsContainer = function () {
        return CFrameAniManager.s_containerEffects;
    };
    CFrameAniManager.NewEffect = function () {
        var effect = null;
        if (CFrameAniManager.s_lstRecycledEffects.numChildren > 0) {
            effect = CFrameAniManager.s_lstRecycledEffects.getChildAt(0);
            CFrameAniManager.s_lstRecycledEffects.removeChildAt(0);
        }
        else {
            effect = new CFrameAni();
        }
        // CFrameAniManager.s_containerEffects.addChild( effect );
        CFrameAniManager.s_aryPlayingEffects.push(effect);
        return effect;
    };
    CFrameAniManager.DeleteEffect = function (effect) {
        CFrameAniManager.s_lstRecycledEffects.addChild(effect);
        for (var i = 0; i < CFrameAniManager.s_aryPlayingEffects.length; i++) {
            var effect_node = CFrameAniManager.s_aryPlayingEffects[i];
            if (effect_node == effect) {
                CFrameAniManager.s_aryPlayingEffects.splice(i, 1);
                break;
            }
        }
    };
    CFrameAniManager.FixedUpdate = function () {
        for (var i = CFrameAniManager.s_aryPlayingEffects.length - 1; i >= 0; i--) {
            var effect = CFrameAniManager.s_aryPlayingEffects[i];
            effect.FixedUpdate();
        } // end for`
    };
    CFrameAniManager.s_containerEffects = new egret.DisplayObjectContainer();
    CFrameAniManager.s_lstRecycledEffects = new egret.DisplayObjectContainer();
    CFrameAniManager.s_aryPlayingEffects = new Array();
    return CFrameAniManager;
}(egret.DisplayObjectContainer)); // end class
__reflect(CFrameAniManager.prototype, "CFrameAniManager");
//# sourceMappingURL=CFrameAniManager.js.map