var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var CEffectManager = (function () {
    function CEffectManager() {
    }
    CEffectManager.Init = function () {
        CEffectManager.s_lstUiEffect.addChild(CFrameAniManager.s_containerEffects);
    };
    CEffectManager.FixedUpdate = function () {
        CFrameAniManager.FixedUpdate();
    };
    CEffectManager.s_lstUiEffect = new egret.DisplayObjectContainer();
    return CEffectManager;
}()); // end class
__reflect(CEffectManager.prototype, "CEffectManager");
//# sourceMappingURL=CEffectManager.js.map