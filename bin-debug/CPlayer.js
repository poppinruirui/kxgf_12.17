var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CPlayer = (function (_super) {
    __extends(CPlayer, _super);
    function CPlayer() {
        return _super.call(this) || this;
    } // end constructor
    CPlayer.GetDiamond = function () {
        return CPlayer.s_nDiaomonds;
    };
    CPlayer.SetDiamond = function (nDiamond) {
        CPlayer.s_nDiaomonds = nDiamond;
        CUIManager.s_uiMainTitle.SetDiamond(nDiamond);
    };
    CPlayer.BeginDoubleTime = function (nDoubleTime) {
        CPlayer.s_nDoubleTimeLeft = nDoubleTime;
        CPlayer.s_bDoubleTiming = true;
        Main.s_CurTown.UpdateCPS();
        CAutomobileManager.ChangeCarSpeedDueToDoubleTime(4);
        CUIManager.s_uiCPosButton.SetDoubleTimeLeftVisible(true);
        var music_url = CResourceManager.url_root + "Audio/accelerate.mp3";
        RES.getResByUrl(music_url, CPlayer.onLoadAudioComplete, this, RES.ResourceItem.TYPE_SOUND);
    };
    CPlayer.onLoadAudioComplete = function (event) {
        this.m_Sound = event;
        this.m_Sound.play(0, 1);
    };
    // 一秒一次的轮询
    CPlayer.MainLoop_1 = function () {
        CPlayer.DoubleTimeLoop();
    };
    CPlayer.DoubleTimeLoop = function () {
        if (!CPlayer.s_bDoubleTiming) {
            return;
        }
        CPlayer.s_nDoubleTimeLeft -= 1; // 单位是秒
        var nMin = Math.floor(CPlayer.s_nDoubleTimeLeft / 60);
        var szMin = nMin.toString();
        if (nMin == 0) {
            szMin = "<1";
        }
        CUIManager.s_uiCPosButton.SetDoubleTimeLeft(szMin);
        if (CPlayer.s_nDoubleTimeLeft <= 0) {
            CPlayer.EndDoubleTime();
        }
    };
    CPlayer.IsDoubleTime = function () {
        return CPlayer.s_bDoubleTiming;
    };
    CPlayer.EndDoubleTime = function () {
        CPlayer.s_bDoubleTiming = false;
        CPlayer.s_nDoubleTimeLeft = 0;
        CAutomobileManager.ChangeCarSpeedDueToDoubleTime(0.25);
        CUIManager.s_uiCPosButton.SetDoubleTimeLeftVisible(false);
    };
    CPlayer.GetMoney = function (eMoneyType) {
        if (eMoneyType == Global.eMoneyType.coin) {
            return Main.s_CurTown.GetCoins();
        }
        else if (eMoneyType == Global.eMoneyType.diamond) {
            return CPlayer.GetDiamond();
        }
        return -1;
    };
    CPlayer.SetMoney = function (eMoneyType, nValue) {
        if (eMoneyType == Global.eMoneyType.coin) {
            Main.s_CurTown.SetCoins(nValue, true);
        }
        else if (eMoneyType == Global.eMoneyType.diamond) {
            CPlayer.SetDiamond(nValue);
        }
    };
    CPlayer.SetCityHallGamePageAffect = function (nIndex, nValue) {
        CPlayer.m_dicCityHallGamePageAffect[nIndex] = nValue;
    };
    CPlayer.GetCityHallGamePageAffect = function (nIndex) {
        return CPlayer.m_dicCityHallGamePageAffect[nIndex];
    };
    /*
    注意，钻石是绑定玩家账号的，不会清空。即便城市重生了，钻石也不会消失
    */
    CPlayer.s_nDiaomonds = 10000000; // 当前钻石数量
    // 注：DoubleTime状态是绑在Player身上的，即便城市切换到下一级或者重生了，也不会清零s
    CPlayer.s_nDoubleTimeLeft = 0;
    CPlayer.s_bDoubleTiming = false;
    CPlayer.m_dicCityHallGamePageAffect = new Object();
    return CPlayer;
}(egret.DisplayObjectContainer)); // end class
__reflect(CPlayer.prototype, "CPlayer");
//# sourceMappingURL=CPlayer.js.map