var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CCityHallData = (function (_super) {
    __extends(CCityHallData, _super);
    function CCityHallData() {
        var _this = _super.call(this) || this;
        _this.m_nLevel = 0;
        _this.m_nCurTotalGain = 0;
        return _this;
    } // end constructor
    return CCityHallData;
}(egret.DisplayObjectContainer)); // end class
__reflect(CCityHallData.prototype, "CCityHallData");
//# sourceMappingURL=CCityHallData.js.map