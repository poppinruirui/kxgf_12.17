var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CObjManager = (function (_super) {
    __extends(CObjManager, _super);
    function CObjManager() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CObjManager.NewObj = function () {
        var obj = null;
        if (CObjManager.m_lstRecycledObjs.numChildren > 0) {
            obj = CObjManager.m_lstRecycledObjs.getChildAt(0);
            obj.visible = true;
            obj.Reset();
            CObjManager.m_lstRecycledObjs.removeChildAt(0);
        }
        else {
            obj = new CObj();
        }
        return obj;
    };
    CObjManager.NewObjByResName = function (szResName) {
        var obj = new CObj(); // CObjManager.NewObj();
        var tex = RES.getRes(szResName);
        obj.SetTexture(tex);
        obj.SetGuid(this.s_nObjGuid);
        this.s_nObjGuid++;
        return obj;
    };
    CObjManager.DeleteObj = function (obj) {
        obj.visible = false;
        CObjManager.m_lstRecycledObjs.addChild(obj);
    };
    CObjManager.s_aryRoadResName = [
        "blueRoad0_png",
        "blueRoad1_png"
    ];
    CObjManager.s_nObjGuid = 0;
    CObjManager.m_lstRecycledObjs = new egret.DisplayObjectContainer();
    return CObjManager;
}(egret.DisplayObjectContainer)); // end CObjManager
__reflect(CObjManager.prototype, "CObjManager");
//# sourceMappingURL=CObjManager.js.map