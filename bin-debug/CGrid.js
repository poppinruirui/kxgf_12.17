var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CGrid = (function (_super) {
    __extends(CGrid, _super);
    function CGrid() {
        var _this = _super.call(this) || this;
        _this.m_txtId = null; // = new egret.TextField();
        _this.m_bmpEmpty = null; // = new egret.Bitmap(); //  该网格没有物件时显示的代图
        _this.m_Graphics = null; // = new egret.Shape();
        _this.m_BoundObj = null;
        _this.m_eEmptyGridType = Global.eEmptyGridType.outside_city;
        _this.m_dicCornerPosX = new Object();
        _this.m_dicCornerPosY = new Object();
        _this.m_nGuid = 0;
        _this.m_szId = "";
        _this.m_dicId = new Object();
        _this.m_bZhiNeng = false;
        _this.m_objectPos = new Object();
        if (CDef.EDITOR_MODE) {
            _this.m_bmpEmpty = new egret.Bitmap();
            _this.m_bmpEmpty.texture = RES.getRes("Tile_1x1_png");
            _this.addChild(_this.m_bmpEmpty);
            _this.m_bmpEmpty.filters = [CColorFucker.GetColorMatrixFilterByRGBA(0.663, 0.663, 0.663)];
            //this.m_Graphics = new egret.Shape();
            //this.addChild( this.m_Graphics );
            _this.m_txtId = new egret.TextField();
            _this.addChild(_this.m_txtId);
            _this.m_txtId.text = "123";
            _this.m_txtId.size = 20;
            _this.m_txtId.x = _this.width / 4;
            _this.m_txtId.y = _this.height / 4;
            _this.m_txtId.textAlign = egret.HorizontalAlign.JUSTIFY;
            _this.m_txtId.touchEnabled = true;
            _this.m_txtId.addEventListener(egret.TouchEvent.TOUCH_TAP, _this.onTapMe, _this);
        }
        return _this;
    }
    CGrid.prototype.onTapMe = function (evt) {
        console.log("tap grid:" + this.GetIdByString());
        Main.s_CurTown.Processtap(this, CTown.s_PosXInTown, CTown.s_PosYInTown);
    };
    //  绘制菱形（矢量图）
    CGrid.prototype.DrawRhombus = function (nWidth, nHeight, nHalfWidth, nHalfHeight) {
        if (this.m_Graphics == null) {
            return;
        }
        this.m_Graphics.graphics.lineStyle(0.1, 0x000000);
        this.m_Graphics.graphics.moveTo(0, nHalfHeight);
        this.m_Graphics.graphics.lineTo(nHalfWidth, nHeight);
        this.m_Graphics.graphics.lineTo(nWidth, nHalfHeight);
        this.m_Graphics.graphics.lineTo(nHalfWidth, 0);
        this.m_Graphics.graphics.lineTo(0, nHalfHeight);
        this.m_Graphics.graphics.endFill();
    };
    CGrid.prototype.SetId = function (nIndexX, nIndexY) {
        this.m_dicId["nIndexX"] = nIndexX;
        this.m_dicId["nIndexY"] = nIndexY;
    };
    CGrid.prototype.GetId = function () {
        return this.m_dicId;
    };
    CGrid.prototype.GetIdByString = function () {
        return (this.m_dicId["nIndexX"] + "_" + this.m_dicId["nIndexY"]);
    };
    CGrid.prototype.SetText = function (txt) {
        if (this.m_txtId == null) {
            return;
        }
        this.m_txtId.text = txt;
    };
    CGrid.prototype.SetGridVisible = function (bVisible) {
        if (this.m_Graphics) {
            this.m_Graphics.visible = bVisible;
        }
        this.m_txtId.visible = bVisible;
        this.m_bmpEmpty.alpha = 0.01;
    };
    CGrid.prototype.ClearBoundObj = function () {
        this.m_BoundObj = null;
        return;
        if (this.m_BoundObj == null || this.m_BoundObj == undefined) {
            return;
        }
        // console.log( "销毁当前已有物体：" + this.m_BoundObj.GetGuid() );
        var obj_to_delete = this.m_BoundObj;
        this.m_BoundObj.ClearBoundGrid(); // 互相解绑
        // Main.s_CurTown.DelObj( obj_to_delete );
        CResourceManager.DeleteObj(obj_to_delete);
        this.m_BoundObj = null;
    };
    CGrid.prototype.SetZhiNengJianZhu = function (bVal) {
        this.m_bZhiNeng = bVal;
    };
    CGrid.prototype.GetZhiNengJianZhu = function () {
        return this.m_bZhiNeng;
    };
    CGrid.prototype.SetBoundObj = function (obj) {
        this.m_BoundObj = obj;
        /*
                if ( this.m_BoundObj == null )
                {
                  this.m_bmpEmpty.alpha = 1;
                }
                else
                {
                  this.m_bmpEmpty.alpha = 0.01;
                }
                */
    };
    CGrid.prototype.GetBoundObj = function () {
        return this.m_BoundObj;
    };
    CGrid.prototype.SetPos = function (x, y) {
        this.x = x;
        this.y = y;
        var CornerPosX = 0;
        var CornerPosY = 0;
        CornerPosX = x;
        CornerPosY = y + CDef.s_nTileHeight * 0.5;
        this.m_dicCornerPosX[Global.eGridCornerPosType.bottom] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.bottom] = CornerPosY;
        CornerPosX = x + CDef.s_nTileWidth * 0.5;
        CornerPosY = y;
        this.m_dicCornerPosX[Global.eGridCornerPosType.right] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.right] = CornerPosY;
        CornerPosX = x;
        CornerPosY = y - CDef.s_nTileHeight * 0.5;
        this.m_dicCornerPosX[Global.eGridCornerPosType.top] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.top] = CornerPosY;
        CornerPosX = x - CDef.s_nTileWidth * 0.5;
        CornerPosY = y;
        this.m_dicCornerPosX[Global.eGridCornerPosType.left] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.left] = CornerPosY;
        ///// ------------ 
        // poppin trick
        y += CDef.s_nTileHeight * 0.5;
        CornerPosX = x + CDef.s_nTileWidth * 0.125;
        CornerPosY = y - CDef.s_nTileHeight * 0.125;
        this.m_dicCornerPosX[Global.eGridCornerPosType.one_fourth_0] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.one_fourth_0] = CornerPosY;
        CornerPosX = x + CDef.s_nTileWidth * 0.375;
        CornerPosY = y - CDef.s_nTileHeight * 0.375;
        this.m_dicCornerPosX[Global.eGridCornerPosType.one_fourth_1] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.one_fourth_1] = CornerPosY;
        CornerPosX = x + CDef.s_nTileWidth * 0.375;
        CornerPosY = y - CDef.s_nTileHeight * 0.625;
        this.m_dicCornerPosX[Global.eGridCornerPosType.one_fourth_2] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.one_fourth_2] = CornerPosY;
        CornerPosX = x + CDef.s_nTileWidth * 0.125;
        CornerPosY = y - CDef.s_nTileHeight * 0.875;
        this.m_dicCornerPosX[Global.eGridCornerPosType.one_fourth_3] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.one_fourth_3] = CornerPosY;
        CornerPosX = x - CDef.s_nTileWidth * 0.125;
        CornerPosY = y - CDef.s_nTileHeight * 0.875;
        this.m_dicCornerPosX[Global.eGridCornerPosType.one_fourth_4] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.one_fourth_4] = CornerPosY;
        var CornerPosX = x - CDef.s_nTileWidth * 0.375;
        CornerPosY = y - CDef.s_nTileHeight * 0.625;
        this.m_dicCornerPosX[Global.eGridCornerPosType.one_fourth_5] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.one_fourth_5] = CornerPosY;
        CornerPosX = x - CDef.s_nTileWidth * 0.375;
        CornerPosY = y - CDef.s_nTileHeight * 0.375;
        this.m_dicCornerPosX[Global.eGridCornerPosType.one_fourth_6] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.one_fourth_6] = CornerPosY;
        CornerPosX = x - CDef.s_nTileWidth * 0.125;
        CornerPosY = y - CDef.s_nTileHeight * 0.125;
        this.m_dicCornerPosX[Global.eGridCornerPosType.one_fourth_7] = CornerPosX;
        this.m_dicCornerPosY[Global.eGridCornerPosType.one_fourth_7] = CornerPosY;
    };
    CGrid.prototype.GetCornerPosX = function (eCornerType) {
        return this.m_dicCornerPosX[eCornerType];
    };
    CGrid.prototype.GetCornerPosY = function (eCornerType) {
        return this.m_dicCornerPosY[eCornerType];
    };
    CGrid.prototype.GetPos = function () {
        this.m_objectPos["x"] = this.x;
        this.m_objectPos["y"] = this.y;
        return this.m_objectPos;
    };
    CGrid.prototype.Processtap = function () {
        switch (Main.s_CurTown.GetOperate()) {
            case Global.eOperate.add_windturbine:
                {
                    Main.s_CurTown.AddWindTurbine(this);
                }
                break;
            /*
                      case Global.eOperate.set_inside_empty_grid:
                      {
                        this.SetEmptyGridType( Global.eEmptyGridType.inside_city );
                      }
                      break;
            
                       case Global.eOperate.set_edge_empty_grid:
                      {
                        this.SetEmptyGridType( Global.eEmptyGridType.city_edge );
                      }
                      break;
                      */
            case Global.eOperate.del_obj:
                {
                    var obj = this.GetBoundObj();
                    if (obj != null) {
                        if (this.GetZhiNengJianZhu()) {
                            console.log("不得哦3333333");
                        }
                        this.ClearBoundObj();
                    }
                }
                break;
        } // end switch
        var obj = this.GetBoundObj();
        if (obj != null) {
            obj.ProcessEdit();
        }
    };
    CGrid.prototype.IsTaken = function () {
        return this.m_BoundObj != null;
    };
    CGrid.prototype.SetColor = function (r, g, b) {
        this.m_bmpEmpty.filters = [CColorFucker.GetColorMatrixFilterByRGBA_255(r, g, b)];
    };
    CGrid.prototype.SetEmptyGridType = function (eType) {
        if (this.IsTaken()) {
            return;
        }
        this.m_eEmptyGridType = eType;
        if (this.m_eEmptyGridType == Global.eEmptyGridType.inside_city) {
            this.SetColor(105, 105, 105);
        }
        else if (this.m_eEmptyGridType == Global.eEmptyGridType.city_edge) {
            this.SetColor(82, 173, 70);
        }
    };
    CGrid.prototype.GetEmptyGridType = function () {
        return this.m_eEmptyGridType;
    };
    CGrid.prototype.SetGuid = function (nGuid) {
        this.m_nGuid = nGuid;
    };
    CGrid.prototype.GetGuid = function () {
        return this.m_nGuid;
    };
    return CGrid;
}(egret.DisplayObjectContainer)); // end class CGrid
__reflect(CGrid.prototype, "CGrid");
//# sourceMappingURL=CGrid.js.map