var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var CDistrict = (function (_super) {
    __extends(CDistrict, _super);
    function CDistrict() {
        var _this = _super.call(this) || this;
        _this.m_containerGrids = new egret.DisplayObjectContainer();
        _this.m_containerObjs = new egret.DisplayObjectContainer();
        _this.m_containerRoads = new egret.DisplayObjectContainer();
        _this.m_containerTrees = new egret.DisplayObjectContainer();
        _this.m_containerArrows = new egret.DisplayObjectContainer();
        _this.m_containerTurbine = new egret.DisplayObjectContainer();
        _this.m_containerProgressbar = new egret.DisplayObjectContainer();
        _this.m_containerDiBiao = new egret.DisplayObjectContainer();
        _this.m_containerTiaoZi = new egret.DisplayObjectContainer();
        _this.m_containerTile = new egret.DisplayObjectContainer();
        _this.m_containerDebug = new egret.DisplayObjectContainer();
        _this.m_dicGrids = new Object();
        _this.m_aryAllGrids = new Array();
        _this.m_nId = 1;
        if (CDef.EDITOR_MODE) {
            _this.addChild(_this.m_containerGrids);
        }
        _this.addChild(_this.m_containerDiBiao);
        _this.addChild(_this.m_containerRoads);
        _this.addChild(_this.m_containerTile);
        _this.addChild(_this.m_containerTrees);
        _this.addChild(_this.m_containerObjs);
        _this.addChild(_this.m_containerTurbine);
        _this.addChild(_this.m_containerArrows);
        _this.addChild(_this.m_containerProgressbar);
        _this.addChild(_this.m_containerTiaoZi);
        _this.CreateGrids();
        _this.addChild(_this.m_containerDebug);
        return _this;
    }
    CDistrict.prototype.SetId = function (nId) {
        this.m_nId = nId;
    };
    CDistrict.prototype.GetId = function () {
        return this.m_nId;
    };
    // 12.1_Load
    CDistrict.prototype.LoadTownData = function (szData) {
        var aryData = szData.split('|');
        for (var i = 0; i < aryData.length; i++) {
            if (aryData[i] == "") {
                continue;
            }
            var op = i;
            var szGridIds = aryData[i];
            var aryGridIs = szGridIds.split(',');
            for (var j = 0; j < aryGridIs.length; j++) {
                switch (op) {
                    case Global.eOperate.add_tree:
                        {
                            var aryParams = aryGridIs[j].split('_');
                            var nTreeResId = (Number)(aryParams[0]);
                            var fPosInTownX = (Number)(aryParams[1]);
                            var fPosInTownY = (Number)(aryParams[2]);
                            Main.s_CurTown.DoAddTree(nTreeResId, fPosInTownX, fPosInTownY);
                        }
                        break;
                    case Global.eOperate.add_windturbine:
                        {
                            var aryParams = aryGridIs[j].split('_');
                            var fPosInTownX = (Number)(aryParams[0]);
                            var fPosInTownY = (Number)(aryParams[1]);
                            Main.s_CurTown.DoAddWindTurbine(fPosInTownX, fPosInTownY);
                        }
                        break;
                    default:
                        {
                            var aryGridIndex = aryGridIs[j].split('_');
                            var szIndexX = aryGridIndex[0];
                            var szIndexY = aryGridIndex[1];
                            var szParam0 = "";
                            var szParam1 = "";
                            if (aryGridIndex.length >= 3) {
                                szParam0 = aryGridIndex[2];
                                szParam1 = aryGridIndex[2];
                            }
                            var grid = this.GetGridByIndex(szIndexX, szIndexY);
                            Main.s_CurTown.DoAddObj(grid, op, szParam0, szParam1);
                        }
                        break;
                }
            } // end j
        } // end i
    }; // end LoadTownData
    CDistrict.prototype.ClearAll = function () {
        // 清除Obj
        for (var i = this.m_containerObjs.numChildren - 1; i >= 0; i--) {
            var obj = this.m_containerObjs.getChildAt(i);
            if (obj.GetFuncType() == Global.eObjFunc.car) {
                var jinbi = obj.getChildByName("jinbi");
                if (jinbi != null && jinbi != undefined) {
                    CJinBiManager.DeleteJinBi(jinbi);
                }
            }
            obj.ClearBoundGrid();
            if (obj.GetFuncType() == Global.eObjFunc.car) {
                CAutomobileManager.DeleteCar(obj);
            }
            else {
                CResourceManager.DeleteObj(obj);
            }
        }
        // 清除地块
        for (var i = this.m_containerTile.numChildren - 1; i >= 0; i--) {
            var obj = this.m_containerTile.getChildAt(i);
            CResourceManager.DeleteObj(obj);
        }
        // 清除路面
        for (var i = this.m_containerRoads.numChildren - 1; i >= 0; i--) {
            var obj = this.m_containerRoads.getChildAt(i);
            obj.ClearBoundGrid();
            CResourceManager.DeleteObj(obj);
        }
        // 清除树木
        for (var i = this.m_containerTrees.numChildren - 1; i >= 0; i--) {
            var obj = this.m_containerTrees.getChildAt(i);
            CResourceManager.DeleteObj(obj);
        }
        // 清除地表
        for (var i = this.m_containerDiBiao.numChildren - 1; i >= 0; i--) {
            var obj = this.m_containerDiBiao.getChildAt(i);
            CResourceManager.DeleteObj(obj);
        }
        // 清除风车 
        for (var i = this.m_containerTurbine.numChildren - 1; i >= 0; i--) {
            var wind = this.m_containerTurbine.getChildAt(i);
            CResourceManager.DeleteWindTurbine(wind);
        }
        // 清空所有格子上的信息
        for (var i = this.m_containerGrids.numChildren - 1; i >= 0; i--) {
            var grid = this.m_containerGrids.getChildAt(i);
            grid.ClearBoundObj();
            //  poppin to do
            // 格子应该回收到对象池中复用
        }
        // 清除进度条
        if (this.m_containerProgressbar) {
            for (var i = this.m_containerProgressbar.numChildren - 1; i >= 0; i--) {
                var bar = this.m_containerProgressbar.getChildAt(i);
                CResourceManager.DeleteProgressBar(bar);
            }
        }
    };
    CDistrict.prototype.ResetAll = function () {
    };
    CDistrict.prototype.GetGridByIndex = function (nIndexX, nIndexY) {
        var szKey = nIndexX + "," + nIndexY;
        var grid = this.m_dicGrids[szKey];
        if (grid == undefined) {
            grid = null;
        }
        return grid;
    };
    // right here 12.1
    CDistrict.prototype.CreateGrids = function () {
        var nWidth = CDef.s_nTileWidth;
        var nHeight = CDef.s_nTileHeight;
        var nHalfWidth = nWidth / 2;
        var nHalfHeight = nHeight / 2;
        // 规定：偶数行的列号都是偶数；奇数行的列号都是奇数
        for (var i = -CDef.s_nGridNumY; i <= CDef.s_nGridNumY; i++) {
            var nShit = Math.abs(i);
            for (var j = -CDef.s_nGridNumX + nShit; j <= CDef.s_nGridNumX - nShit; j++) {
                var posX = 0;
                var posY = 0;
                if (i % 2 == 0) {
                    if (j % 2 != 0) {
                        continue;
                    }
                    posX = (j / 2) * nWidth;
                    posY = i * nHalfHeight;
                }
                else {
                    if (j % 2 == 0) {
                        continue;
                    }
                    posX = (j / 2) * nWidth;
                    posY = i * nHalfHeight;
                }
                var grid = new CGrid(); // to youhua 这里应该做对象池，不应该一概直接new
                if (true) {
                    this.m_containerGrids.addChild(grid);
                }
                grid.anchorOffsetX = nHalfWidth;
                grid.anchorOffsetY = nHeight; //nHalfHeight; //;CDef.s_nTileHeight;
                //grid.DrawRhombus( CDef.s_nTileWidth, CDef.s_nTileHeight, nHalfWidth, nHalfHeight);
                //grid.x = posX;
                //grid.y = posY;
                grid.SetPos(posX, posY);
                // j是x轴坐标，i是y轴坐标
                var szKey = j + "," + i;
                grid.SetText(szKey);
                grid.SetId(j, i);
                this.m_dicGrids[szKey] = grid;
                this.m_aryAllGrids.push(grid);
            } // end for j
        } // end for i    
    }; // end CreateGrids
    CDistrict.prototype.BuildingAndUpgradingLoop = function (bBuildingLandLoop) {
        for (var i = 0; i < this.m_containerObjs.numChildren; i++) {
            var obj = this.m_containerObjs.getChildAt(i);
            if (obj.GetFuncType() != Global.eObjFunc.building) {
                continue;
            }
            obj.SetDistrict(this);
            if (obj.GetFuncType() != Global.eObjFunc.building && obj.GetFuncType() != Global.eObjFunc.tile) {
                continue;
            }
            obj.ProgressBarLoop();
            if (bBuildingLandLoop) {
                obj.BuildingLandLoop();
            }
        } //end for vert_obj
        for (var i = 0; i < this.m_containerTile.numChildren; i++) {
            var obj = this.m_containerTile.getChildAt(i);
            if (obj.GetFuncType() != Global.eObjFunc.tile) {
                continue;
            }
            obj.SetDistrict(this);
            if (obj.GetFuncType() != Global.eObjFunc.building && obj.GetFuncType() != Global.eObjFunc.tile) {
                continue;
            }
            obj.ProgressBarLoop();
            if (bBuildingLandLoop) {
                obj.BuildingLandLoop();
            }
        }
    };
    CDistrict.prototype.GetCars = function () {
        CDistrict.s_aryTempObjs.length = 1;
        for (var i = 0; i < this.m_containerObjs.numChildren; i++) {
        }
    };
    CDistrict.s_aryTempObjs = new Array();
    return CDistrict;
}(egret.DisplayObjectContainer)); // end class
__reflect(CDistrict.prototype, "CDistrict");
//# sourceMappingURL=CDistrict.js.map